/*
 * Copyright (c) 2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.common.entity.goal;

import net.aliphaticus.onionadventures.common.entity.custom.mobs.BrownOnionEntity;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.item.Item;

import java.util.HashMap;
import java.util.List;
import java.util.function.Predicate;

public class BrownOnionPickupLovedItemGoal extends Goal {

    private final BrownOnionEntity entity;
    private final int coolDown;
    private final HashMap<Item, Integer> validItems = new HashMap<>();
    private final Predicate<ItemEntity> stackPredicate;

    private int coolDownTimer = 0;
    private ItemEntity targetEntity = null;

    public BrownOnionPickupLovedItemGoal(BrownOnionEntity entity, int coolDown, HashMap<Item, Integer> validItems) {
        this.entity = entity;
        this.coolDown = coolDown;
        this.validItems.putAll(validItems);


        this.stackPredicate = (itemEntity -> {
            return itemEntity.getItem() != null && !itemEntity.getItem().isEmpty() &&
                    validItems.keySet().stream().anyMatch(item -> itemEntity.getItem().getItem() == item);
        });
    }

    @Override
    public boolean canUse() {
        if (--this.coolDownTimer <= 0) {
            this.coolDownTimer = coolDown;
            List<ItemEntity> list = this.entity.level.getEntitiesOfClass(ItemEntity.class, this.entity.getBoundingBox().inflate(1.5D), stackPredicate);
            if (!list.isEmpty()) {
                for (ItemEntity itemEntity :
                        list) {
                    this.targetEntity = itemEntity;
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public boolean canContinueToUse() {
        return false;
    }

    @Override
    public void stop() {
        this.targetEntity = null;
    }

    @Override
    public void tick() {
        if (this.targetEntity != null && this.targetEntity.getItem().getCount() > 0) {
            this.entity.addFriendliness(this.validItems.get(this.targetEntity.getItem().getItem()));

            this.targetEntity.getItem().shrink(1);
            if (targetEntity.getItem().getCount() <= 0) {
                targetEntity.remove(Entity.RemovalReason.DISCARDED);
            }
        }
    }
}
