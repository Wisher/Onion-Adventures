/*
 * Copyright (c) 2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 *
 * Permission & a License is hereby granted to use this code under the conditions that:
 * 1) The project using this code is open-source and accessible publicly via a service such as GitHub, GitLab, etc.
 * 2) The Project using this code is copy-left under a license such as MIT, GNU General Public License v3.0, etc.
 */

package net.aliphaticus.onionadventures.api.networking.packet;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.network.NetworkEvent;

import java.util.function.Supplier;

/**
 * Sets the Item-slot in a Players inventory to a given ItemStack
 */
public class SetInventoryItemC2SPacket extends C2SPacket {

    /**
     * The Slot to set the ItemStack to
     */
    private final int itemSlot;

    /**
     * The ItemStack to be set
     */
    private final ItemStack itemStack;

    public SetInventoryItemC2SPacket(int itemSlot, ItemStack itemStack) {
        this.itemSlot = itemSlot;
        this.itemStack = itemStack;
    }

    public SetInventoryItemC2SPacket(FriendlyByteBuf buf) {
        this.itemSlot = buf.readInt();
        this.itemStack = buf.readItem();
    }

    @Override
    public void toBytes(FriendlyByteBuf buf) {
        buf.writeInt(itemSlot);
        buf.writeItem(itemStack);
    }

    @Override
    public void handle(Supplier<NetworkEvent.Context> supplier) {
        NetworkEvent.Context context = supplier.get();
        context.enqueueWork(() -> {
            // On the Server

            // get player
            ServerPlayer player = context.getSender();

            player.getInventory().items.set(itemSlot, itemStack);
        });
    }
}
