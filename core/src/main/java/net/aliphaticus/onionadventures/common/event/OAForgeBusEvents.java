/*
 * Copyright (c) 2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.common.event;

import net.aliphaticus.onionadventures.OnionAdventures;
import net.aliphaticus.onionadventures.common.command.ChakraCommand;
import net.minecraftforge.event.RegisterCommandsEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = OnionAdventures.MODID, bus = Mod.EventBusSubscriber.Bus.FORGE)
public class OAForgeBusEvents {

    @SubscribeEvent
    public static void registerCommands(RegisterCommandsEvent event) {
        ChakraCommand.register(event.getDispatcher());
    }

}
