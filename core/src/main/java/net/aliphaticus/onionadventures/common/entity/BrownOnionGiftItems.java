/*
 * Copyright (c) 2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.common.entity;

import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;

import java.util.HashMap;

public final class BrownOnionGiftItems {
    
    private static final HashMap<Item, Integer> GIFT_ITEMS = new HashMap<>();
    private static final int MIN_F_TO_DROP_GIFT = 2;
    
    static {
        // special
        GIFT_ITEMS.put(new ItemStack(Items.ENCHANTED_BOOK).getItem(), MIN_F_TO_DROP_GIFT);
        GIFT_ITEMS.put(new ItemStack(Items.POTION).getItem(), MIN_F_TO_DROP_GIFT);
        GIFT_ITEMS.put(new ItemStack(Items.SPLASH_POTION).getItem(), MIN_F_TO_DROP_GIFT + 3);
        GIFT_ITEMS.put(new ItemStack(Items.LINGERING_POTION).getItem(), MIN_F_TO_DROP_GIFT + 8);

        // misc
        GIFT_ITEMS.put(new ItemStack(Items.TOTEM_OF_UNDYING).getItem(), MIN_F_TO_DROP_GIFT + 48);
        GIFT_ITEMS.put(new ItemStack(Items.NETHER_STAR).getItem(), MIN_F_TO_DROP_GIFT + 58);
        GIFT_ITEMS.put(new ItemStack(Items.GOLDEN_APPLE).getItem(), MIN_F_TO_DROP_GIFT + 8);
        GIFT_ITEMS.put(new ItemStack(Items.BLAZE_ROD).getItem(), MIN_F_TO_DROP_GIFT + 8);
        GIFT_ITEMS.put(new ItemStack(Items.ENCHANTED_GOLDEN_APPLE).getItem(), MIN_F_TO_DROP_GIFT + 18);
        GIFT_ITEMS.put(new ItemStack(Items.EXPERIENCE_BOTTLE).getItem(), MIN_F_TO_DROP_GIFT + 13);
        GIFT_ITEMS.put(new ItemStack(Items.SLIME_BALL).getItem(), MIN_F_TO_DROP_GIFT + 3);
        GIFT_ITEMS.put(new ItemStack(Items.GUNPOWDER).getItem(), MIN_F_TO_DROP_GIFT + 4);

        // silk touched ores
        GIFT_ITEMS.put(new ItemStack(Items.ANCIENT_DEBRIS).getItem(), MIN_F_TO_DROP_GIFT + 18);
        GIFT_ITEMS.put(new ItemStack(Items.BUDDING_AMETHYST).getItem(), MIN_F_TO_DROP_GIFT + 23);
        GIFT_ITEMS.put(new ItemStack(Items.DIAMOND_ORE).getItem(), MIN_F_TO_DROP_GIFT + 8);
        GIFT_ITEMS.put(new ItemStack(Items.NETHER_QUARTZ_ORE).getItem(), MIN_F_TO_DROP_GIFT + 11);
        GIFT_ITEMS.put(new ItemStack(Items.GLOWSTONE).getItem(), MIN_F_TO_DROP_GIFT + 11);
        GIFT_ITEMS.put(new ItemStack(Items.EMERALD_ORE).getItem(), MIN_F_TO_DROP_GIFT + 13);
        GIFT_ITEMS.put(new ItemStack(Items.LAPIS_ORE).getItem(), MIN_F_TO_DROP_GIFT + 6);
        GIFT_ITEMS.put(new ItemStack(Items.REDSTONE_ORE).getItem(), MIN_F_TO_DROP_GIFT + 6);
        GIFT_ITEMS.put(new ItemStack(Items.GOLD_ORE).getItem(), MIN_F_TO_DROP_GIFT + 6);
        GIFT_ITEMS.put(new ItemStack(Items.IRON_ORE).getItem(), MIN_F_TO_DROP_GIFT + 5);
        GIFT_ITEMS.put(new ItemStack(Items.COPPER_ORE).getItem(), MIN_F_TO_DROP_GIFT + 5);
        GIFT_ITEMS.put(new ItemStack(Items.COAL_ORE).getItem(), MIN_F_TO_DROP_GIFT + 5);

        // raw ores
        GIFT_ITEMS.put(new ItemStack(Items.NETHERITE_SCRAP).getItem(), MIN_F_TO_DROP_GIFT + 8);
        GIFT_ITEMS.put(new ItemStack(Items.GLOWSTONE_DUST).getItem(), MIN_F_TO_DROP_GIFT + 5);
        GIFT_ITEMS.put(new ItemStack(Items.QUARTZ).getItem(), MIN_F_TO_DROP_GIFT + 5);
        GIFT_ITEMS.put(new ItemStack(Items.DIAMOND).getItem(), MIN_F_TO_DROP_GIFT + 3);
        GIFT_ITEMS.put(new ItemStack(Items.EMERALD).getItem(), MIN_F_TO_DROP_GIFT + 6);
        GIFT_ITEMS.put(new ItemStack(Items.AMETHYST_SHARD).getItem(), MIN_F_TO_DROP_GIFT + 2);
        GIFT_ITEMS.put(new ItemStack(Items.LAPIS_LAZULI).getItem(), MIN_F_TO_DROP_GIFT + 1);
        GIFT_ITEMS.put(new ItemStack(Items.REDSTONE).getItem(), MIN_F_TO_DROP_GIFT + 1);
        GIFT_ITEMS.put(new ItemStack(Items.RAW_GOLD).getItem(), MIN_F_TO_DROP_GIFT + 1);
        GIFT_ITEMS.put(new ItemStack(Items.RAW_IRON).getItem(), MIN_F_TO_DROP_GIFT);
        GIFT_ITEMS.put(new ItemStack(Items.RAW_COPPER).getItem(), MIN_F_TO_DROP_GIFT);
        GIFT_ITEMS.put(new ItemStack(Items.COAL).getItem(), MIN_F_TO_DROP_GIFT);
        GIFT_ITEMS.put(new ItemStack(Items.CLAY_BALL).getItem(), MIN_F_TO_DROP_GIFT);
    }
    
    public static HashMap<Item, Integer> getGiftItems() {
        return GIFT_ITEMS;
    }
    
}
