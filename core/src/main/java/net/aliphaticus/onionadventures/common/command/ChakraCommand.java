/*
 * Copyright (c) 2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.common.command;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.context.CommandContext;
import net.aliphaticus.onionadventures.api.util.PlayerChakraUtils;
import net.minecraft.ChatFormatting;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.Commands;
import net.minecraft.commands.arguments.EntityArgument;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerPlayer;

import java.util.Collection;

public class ChakraCommand {

    public static void register(CommandDispatcher<CommandSourceStack> commandDispatcher) {
        commandDispatcher.register(Commands.literal("chakra").requires((command) -> {
                            return command.hasPermission(2);
                        })
                        .then(Commands.literal("get")
                                .then(Commands.argument("targets", EntityArgument.players())
                                        .executes((command) -> {
                                            return getChakra(command, EntityArgument.getPlayers(command, "targets"));
                                        })
                                )
                        )
                        .then(Commands.literal("broadcast")
                                .then(Commands.argument("targets", EntityArgument.players())
                                        .executes((command) -> {
                                            return broadcastChakra(command, EntityArgument.getPlayers(command, "targets"));
                                        })
                                )
                        )
                        .then(Commands.literal("add")
                                .then(Commands.argument("targets", EntityArgument.players())
                                        .then(Commands.argument("amount", IntegerArgumentType.integer(1))
                                                .executes((command) -> {
                                                    return addChakra(EntityArgument.getPlayers(command, "targets"), IntegerArgumentType.getInteger(command, "amount"));
                                                })
                                        )
                                )
                        )
                        .then(Commands.literal("sub")
                                .then(Commands.argument("targets", EntityArgument.players())
                                        .then(Commands.argument("amount", IntegerArgumentType.integer(1))
                                                .executes((command) -> {
                                                    return subChakra(EntityArgument.getPlayers(command, "targets"), IntegerArgumentType.getInteger(command, "amount"));
                                                })
                                        )
                                )
                        )
                        .then(Commands.literal("set")
                                .then(Commands.argument("targets", EntityArgument.players())
                                        .then(Commands.argument("amount", IntegerArgumentType.integer(0))
                                                .executes((command) -> {
                                                    return setChakra(EntityArgument.getPlayers(command, "targets"), IntegerArgumentType.getInteger(command, "amount"));
                                                })
                                        )
                                )
                        )
        );
    }

    private static int getChakra(CommandContext<CommandSourceStack> command, Collection<ServerPlayer> players) {
        if (command.getSource().getEntity() instanceof ServerPlayer player) {
            // send all listed players chakra is using @a
            players.forEach(playerTarget -> {
                if (playerTarget.getUUID() != player.getUUID()) {
                    player.sendSystemMessage(
                            Component.literal(
                                            playerTarget.getName().getString() + "'s Chakra: ")
                                    .withStyle(ChatFormatting.WHITE).withStyle(ChatFormatting.UNDERLINE).withStyle(ChatFormatting.RESET)
                                    .append(String.valueOf(PlayerChakraUtils.getChakra(playerTarget)))
                                    .withStyle(ChatFormatting.RESET)
                                    .withStyle(ChatFormatting.GREEN)
                    );
                }
            });

            // send own chakra
            player.sendSystemMessage(
                    Component.literal("Chakra: ")
                            .append(String.valueOf(PlayerChakraUtils.getChakra(player))).withStyle(ChatFormatting.GREEN)
            );
        }

        return 0;
    }

    private static int broadcastChakra(CommandContext<CommandSourceStack> command, Collection<ServerPlayer> players) {
        players.forEach(player -> {
            player.sendSystemMessage(
                    Component.literal("Chakra: ")
                            .append(String.valueOf(PlayerChakraUtils.getChakra(player))).withStyle(ChatFormatting.GREEN)
            );
        });

        return 0;
    }

    private static int addChakra(Collection<ServerPlayer> players, int amount) {
        players.forEach(player -> {
            PlayerChakraUtils.addChakra(player, amount);
        });

        return 0;
    }

    private static int subChakra(Collection<ServerPlayer> players, int amount) {
        players.forEach(player -> {
            PlayerChakraUtils.subChakra(player, amount);
        });

        return 0;
    }

    private static int setChakra(Collection<ServerPlayer> players, int amount) {
        players.forEach(player -> {
            PlayerChakraUtils.setChakra(player, amount);
        });

        return 0;
    }

}
