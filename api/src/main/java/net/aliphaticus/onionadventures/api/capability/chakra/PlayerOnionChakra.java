/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 *
 * Permission & a License is hereby granted to use this code under the conditions that:
 * 1) The project using this code is open-source and accessible publicly via a service such as GitHub, GitLab, etc.
 * 2) The Project using this code is copy-left under a license such as MIT, GNU General Public License v3.0, etc.
 */

package net.aliphaticus.onionadventures.api.capability.chakra;

import net.minecraft.nbt.CompoundTag;

/**
 * The class responsible for storing and handling {@link net.minecraft.world.entity.player.Player Player} Chakra
 */
public class PlayerOnionChakra {

    /**
     * Value of the {@link net.minecraft.world.entity.player.Player Player} Chakra
     */
    private int chakra;

    /**
     * Minimum {@link PlayerOnionChakra#chakra} value
     */
    private final int MIN_CHAKRA = 0;
    /**
     * Maximum {@link PlayerOnionChakra#chakra} value
     */
    private final int MAX_CHAKRA = Integer.MAX_VALUE;

    /**
     * Gets the current Chakra
     * @return Chakra value
     */
    public int getChakra() {
        return chakra;
    }

    /**
     * Gets the max Chakra Value
     * @return Maximum possible Chakra value
     */
    public int getMaxChakra() {
        return MAX_CHAKRA;
    }

    /**
     * Adds to the Chakra value
     * @param add amount to add
     */
    public void addChakra(int add) {

        // if chakra is the max value, skip logic
        if (this.chakra != Integer.MAX_VALUE) {
            this.chakra = Math.min(chakra + add, MAX_CHAKRA);
        }

        // if over-flow, set to max value
        if (chakra < 0) {
            this.chakra = Integer.MAX_VALUE;
        }
    }

    /**
     * Subtracts from the Chakra value
     * @param sub amount to subtract
     */
    public void subChakra(int sub) {
        this.chakra = Math.max(chakra - sub, MIN_CHAKRA);
    }

    /**
     * Sets the players chakra to the specified value
     * @param chakra int to set the chakra to
     */
    public void setChakra(int chakra) {
        if (chakra >= MIN_CHAKRA) {
            if (chakra <= MAX_CHAKRA) {
                this.chakra = chakra;
            }
        }
    }

    /**
     * Copies a chakra value from another instance
     * @param source Instance to copy from
     */
    public void copyFrom(PlayerOnionChakra source) {
        this.chakra = source.getChakra();
    }

    /**
     * Used to save the chakra value to disk
     * @param nbt CompoundTag
     */
    public void saveNBTData(CompoundTag nbt) {
        nbt.putInt("onion_chakra", chakra);
    }

    /**
     *  Used to load the chakra value from disk
     * @param nbt CompoundTag
     */
    public void loadNBTData(CompoundTag nbt) {
        chakra = nbt.getInt("onion_chakra");
    }


}
