/*
 * Copyright (c) 2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.common.entity;

import net.minecraft.world.item.alchemy.Potion;
import net.minecraft.world.item.alchemy.Potions;

import java.util.HashMap;

public final class BrownOnionGiftPotionEffects {

    private static final HashMap<Potion, Integer> POTION_EFFECTS = new HashMap<>();
    private static final int MIN_F_TO_DROP_GIFT = 2;

    static {
        // luck
        POTION_EFFECTS.put(Potions.LUCK, MIN_F_TO_DROP_GIFT + 38);

        // invisibility
        POTION_EFFECTS.put(Potions.INVISIBILITY, MIN_F_TO_DROP_GIFT + 8);
        POTION_EFFECTS.put(Potions.LONG_INVISIBILITY, MIN_F_TO_DROP_GIFT + 18);

        // water breathing
        POTION_EFFECTS.put(Potions.WATER_BREATHING, MIN_F_TO_DROP_GIFT + 8);
        POTION_EFFECTS.put(Potions.LONG_WATER_BREATHING, MIN_F_TO_DROP_GIFT + 18);

        // Fire Resistance
        POTION_EFFECTS.put(Potions.FIRE_RESISTANCE, MIN_F_TO_DROP_GIFT + 8);
        POTION_EFFECTS.put(Potions.LONG_FIRE_RESISTANCE, MIN_F_TO_DROP_GIFT + 18);

        // healing
        POTION_EFFECTS.put(Potions.HEALING, MIN_F_TO_DROP_GIFT);
        POTION_EFFECTS.put(Potions.STRONG_HEALING, MIN_F_TO_DROP_GIFT + 13);

        // regeneration
        POTION_EFFECTS.put(Potions.REGENERATION, MIN_F_TO_DROP_GIFT);
        POTION_EFFECTS.put(Potions.LONG_REGENERATION, MIN_F_TO_DROP_GIFT + 5);
        POTION_EFFECTS.put(Potions.STRONG_REGENERATION, MIN_F_TO_DROP_GIFT + 13);

        // leaping
        POTION_EFFECTS.put(Potions.LEAPING, MIN_F_TO_DROP_GIFT);
        POTION_EFFECTS.put(Potions.LONG_LEAPING, MIN_F_TO_DROP_GIFT + 5);
        POTION_EFFECTS.put(Potions.STRONG_LEAPING, MIN_F_TO_DROP_GIFT + 13);

        // harming
        POTION_EFFECTS.put(Potions.HARMING, MIN_F_TO_DROP_GIFT);
        POTION_EFFECTS.put(Potions.STRONG_HARMING, MIN_F_TO_DROP_GIFT + 13);

        // poison
        POTION_EFFECTS.put(Potions.POISON, MIN_F_TO_DROP_GIFT);
        POTION_EFFECTS.put(Potions.LONG_POISON, MIN_F_TO_DROP_GIFT + 5);
        POTION_EFFECTS.put(Potions.STRONG_POISON, MIN_F_TO_DROP_GIFT + 13);

        // strength
        POTION_EFFECTS.put(Potions.STRENGTH, MIN_F_TO_DROP_GIFT);
        POTION_EFFECTS.put(Potions.LONG_STRENGTH, MIN_F_TO_DROP_GIFT + 5);
        POTION_EFFECTS.put(Potions.STRONG_STRENGTH, MIN_F_TO_DROP_GIFT + 13);

        // swiftness
        POTION_EFFECTS.put(Potions.SWIFTNESS, MIN_F_TO_DROP_GIFT);
        POTION_EFFECTS.put(Potions.LONG_SWIFTNESS, MIN_F_TO_DROP_GIFT + 5);
        POTION_EFFECTS.put(Potions.STRONG_SWIFTNESS, MIN_F_TO_DROP_GIFT + 13);
    }

    public static HashMap<Potion, Integer> getPotionEffects() {
        return POTION_EFFECTS;
    }

}
