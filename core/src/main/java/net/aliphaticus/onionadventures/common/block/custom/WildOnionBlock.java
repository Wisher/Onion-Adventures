/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.common.block.custom;

import net.aliphaticus.onionadventures.api.block.custom.AbstractChakraCropBlock;
import net.aliphaticus.onionadventures.api.util.PlayerChakraUtils;
import net.aliphaticus.onionadventures.common.block.OABlocks;
import net.aliphaticus.onionadventures.api.block.state.OABlockStateProperties;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.phys.shapes.VoxelShape;
import org.jetbrains.annotations.NotNull;

public class WildOnionBlock extends AbstractChakraCropBlock {

    public static final int MAX_AGE = OABlockStateProperties.MAX_AGE_2;
    public static final IntegerProperty AGE = OABlockStateProperties.AGE_2;

    private static final VoxelShape[] SHAPE_BY_AGE = new VoxelShape[]{
            Block.box(0.0D, 0.0D, 0.0D, 16.0D, 8.0D, 16.0D),
            Block.box(0.0D, 0.0D, 0.0D, 16.0D, 11.0D, 16.0D),
            Block.box(0.0D, 0.0D, 0.0D, 16.0D, 13.0D, 16.0D)
    };

    public WildOnionBlock(Properties properties, int chakra) {
        super(properties, chakra);
    }

    @Override
    public @NotNull IntegerProperty getAgeProperty() {
        return AGE;
    }

    @Override
    public int getMaxAge() {
        return MAX_AGE;
    }

    @Override
    public VoxelShape[] getShapeByAge() {
        return SHAPE_BY_AGE;
    }

    @Override
    public boolean onDestroyedByPlayer(BlockState state, Level level, BlockPos pos, Player player, boolean willHarvest, FluidState fluid) {

        // if on server
        if (!level.isClientSide()) {

            // if max age
            if (state.getValue(getAgeProperty()) == getMaxAge()) {

                // add chakra to player
                PlayerChakraUtils.addChakra((ServerPlayer) player, getChakra());

            // if one age down
            } else if (state.getValue(getAgeProperty()) == getMaxAge() - 1) {

                // give less chakra
                // give 40% less chakra, if chakra > 0, else give 1 chakra
                PlayerChakraUtils.addChakra((ServerPlayer) player, (Math.round(getChakra() * 0.6) > 0 ? ((int) Math.round(getChakra() * 0.6)) : 1));

            // if two ages down
            } else if (state.getValue(getAgeProperty()) == getMaxAge() - 2) {

                // give even less chakra
                // give 70% less chakra, if chakra > 0, else give 1 chakra
                PlayerChakraUtils.addChakra((ServerPlayer) player, (Math.round(getChakra() * 0.3) > 0 ? ((int) Math.round(getChakra() * 0.3)) : 1));
            }
        }

        return super.onDestroyedByPlayer(state, level, pos, player, willHarvest, fluid, true);
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.@NotNull Builder<Block, BlockState> builder) {
        builder.add(AGE);
    }

    @Override
    protected boolean mayPlaceOn(BlockState blockState, @NotNull BlockGetter blockGetter, @NotNull BlockPos blockPos) {
        return blockState.is(BlockTags.DIRT);
    }

    @Override
    public boolean isValidBonemealTarget(@NotNull LevelReader p_255715_, @NotNull BlockPos p_52259_,
                                         @NotNull BlockState p_52260_, boolean p_52261_) {
        
        return false;
    }

    @Override
    protected @NotNull ItemLike getBaseSeedId() {
        return OABlocks.WILD_ONIONS.get().asItem();
    }
}
