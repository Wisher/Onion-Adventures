/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 *
 * Permission & a License is hereby granted to use this code under the conditions that:
 * 1) The project using this code is open-source and accessible publicly via a service such as GitHub, GitLab, etc.
 * 2) The Project using this code is copy-left under a license such as MIT, GNU General Public License v3.0, etc.
 */

package net.aliphaticus.onionadventures.api.item;

import net.aliphaticus.onionadventures.api.util.OALoggingUtils;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;

/**
 * Interface with some default methods for empowered armour items that use Players Chakra
 */
public interface IChakraEmpoweredArmourItem extends IChakraEmpoweredItem {

    /**
     * Update all equipped armour as dis-empowered armour
     * <br>This causes it to drop with the natural force/spread
     * @param player Player to drop from
     * @param itemStack armour to be dis-empowered and dropped
     * @param armourSlot slot of the armour
     */
    default void updateEquippedItemForDeath(Player player, ItemStack itemStack, int armourSlot) {
        OALoggingUtils.logDebugInfo("Dis-Empowering armour slot " + armourSlot + ", " + itemStack.getDisplayName().getString()
                + " due to player death");

        // create the dis-empowered armour with appropriate damage
        int armourDamage = itemStack.getDamageValue();
        ItemStack itemStack1 = createUpdatedItem(armourDamage);
        copyOrSetUUID(itemStack1, itemStack);

        // set the armour slot to updated item
        player.getInventory().armor.set(armourSlot, itemStack1);

        // remove the original item
        player.getInventory().removeItem(itemStack);
    }


}
