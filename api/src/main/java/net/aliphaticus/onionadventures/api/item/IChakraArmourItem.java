/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 *
 * Permission & a License is hereby granted to use this code under the conditions that:
 * 1) The project using this code is open-source and accessible publicly via a service such as GitHub, GitLab, etc.
 * 2) The Project using this code is copy-left under a license such as MIT, GNU General Public License v3.0, etc.
 */

package net.aliphaticus.onionadventures.api.item;

import net.aliphaticus.onionadventures.api.util.OALoggingUtils;
import net.aliphaticus.onionadventures.api.util.PlayerChakraUtils;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;

/**
 * Interface with some default methods for armour items that use Players Chakra
 */
public interface IChakraArmourItem extends IChakraItem {

    /**
     * Removes the old item and replaces it with the new, equivalent item
     * @param player player to replace the armour of
     * @param stack New item to replace the original
     * @param armourSlot Slot of the armour
     */
    default void copyItemArmour(Player player, ItemStack stack, int armourSlot) {
        OALoggingUtils.logDebugInfo("Setting " + player.getName().getString() + "'s armour slot " +
                armourSlot + " to " + stack.getDisplayName().getString());

        // set the stack
        player.getInventory().armor.set(armourSlot, stack);
    }

    /**
     * Inverts the Empowered status of an item
     * @param player Player to update the item on
     * @param stack Original item
     * @param armourSlot Slot of the armour
     */
    default void updateItemEquipped(Player player, ItemStack stack, int armourSlot) {
        OALoggingUtils.logDebugInfo("Attempting to Dis/Empower " + player.getName().getString() + "'s equipped " +
                stack.getDisplayName().getString());

        // get damage
        int damage = stack.getDamageValue();
        ItemStack stack1 = createUpdatedItem(damage);
        copyOrSetUUID(stack1, stack);

        // dis/empower tool
        // if empowering, check chakra
        if (!getEmpowered()) {
            if (hasEnoughChakra(getActivationCost() + getUseCost(), (ServerPlayer) player)) {
                // if has enough chakra to empower
                PlayerChakraUtils.subChakra((ServerPlayer) player, getActivationCost());

                copyItemArmour(player, stack1, armourSlot);
            } else {
                // add cooldown if failed to empower
                player.getCooldowns().addCooldown(stack.getItem(), 20);
            }
        } else {
            // dis-empower tool
            copyItemArmour(player, stack1, armourSlot);
        }

        // add cooldown
        player.getCooldowns().addCooldown(stack1.getItem(), 20);
    }

}
