/*
 * Copyright (c) 2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 *
 * Permission & a License is hereby granted to use this code under the conditions that:
 * 1) The project using this code is open-source and accessible publicly via a service such as GitHub, GitLab, etc.
 * 2) The Project using this code is copy-left under a license such as MIT, GNU General Public License v3.0, etc.
 */

package net.aliphaticus.onionadventures.api.util;

import net.minecraftforge.fml.ModList;

/**
 * Static Utilities for Mod Compatability
 */
public class ModCompatUtils {

    /**
     * Checks if Curios is loaded
     * @return is Curios loaded
     */
    public static boolean isCuriosLoaded() {
        return ModList.get().isLoaded("curios");
    }

}
