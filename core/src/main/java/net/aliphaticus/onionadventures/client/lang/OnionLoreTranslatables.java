/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.client.lang;

public final class OnionLoreTranslatables {

    /**
     * Current Number of Lore values
     */
    public static final int LORE_COUNT = 15;

    public static final String ONION_LORE_1 = "lore.onionadventures.one";
    public static final String ONION_LORE_2 = "lore.onionadventures.two";
    public static final String ONION_LORE_3 = "lore.onionadventures.three";
    public static final String ONION_LORE_4 = "lore.onionadventures.four";
    public static final String ONION_LORE_5 = "lore.onionadventures.five";
    public static final String ONION_LORE_6 = "lore.onionadventures.six";
    public static final String ONION_LORE_7 = "lore.onionadventures.seven";
    public static final String ONION_LORE_8 = "lore.onionadventures.eight";
    public static final String ONION_LORE_9 = "lore.onionadventures.nine";
    public static final String ONION_LORE_10 = "lore.onionadventures.ten";
    public static final String ONION_LORE_11 = "lore.onionadventures.eleven";
    public static final String ONION_LORE_12 = "lore.onionadventures.twelve";
    public static final String ONION_LORE_13 = "lore.onionadventures.thirteen";
    public static final String ONION_LORE_14 = "lore.onionadventures.fourteen";
    public static final String ONION_LORE_15 = "lore.onionadventures.fifteen";

    /**
     * Informative error
     */
    public static final String ONION_LORE_ERROR = "lore.onionadventures.error";

}
