/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 *
 * Permission & a License is hereby granted to use this code under the conditions that:
 * 1) The project using this code is open-source and accessible publicly via a service such as GitHub, GitLab, etc.
 * 2) The Project using this code is copy-left under a license such as MIT, GNU General Public License v3.0, etc.
 */

package net.aliphaticus.onionadventures.api.item.armour;

import net.aliphaticus.onionadventures.api.capability.chakra.PlayerOnionChakra;
import net.aliphaticus.onionadventures.api.item.IChakraArmourItem;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import org.jetbrains.annotations.NotNull;

/**
 * Parent class for non-empowered Armour items
 */
public abstract class AbstractOnionChakraArmourItem extends ArmorItem implements IChakraArmourItem {

    /**
     * Is the Item Empowered (usually false)
     */
    private final boolean isEmpowered;
    /**
     * {@link PlayerOnionChakra Chakra} cost to empower the item
     */
    private final int activationCost;
    /**
     * {@link PlayerOnionChakra Chakra} cost to use the item
     */
    private final int useCost;
    /**
     * String[] of the hover text that appears when holding shift
     */
    private final String[] hoverTranslatable;

    /**
     * Invokes the other AbstractOnionChakraArmourItem {@link AbstractOnionChakraArmourItem#AbstractOnionChakraArmourItem(ArmorMaterial, Type, boolean, int, int, Properties, String[]) Constructor}
     * with the default value of isEmpowered: false
     * @param armorMaterial Can be a custom material
     * @param type For the Armour slot
     * @param activationCost Chakra cost to activate
     * @param useCost Chakra cost to use
     * @param properties Item {@link net.minecraft.world.item.Item.Properties Properties}
     * @param hoverTranslatable Shift Hover text
     */
    public AbstractOnionChakraArmourItem(ArmorMaterial armorMaterial, ArmorItem.Type type, int activationCost,
                                         int useCost, Properties properties, String[] hoverTranslatable) {
        this(armorMaterial, type, false, activationCost, useCost, properties, hoverTranslatable);
    }

    /**
     * Invokes the {@link ArmorItem} constructor, and sets the additional properties of {@link net.aliphaticus.onionadventures.api.item.IChakraArmourItem IChakraArmourItem}
     * @param armorMaterial Can be a custom material
     * @param type For the Armour slot
     * @param isEmpowered Empowered status
     * @param activationCost Chakra cost to activate
     * @param useCost Chakra cost to use
     * @param properties Item {@link net.minecraft.world.item.Item.Properties Properties}
     * @param hoverTranslatable Shift Hover text
     */
    public AbstractOnionChakraArmourItem(ArmorMaterial armorMaterial, ArmorItem.Type type, boolean isEmpowered,
                                         int activationCost, int useCost, Properties properties, String[] hoverTranslatable) {
        super(armorMaterial, type, properties);

        this.isEmpowered = isEmpowered;
        this.activationCost = activationCost;
        this.useCost = useCost;

        this.hoverTranslatable = hoverTranslatable;
    }

    public int getActivationCost() {
        return this.activationCost;
    }

    public int getUseCost() {
        return this.useCost;
    }

    public boolean getEmpowered() {
        return this.isEmpowered;
    }

    public String[] getHoverTranslatable() {
        return this.hoverTranslatable;
    }

    @Override
    public @NotNull InteractionResultHolder<ItemStack> use(@NotNull Level level, @NotNull Player player,
                                                           @NotNull InteractionHand hand) {

        if (hand == InteractionHand.MAIN_HAND && !level.isClientSide()) {
            updateItem(player, player.getItemInHand(hand));
        }

//        return super.use(level, player, hand);
        return InteractionResultHolder.sidedSuccess(player.getItemInHand(InteractionHand.MAIN_HAND), level.isClientSide());
    }

    @Override
    public boolean isFoil(@NotNull ItemStack itemStack) {
        return getEmpowered();
    }

    @Override
    public void onCraftedBy(@NotNull ItemStack itemStack, @NotNull Level level, @NotNull Player player) {
        copyOrSetUUID(itemStack, itemStack);
        super.onCraftedBy(itemStack, level, player);
    }

}
