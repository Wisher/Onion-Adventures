/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.datagen;

import net.aliphaticus.onionadventures.OnionAdventures;
import net.minecraft.core.HolderLookup;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.PackOutput;
import net.minecraftforge.common.data.BlockTagsProvider;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.data.event.GatherDataEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;

@Mod.EventBusSubscriber(modid = OnionAdventures.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class DataGenerators {

    @SubscribeEvent
    public static void gatherData(GatherDataEvent event) {
        DataGenerator generator = event.getGenerator();
        PackOutput packOutput = generator.getPackOutput();
        ExistingFileHelper existingFileHelper = event.getExistingFileHelper();
        CompletableFuture<HolderLookup.Provider> lookupProvider = event.getLookupProvider();

        OABlockTagProvider blockTagProvider = new OABlockTagProvider(packOutput, lookupProvider, existingFileHelper);

        generator.addProvider(true, new OARecipeProvider(packOutput));
        generator.addProvider(true, OALootTableProvider.create(packOutput));
        generator.addProvider(true, new OABlockStateProvider(packOutput, existingFileHelper));
        generator.addProvider(true, new OABlockModelProvider(packOutput, existingFileHelper));
        generator.addProvider(true, new OAItemModelProvider(packOutput, existingFileHelper));
        generator.addProvider(event.includeServer(), new OAWorldGenProvider(packOutput, lookupProvider));
        generator.addProvider(event.includeServer(), blockTagProvider);
        generator.addProvider(event.includeServer(),
                new OAItemTagProvider(packOutput, lookupProvider, blockTagProvider.contentsGetter(), existingFileHelper)
        );
    }

}
