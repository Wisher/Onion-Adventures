/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.common.item;

import net.aliphaticus.onionadventures.OnionAdventures;
import net.aliphaticus.onionadventures.api.item.AbstractChakraGivingItem;
import net.aliphaticus.onionadventures.api.item.ChakraItemRecord;
import net.aliphaticus.onionadventures.client.lang.ItemHoverTranslatables;
import net.aliphaticus.onionadventures.common.block.OABlocks;
import net.aliphaticus.onionadventures.common.entity.OAEntities;
import net.aliphaticus.onionadventures.common.item.custom.CookedOnionItem;
import net.aliphaticus.onionadventures.common.item.custom.OnionChakraLedgerItem;
import net.aliphaticus.onionadventures.common.item.custom.armour.base.*;
import net.aliphaticus.onionadventures.common.item.custom.armour.diamond.*;
import net.aliphaticus.onionadventures.common.item.custom.armour.gold.*;
import net.aliphaticus.onionadventures.common.item.custom.armour.iron.*;
import net.aliphaticus.onionadventures.common.item.custom.armour.netherite.*;
import net.aliphaticus.onionadventures.common.item.custom.food.RedOnionItem;
import net.aliphaticus.onionadventures.common.item.custom.food.RoastedOnionEmpoweredItem;
import net.aliphaticus.onionadventures.common.item.custom.food.RoastedOnionItem;
import net.aliphaticus.onionadventures.common.item.custom.projectile.RedOnionEmpoweredItem;
import net.aliphaticus.onionadventures.common.item.custom.tool.axe.*;
import net.aliphaticus.onionadventures.common.item.custom.tool.hoe.*;
import net.aliphaticus.onionadventures.common.item.custom.tool.pickaxe.*;
import net.aliphaticus.onionadventures.common.item.custom.tool.shovel.*;
import net.aliphaticus.onionadventures.common.item.custom.weapon.*;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.food.FoodProperties;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemNameBlockItem;
import net.minecraftforge.common.ForgeSpawnEggItem;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class OAItems {

    // Need a deferred register to register the items
    public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, OnionAdventures.MODID);


    // ########## ChakraItem Records ##########

    public static final ChakraItemRecord ROASTED_ONION_RECORD = new ChakraItemRecord(50, 0);
    private static final ChakraItemRecord RED_ONION_RECORD = new ChakraItemRecord(100, 0);
    private static final ChakraItemRecord TIERED_BASE = new ChakraItemRecord(10, 1);
    private static final ChakraItemRecord TIERED_IRON = new ChakraItemRecord(20, 2);
    public static final ChakraItemRecord TIERED_ARMOUR_IRON = new ChakraItemRecord(15, 2);
    private static final ChakraItemRecord TIERED_GOLD = new ChakraItemRecord(20, 2);
    public static final ChakraItemRecord TIERED_ARMOUR_GOLD = new ChakraItemRecord(15, 2);
    private static final ChakraItemRecord TIERED_DIAMOND = new ChakraItemRecord(30, 4);
    public static final ChakraItemRecord TIERED_ARMOUR_DIAMOND = new ChakraItemRecord(20, 4);
    private static final ChakraItemRecord TIERED_NETHERITE = new ChakraItemRecord(40, 6);
    public static final ChakraItemRecord TIERED_ARMOUR_NETHERITE = new ChakraItemRecord(25, 6);


    // ########## Standard Items ##########
    public static final RegistryObject<Item> ONION = ITEMS.register("onion",
            () -> new Item(new Item.Properties().food(new FoodProperties.Builder()
                    .nutrition(2).saturationMod(2F).build())));

    public static final RegistryObject<Item> BUNCH_OF_ONIONS = ITEMS.register("bunch_of_onions",
            () -> new Item(new Item.Properties().food(new FoodProperties.Builder()
                    .nutrition(3).saturationMod(3F).build())));

    public static final RegistryObject<Item> ONION_SEEDS = ITEMS.register("onion_seeds",
            () -> new ItemNameBlockItem(OABlocks.ONION_CROP.get(),
                    new Item.Properties()));

    public static final RegistryObject<Item> BROWN_ONION_SPAWN_EGG = ITEMS.register("brown_onion_spawn_egg",
            () -> new ForgeSpawnEggItem(OAEntities.BROWN_ONION, 0x924e12 , 0x842100, new Item.Properties()));

    public static final RegistryObject<Item> RED_ONION_SPAWN_EGG = ITEMS.register("red_onion_spawn_egg",
            () -> new ForgeSpawnEggItem(OAEntities.RED_ONION, 0x863c5c , 0x5b1238, new Item.Properties()));



    // ########## Custom Items ##########
    public static final RegistryObject<Item> ROASTED_ONION = ITEMS.register("roasted_onion",
            () -> new RoastedOnionItem(ROASTED_ONION_RECORD.activationCost(), ROASTED_ONION_RECORD.useCost(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_FOOD,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_FOOD_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3_STACKABLE
                    },
                    new Item.Properties().food(new FoodProperties.Builder()
                            .nutrition(3).saturationMod(3F).build())));

    public static final RegistryObject<Item> ROASTED_ONION_EMPOWERED = ITEMS.register("roasted_onion_empowered",
            () -> new RoastedOnionEmpoweredItem(ROASTED_ONION_RECORD.activationCost(), ROASTED_ONION_RECORD.useCost(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_FOOD,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_FOOD_1
                    },
                    new Item.Properties().food(new FoodProperties.Builder()
                            .nutrition(4).fast().saturationMod(4F).alwaysEat()
                            .effect(() -> new MobEffectInstance(MobEffects.REGENERATION, 200, 0), 1F)
                            .effect(() -> new MobEffectInstance(MobEffects.ABSORPTION, 160, 0), 1F)
                            .build())));

    public static final RegistryObject<Item> RED_ONION = ITEMS.register("red_onion",
            () -> new RedOnionItem(RED_ONION_RECORD.activationCost(), RED_ONION_RECORD.useCost(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_FOOD,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_FOOD_RED_ONION,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3_STACKABLE
                    },
                    new Item.Properties().food(new FoodProperties.Builder()
                            .nutrition(1).saturationMod(1F)
                            .effect(() -> new MobEffectInstance(MobEffects.HUNGER, 30, 0), 1F)
                            .effect(() -> new MobEffectInstance(MobEffects.POISON, 30, 0), 0.5F)
                            .build())));
    
    public static final RegistryObject<Item> RED_ONION_EMPOWERED = ITEMS.register("red_onion_empowered",
            () -> new RedOnionEmpoweredItem(RED_ONION_RECORD.activationCost(), RED_ONION_RECORD.useCost(), 200, 1.5F,
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_FOOD_RED_ONION,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED_EMPOWERED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_FOOD_1_RED_ONION
                    },
                    new Item.Properties().stacksTo(1)
                    ));
    
    public static final RegistryObject<Item> COOKED_ONION = ITEMS.register("cooked_onion",
            () -> new CookedOnionItem(new Item.Properties().stacksTo(1)));

    public static final RegistryObject<Item> ONION_CHAKRA_LEDGER = ITEMS.register("onion_chakra_ledger",
            () -> new OnionChakraLedgerItem(new Item.Properties().stacksTo(1)));

    public static final RegistryObject<Item> SOLID_CHAKRA = ITEMS.register("solid_chakra",
            () -> new AbstractChakraGivingItem(10, 2, 3, 4, 1, new Item.Properties()));

    public static final RegistryObject<Item> SOLID_CHAKRA_DENSE = ITEMS.register("solid_chakra_dense",
            () -> new AbstractChakraGivingItem(90, 2, 1, 4, 0, new Item.Properties()));


    // ########## Tools ##########
    // pickaxes
    public static final RegistryObject<Item> ONION_PICKAXE = ITEMS.register("onion_pickaxe",
            () -> new OnionPickaxeItem(OAItemTier.BASE, 1, -2.8F,
                    TIERED_BASE.activationCost(), TIERED_BASE.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
            }));
    public static final RegistryObject<Item> ONION_PICKAXE_EMPOWERED = ITEMS.register("onion_pickaxe_empowered",
            () -> new OnionPickaxeEmpoweredItem(OAItemTier.BASE_EMPOWERED, 1,
                    -1.3F, TIERED_BASE.activationCost(), TIERED_BASE.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_TOOL
            }));

    public static final RegistryObject<Item> ONION_IRON_PICKAXE = ITEMS.register("onion_iron_pickaxe",
            () -> new OnionPickaxeIronItem(OAItemTier.IRON, 1, -2.6F,
                    TIERED_IRON.activationCost(), TIERED_IRON.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
            }));
    public static final RegistryObject<Item> ONION_IRON_PICKAXE_EMPOWERED = ITEMS.register("onion_iron_pickaxe_empowered",
            () -> new OnionPickaxeIronEmpoweredItem(OAItemTier.IRON_EMPOWERED, 1,
                    -1.1F, TIERED_IRON.activationCost(), TIERED_IRON.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_TOOL
            }));

    public static final RegistryObject<Item> ONION_GOLD_PICKAXE = ITEMS.register("onion_gold_pickaxe",
            () -> new OnionPickaxeGoldItem(OAItemTier.GOLD, 1, -2.1F,
                    TIERED_GOLD.activationCost(), TIERED_GOLD.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
            }));
    public static final RegistryObject<Item> ONION_GOLD_PICKAXE_EMPOWERED = ITEMS.register("onion_gold_pickaxe_empowered",
            () -> new OnionPickaxeGoldEmpoweredItem(OAItemTier.GOLD_EMPOWERED, 1,
                    -0.6F, TIERED_GOLD.activationCost(), TIERED_GOLD.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_TOOL
            }));

    public static final RegistryObject<Item> ONION_DIAMOND_PICKAXE = ITEMS.register("onion_diamond_pickaxe",
            () -> new OnionPickaxeDiamondItem(OAItemTier.DIAMOND, 1, -2.1F,
                    TIERED_DIAMOND.activationCost(), TIERED_DIAMOND.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
            }));
    public static final RegistryObject<Item> ONION_DIAMOND_PICKAXE_EMPOWERED = ITEMS.register("onion_diamond_pickaxe_empowered",
            () -> new OnionPickaxeDiamondEmpoweredItem(OAItemTier.DIAMOND_EMPOWERED, 1,
                    -0.6F, TIERED_DIAMOND.activationCost(), TIERED_DIAMOND.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_TOOL
            }));

    public static final RegistryObject<Item> ONION_NETHERITE_PICKAXE = ITEMS.register("onion_netherite_pickaxe",
            () -> new OnionPickaxeNetheriteItem(OAItemTier.NETHERITE, 1, -1.1F,
                    TIERED_NETHERITE.activationCost(), TIERED_NETHERITE.useCost(), new Item.Properties().fireResistant(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
            }));
    public static final RegistryObject<Item> ONION_NETHERITE_PICKAXE_EMPOWERED = ITEMS.register("onion_netherite_pickaxe_empowered",
            () -> new OnionPickaxeNetheriteEmpoweredItem(OAItemTier.NETHERITE_EMPOWERED, 1,
                    0.4F, TIERED_NETHERITE.activationCost(), TIERED_NETHERITE.useCost(), new Item.Properties().fireResistant(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_TOOL
            }));


    // shovels
    public static final RegistryObject<Item> ONION_SHOVEL = ITEMS.register("onion_shovel",
            () -> new OnionShovelItem(OAItemTier.BASE, 1.5F, -3F,
                    TIERED_BASE.activationCost(), TIERED_BASE.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
            }));
    public static final RegistryObject<Item> ONION_SHOVEL_EMPOWERED = ITEMS.register("onion_shovel_empowered",
            () -> new OnionShovelEmpoweredItem(OAItemTier.BASE_EMPOWERED, 1.5F,
                    -1.5F, TIERED_BASE.activationCost(), TIERED_BASE.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_TOOL
            }));

    public static final RegistryObject<Item> ONION_IRON_SHOVEL = ITEMS.register("onion_iron_shovel",
            () -> new OnionShovelIronItem(OAItemTier.IRON, 1.5F, -2.5F,
                    TIERED_IRON.activationCost(), TIERED_IRON.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
                    }));
    public static final RegistryObject<Item> ONION_IRON_SHOVEL_EMPOWERED = ITEMS.register("onion_iron_shovel_empowered",
            () -> new OnionShovelIronEmpoweredItem(OAItemTier.IRON_EMPOWERED, 1.5F,
                    -1F, TIERED_IRON.activationCost(), TIERED_IRON.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_TOOL
                    }));

    public static final RegistryObject<Item> ONION_GOLD_SHOVEL = ITEMS.register("onion_gold_shovel",
            () -> new OnionShovelGoldItem(OAItemTier.GOLD, 1.5F, -2F,
                    TIERED_GOLD.activationCost(), TIERED_GOLD.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
                    }));
    public static final RegistryObject<Item> ONION_GOLD_SHOVEL_EMPOWERED = ITEMS.register("onion_gold_shovel_empowered",
            () -> new OnionShovelGoldEmpoweredItem(OAItemTier.GOLD_EMPOWERED, 1.5F,
                    -0.5F, TIERED_GOLD.activationCost(), TIERED_GOLD.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_TOOL
                    }));

    public static final RegistryObject<Item> ONION_DIAMOND_SHOVEL = ITEMS.register("onion_diamond_shovel",
            () -> new OnionShovelDiamondItem(OAItemTier.DIAMOND, 1F, -2F,
                    TIERED_DIAMOND.activationCost(), TIERED_DIAMOND.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
                    }));
    public static final RegistryObject<Item> ONION_DIAMOND_SHOVEL_EMPOWERED = ITEMS.register("onion_diamond_shovel_empowered",
            () -> new OnionShovelDiamondEmpoweredItem(OAItemTier.DIAMOND_EMPOWERED, 1F,
                    -0.5F, TIERED_DIAMOND.activationCost(), TIERED_DIAMOND.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_TOOL
                    }));

    public static final RegistryObject<Item> ONION_NETHERITE_SHOVEL = ITEMS.register("onion_netherite_shovel",
            () -> new OnionShovelNetheriteItem(OAItemTier.NETHERITE, 0.5F, -1F,
                    TIERED_NETHERITE.activationCost(), TIERED_NETHERITE.useCost(), new Item.Properties().fireResistant(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
                    }));
    public static final RegistryObject<Item> ONION_NETHERITE_SHOVEL_EMPOWERED = ITEMS.register("onion_netherite_shovel_empowered",
            () -> new OnionShovelNetheriteEmpoweredItem(OAItemTier.NETHERITE_EMPOWERED, 0.5F,
                    0.5F, TIERED_NETHERITE.activationCost(), TIERED_NETHERITE.useCost(), new Item.Properties().fireResistant(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_TOOL
                    }));



    // axes
    public static final RegistryObject<Item> ONION_AXE = ITEMS.register("onion_axe",
            () -> new OnionAxeItem(OAItemTier.BASE, 6F, -3.2F,
                    TIERED_BASE.activationCost(), TIERED_BASE.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
            }));
    public static final RegistryObject<Item> ONION_AXE_EMPOWERED = ITEMS.register("onion_axe_empowered",
            () -> new OnionAxeEmpoweredItem(OAItemTier.BASE_EMPOWERED, 6F,
                    -1.7F, TIERED_BASE.activationCost(), TIERED_BASE.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_TOOL
            }));

    public static final RegistryObject<Item> ONION_IRON_AXE = ITEMS.register("onion_iron_axe",
            () -> new OnionAxeIronItem(OAItemTier.IRON, 6F, -3.1F,
                    TIERED_IRON.activationCost(), TIERED_IRON.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
                    }));
    public static final RegistryObject<Item> ONION_IRON_AXE_EMPOWERED = ITEMS.register("onion_iron_axe_empowered",
            () -> new OnionAxeIronEmpoweredItem(OAItemTier.IRON_EMPOWERED, 6F,
                    -1.2F, TIERED_IRON.activationCost(), TIERED_IRON.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_TOOL
                    }));

    public static final RegistryObject<Item> ONION_GOLD_AXE = ITEMS.register("onion_gold_axe",
            () -> new OnionAxeGoldItem(OAItemTier.GOLD, 6F, -3F,
                    TIERED_GOLD.activationCost(), TIERED_GOLD.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
                    }));
    public static final RegistryObject<Item> ONION_GOLD_AXE_EMPOWERED = ITEMS.register("onion_gold_axe_empowered",
            () -> new OnionAxeGoldEmpoweredItem(OAItemTier.GOLD_EMPOWERED, 6F,
                    -0.7F, TIERED_GOLD.activationCost(), TIERED_GOLD.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_TOOL
                    }));

    public static final RegistryObject<Item> ONION_DIAMOND_AXE = ITEMS.register("onion_diamond_axe",
            () -> new OnionAxeDiamondItem(OAItemTier.DIAMOND, 5F, -3F,
                    TIERED_DIAMOND.activationCost(), TIERED_DIAMOND.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
                    }));
    public static final RegistryObject<Item> ONION_DIAMOND_AXE_EMPOWERED = ITEMS.register("onion_diamond_axe_empowered",
            () -> new OnionAxeDiamondEmpoweredItem(OAItemTier.DIAMOND_EMPOWERED, 5F,
                    -0.7F, TIERED_DIAMOND.activationCost(), TIERED_DIAMOND.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_TOOL
                    }));

    public static final RegistryObject<Item> ONION_NETHERITE_AXE = ITEMS.register("onion_netherite_axe",
            () -> new OnionAxeNetheriteItem(OAItemTier.NETHERITE, 7F, -2F,
                    TIERED_NETHERITE.activationCost(), TIERED_NETHERITE.useCost(), new Item.Properties().fireResistant(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
                    }));
    public static final RegistryObject<Item> ONION_NETHERITE_AXE_EMPOWERED = ITEMS.register("onion_netherite_axe_empowered",
            () -> new OnionAxeNetheriteEmpoweredItem(OAItemTier.NETHERITE_EMPOWERED, 7F,
                    0.3F, TIERED_NETHERITE.activationCost(), TIERED_NETHERITE.useCost(), new Item.Properties().fireResistant(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_TOOL
                    }));


    // hoes
    public static final RegistryObject<Item> ONION_HOE = ITEMS.register("onion_hoe",
            () -> new OnionHoeItem(OAItemTier.BASE, 0, -3F,
                    TIERED_BASE.activationCost(), TIERED_BASE.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
            }));
    public static final RegistryObject<Item> ONION_HOE_EMPOWERED = ITEMS.register("onion_hoe_empowered",
            () -> new OnionHoeEmpoweredItem(OAItemTier.BASE_EMPOWERED, -2,
                    -2.5F, TIERED_BASE.activationCost(), TIERED_BASE.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_TOOL
            }));

    public static final RegistryObject<Item> ONION_IRON_HOE = ITEMS.register("onion_iron_hoe",
            () -> new OnionHoeIronItem(OAItemTier.IRON, -1, -2.5F,
                    TIERED_IRON.activationCost(), TIERED_IRON.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
                    }));
    public static final RegistryObject<Item> ONION_IRON_HOE_EMPOWERED = ITEMS.register("onion_iron_hoe_empowered",
            () -> new OnionHoeIronEmpoweredItem(OAItemTier.IRON_EMPOWERED, -2,
                    -2F, TIERED_IRON.activationCost(), TIERED_IRON.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_TOOL
                    }));

    public static final RegistryObject<Item> ONION_GOLD_HOE = ITEMS.register("onion_gold_hoe",
            () -> new OnionHoeGoldItem(OAItemTier.GOLD, -1, -2F,
                    TIERED_GOLD.activationCost(), TIERED_GOLD.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
                    }));
    public static final RegistryObject<Item> ONION_GOLD_HOE_EMPOWERED = ITEMS.register("onion_gold_hoe_empowered",
            () -> new OnionHoeGoldEmpoweredItem(OAItemTier.GOLD_EMPOWERED, -3,
                    -1.5F, TIERED_GOLD.activationCost(), TIERED_GOLD.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_TOOL
                    }));

    public static final RegistryObject<Item> ONION_DIAMOND_HOE = ITEMS.register("onion_diamond_hoe",
            () -> new OnionHoeDiamondItem(OAItemTier.DIAMOND, -1, -2F,
                    TIERED_DIAMOND.activationCost(), TIERED_DIAMOND.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
                    }));
    public static final RegistryObject<Item> ONION_DIAMOND_HOE_EMPOWERED = ITEMS.register("onion_diamond_hoe_empowered",
            () -> new OnionHoeDiamondEmpoweredItem(OAItemTier.DIAMOND_EMPOWERED, -2,
                    -1.5F, TIERED_DIAMOND.activationCost(), TIERED_DIAMOND.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_TOOL
                    }));

    public static final RegistryObject<Item> ONION_NETHERITE_HOE = ITEMS.register("onion_netherite_hoe",
            () -> new OnionHoeNetheriteItem(OAItemTier.NETHERITE, -1, -1.5F,
                    TIERED_NETHERITE.activationCost(), TIERED_NETHERITE.useCost(), new Item.Properties().fireResistant(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
                    }));
    public static final RegistryObject<Item> ONION_NETHERITE_HOE_EMPOWERED = ITEMS.register("onion_netherite_hoe_empowered",
            () -> new OnionHoeNetheriteEmpoweredItem(OAItemTier.NETHERITE_EMPOWERED, -3,
                    -1F, TIERED_NETHERITE.activationCost(), TIERED_NETHERITE.useCost(), new Item.Properties().fireResistant(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_TOOL
                    }));


    // ########## Weapons ##########
    public static final RegistryObject<Item> ONION_SWORD = ITEMS.register("onion_sword",
            () -> new OnionSwordItem(OAItemTier.BASE, 3, -2.4F,
                    TIERED_BASE.activationCost(), TIERED_BASE.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
            }));
    public static final RegistryObject<Item> ONION_SWORD_EMPOWERED = ITEMS.register("onion_sword_empowered",
            () -> new OnionSwordEmpoweredItem(OAItemTier.BASE_EMPOWERED, 3,
                    -0.9F, TIERED_BASE.activationCost(), TIERED_BASE.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_TOOL
            }));

    public static final RegistryObject<Item> ONION_IRON_SWORD = ITEMS.register("onion_iron_sword",
            () -> new OnionSwordIronItem(OAItemTier.IRON, 3, -1.9F,
                    TIERED_IRON.activationCost(), TIERED_IRON.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
                    }));
    public static final RegistryObject<Item> ONION_IRON_SWORD_EMPOWERED = ITEMS.register("onion_iron_sword_empowered",
            () -> new OnionSwordIronEmpoweredItem(OAItemTier.IRON_EMPOWERED, 3,
                    -0.4F, TIERED_IRON.activationCost(), TIERED_IRON.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_TOOL
                    }));

    public static final RegistryObject<Item> ONION_GOLD_SWORD = ITEMS.register("onion_gold_sword",
            () -> new OnionSwordGoldItem(OAItemTier.GOLD, 3, -1.4F,
                    TIERED_GOLD.activationCost(), TIERED_GOLD.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
                    }));
    public static final RegistryObject<Item> ONION_GOLD_SWORD_EMPOWERED = ITEMS.register("onion_gold_sword_empowered",
            () -> new OnionSwordGoldEmpoweredItem(OAItemTier.GOLD_EMPOWERED, 3,
                    0.1F, TIERED_GOLD.activationCost(), TIERED_GOLD.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_TOOL
                    }));

    public static final RegistryObject<Item> ONION_DIAMOND_SWORD = ITEMS.register("onion_diamond_sword",
            () -> new OnionSwordDiamondItem(OAItemTier.DIAMOND, 3, -1.4F,
                    TIERED_DIAMOND.activationCost(), TIERED_DIAMOND.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
                    }));
    public static final RegistryObject<Item> ONION_DIAMOND_SWORD_EMPOWERED = ITEMS.register("onion_diamond_sword_empowered",
            () -> new OnionSwordDiamondEmpoweredItem(OAItemTier.DIAMOND_EMPOWERED, 3,
                    0.1F, TIERED_DIAMOND.activationCost(), TIERED_DIAMOND.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_TOOL
                    }));

    public static final RegistryObject<Item> ONION_NETHERITE_SWORD = ITEMS.register("onion_netherite_sword",
            () -> new OnionSwordNetheriteItem(OAItemTier.NETHERITE, 4, -0.9F,
                    TIERED_NETHERITE.activationCost(), TIERED_NETHERITE.useCost(), new Item.Properties().fireResistant(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
                    }));
    public static final RegistryObject<Item> ONION_NETHERITE_SWORD_EMPOWERED = ITEMS.register("onion_netherite_sword_empowered",
            () -> new OnionSwordNetheriteEmpoweredItem(OAItemTier.NETHERITE_EMPOWERED, 4,
                    0.6F, TIERED_NETHERITE.activationCost(), TIERED_NETHERITE.useCost(), new Item.Properties().fireResistant(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_TOOL
                    }));


    // ########## Armours ##########
    // base
    public static final RegistryObject<ArmorItem> ONION_HELMET = ITEMS.register("onion_helmet",
            () -> new OnionHelmetItem(OAArmourMaterials.BASE,
                    TIERED_BASE.activationCost(), TIERED_BASE.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
            }));
    public static final RegistryObject<ArmorItem> ONION_HELMET_EMPOWERED = ITEMS.register("onion_helmet_empowered",
            () -> new OnionHelmetEmpoweredItem(OAArmourMaterials.BASE_EMPOWERED,
                    TIERED_BASE.activationCost(), TIERED_BASE.useCost(), 3, new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ARMOUR
            }));

    public static final RegistryObject<ArmorItem> ONION_CHESTPLATE = ITEMS.register("onion_chestplate",
            () -> new OnionChestplateItem(OAArmourMaterials.BASE,
                    TIERED_BASE.activationCost(), TIERED_BASE.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
            }));
    public static final RegistryObject<ArmorItem> ONION_CHESTPLATE_EMPOWERED = ITEMS.register("onion_chestplate_empowered",
            () -> new OnionChestplateEmpoweredItem(OAArmourMaterials.BASE_EMPOWERED,
                    TIERED_BASE.activationCost(), TIERED_BASE.useCost(), 2, new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ARMOUR
            }));

    public static final RegistryObject<ArmorItem> ONION_LEGGINGS = ITEMS.register("onion_leggings",
            () -> new OnionLeggingsItem(OAArmourMaterials.BASE,
                    TIERED_BASE.activationCost(), TIERED_BASE.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
            }));
    public static final RegistryObject<ArmorItem> ONION_LEGGINGS_EMPOWERED = ITEMS.register("onion_leggings_empowered",
            () -> new OnionLeggingsEmpoweredItem(OAArmourMaterials.BASE_EMPOWERED,
                    TIERED_BASE.activationCost(), TIERED_BASE.useCost(), 1, new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ARMOUR
            }));

    public static final RegistryObject<ArmorItem> ONION_BOOTS = ITEMS.register("onion_boots",
            () -> new OnionBootsItem(OAArmourMaterials.BASE,
                    TIERED_BASE.activationCost(), TIERED_BASE.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
            }));
    public static final RegistryObject<ArmorItem> ONION_BOOTS_EMPOWERED = ITEMS.register("onion_boots_empowered",
            () -> new OnionBootsEmpoweredItem(OAArmourMaterials.BASE_EMPOWERED,
                    TIERED_BASE.activationCost(), TIERED_BASE.useCost(), 0, new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ARMOUR
            }));
    // iron
    public static final RegistryObject<ArmorItem> ONION_IRON_HELMET = ITEMS.register("onion_iron_helmet",
            () -> new OnionHelmetIronItem(OAArmourMaterials.IRON,
                    TIERED_ARMOUR_IRON.activationCost(), TIERED_ARMOUR_IRON.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
            }));
    public static final RegistryObject<ArmorItem> ONION_IRON_HELMET_EMPOWERED = ITEMS.register("onion_iron_helmet_empowered",
            () -> new OnionHelmetIronEmpoweredItem(OAArmourMaterials.IRON_EMPOWERED,
                    TIERED_ARMOUR_IRON.activationCost(), TIERED_ARMOUR_IRON.useCost(), 3, new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ARMOUR
            }));

    public static final RegistryObject<ArmorItem> ONION_IRON_CHESTPLATE = ITEMS.register("onion_iron_chestplate",
            () -> new OnionChestplateIronItem(OAArmourMaterials.IRON,
                    TIERED_ARMOUR_IRON.activationCost(), TIERED_ARMOUR_IRON.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
            }));
    public static final RegistryObject<ArmorItem> ONION_IRON_CHESTPLATE_EMPOWERED = ITEMS.register("onion_iron_chestplate_empowered",
            () -> new OnionChestplateIronEmpoweredItem(OAArmourMaterials.IRON_EMPOWERED,
                    TIERED_ARMOUR_IRON.activationCost(), TIERED_ARMOUR_IRON.useCost(), 2, new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ARMOUR
            }));

    public static final RegistryObject<ArmorItem> ONION_IRON_LEGGINGS = ITEMS.register("onion_iron_leggings",
            () -> new OnionLeggingsIronItem(OAArmourMaterials.IRON,
                    TIERED_ARMOUR_IRON.activationCost(), TIERED_ARMOUR_IRON.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
            }));
    public static final RegistryObject<ArmorItem> ONION_IRON_LEGGINGS_EMPOWERED = ITEMS.register("onion_iron_leggings_empowered",
            () -> new OnionLeggingsIronEmpoweredItem(OAArmourMaterials.IRON_EMPOWERED,
                    TIERED_ARMOUR_IRON.activationCost(), TIERED_ARMOUR_IRON.useCost(), 1, new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ARMOUR
            }));

    public static final RegistryObject<ArmorItem> ONION_IRON_BOOTS = ITEMS.register("onion_iron_boots",
            () -> new OnionBootsIronItem(OAArmourMaterials.IRON,
                    TIERED_ARMOUR_IRON.activationCost(), TIERED_ARMOUR_IRON.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
            }));
    public static final RegistryObject<ArmorItem> ONION_IRON_BOOTS_EMPOWERED = ITEMS.register("onion_iron_boots_empowered",
            () -> new OnionBootsIronEmpoweredItem(OAArmourMaterials.IRON_EMPOWERED,
                    TIERED_ARMOUR_IRON.activationCost(), TIERED_ARMOUR_IRON.useCost(), 0, new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ARMOUR
            }));

    // gold
    public static final RegistryObject<ArmorItem> ONION_GOLD_HELMET = ITEMS.register("onion_gold_helmet",
            () -> new OnionHelmetGoldItem(OAArmourMaterials.GOLD,
                    TIERED_ARMOUR_GOLD.activationCost(), TIERED_ARMOUR_GOLD.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
            }));
    public static final RegistryObject<ArmorItem> ONION_GOLD_HELMET_EMPOWERED = ITEMS.register("onion_gold_helmet_empowered",
            () -> new OnionHelmetGoldEmpoweredItem(OAArmourMaterials.GOLD_EMPOWERED,
                    TIERED_ARMOUR_GOLD.activationCost(), TIERED_ARMOUR_GOLD.useCost(), 3, new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ARMOUR
            }));

    public static final RegistryObject<ArmorItem> ONION_GOLD_CHESTPLATE = ITEMS.register("onion_gold_chestplate",
            () -> new OnionChestplateGoldItem(OAArmourMaterials.GOLD,
                    TIERED_ARMOUR_GOLD.activationCost(), TIERED_ARMOUR_GOLD.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
            }));
    public static final RegistryObject<ArmorItem> ONION_GOLD_CHESTPLATE_EMPOWERED = ITEMS.register("onion_gold_chestplate_empowered",
            () -> new OnionChestplateGoldEmpoweredItem(OAArmourMaterials.GOLD_EMPOWERED,
                    TIERED_ARMOUR_GOLD.activationCost(), TIERED_ARMOUR_GOLD.useCost(), 2, new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ARMOUR
            }));

    public static final RegistryObject<ArmorItem> ONION_GOLD_LEGGINGS = ITEMS.register("onion_gold_leggings",
            () -> new OnionLeggingsGoldItem(OAArmourMaterials.GOLD,
                    TIERED_ARMOUR_GOLD.activationCost(), TIERED_ARMOUR_GOLD.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
            }));
    public static final RegistryObject<ArmorItem> ONION_GOLD_LEGGINGS_EMPOWERED = ITEMS.register("onion_gold_leggings_empowered",
            () -> new OnionLeggingsGoldEmpoweredItem(OAArmourMaterials.GOLD_EMPOWERED,
                    TIERED_ARMOUR_GOLD.activationCost(), TIERED_ARMOUR_GOLD.useCost(), 1, new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ARMOUR
            }));

    public static final RegistryObject<ArmorItem> ONION_GOLD_BOOTS = ITEMS.register("onion_gold_boots",
            () -> new OnionBootsGoldItem(OAArmourMaterials.GOLD,
                    TIERED_ARMOUR_GOLD.activationCost(), TIERED_ARMOUR_GOLD.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
            }));
    public static final RegistryObject<ArmorItem> ONION_GOLD_BOOTS_EMPOWERED = ITEMS.register("onion_gold_boots_empowered",
            () -> new OnionBootsGoldEmpoweredItem(OAArmourMaterials.GOLD_EMPOWERED,
                    TIERED_ARMOUR_GOLD.activationCost(), TIERED_ARMOUR_GOLD.useCost(), 0, new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ARMOUR
            }));

    // diamond
    public static final RegistryObject<ArmorItem> ONION_DIAMOND_HELMET = ITEMS.register("onion_diamond_helmet",
            () -> new OnionHelmetDiamondItem(OAArmourMaterials.DIAMOND,
                    TIERED_ARMOUR_DIAMOND.activationCost(), TIERED_ARMOUR_DIAMOND.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
            }));
    public static final RegistryObject<ArmorItem> ONION_DIAMOND_HELMET_EMPOWERED = ITEMS.register("onion_diamond_helmet_empowered",
            () -> new OnionHelmetDiamondEmpoweredItem(OAArmourMaterials.DIAMOND_EMPOWERED,
                    TIERED_ARMOUR_DIAMOND.activationCost(), TIERED_ARMOUR_DIAMOND.useCost(), 3, new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ARMOUR
            }));

    public static final RegistryObject<ArmorItem> ONION_DIAMOND_CHESTPLATE = ITEMS.register("onion_diamond_chestplate",
            () -> new OnionChestplateDiamondItem(OAArmourMaterials.DIAMOND,
                    TIERED_ARMOUR_DIAMOND.activationCost(), TIERED_ARMOUR_DIAMOND.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
            }));
    public static final RegistryObject<ArmorItem> ONION_DIAMOND_CHESTPLATE_EMPOWERED = ITEMS.register("onion_diamond_chestplate_empowered",
            () -> new OnionChestplateDiamondEmpoweredItem(OAArmourMaterials.DIAMOND_EMPOWERED,
                    TIERED_ARMOUR_DIAMOND.activationCost(), TIERED_ARMOUR_DIAMOND.useCost(), 2, new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ARMOUR
            }));

    public static final RegistryObject<ArmorItem> ONION_DIAMOND_LEGGINGS = ITEMS.register("onion_diamond_leggings",
            () -> new OnionLeggingsDiamondItem(OAArmourMaterials.DIAMOND,
                    TIERED_ARMOUR_DIAMOND.activationCost(), TIERED_ARMOUR_DIAMOND.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
            }));
    public static final RegistryObject<ArmorItem> ONION_DIAMOND_LEGGINGS_EMPOWERED = ITEMS.register("onion_diamond_leggings_empowered",
            () -> new OnionLeggingsDiamondEmpoweredItem(OAArmourMaterials.DIAMOND_EMPOWERED,
                    TIERED_ARMOUR_DIAMOND.activationCost(), TIERED_ARMOUR_DIAMOND.useCost(), 1, new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ARMOUR
            }));

    public static final RegistryObject<ArmorItem> ONION_DIAMOND_BOOTS = ITEMS.register("onion_diamond_boots",
            () -> new OnionBootsDiamondItem(OAArmourMaterials.DIAMOND,
                    TIERED_ARMOUR_DIAMOND.activationCost(), TIERED_ARMOUR_DIAMOND.useCost(), new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
            }));
    public static final RegistryObject<ArmorItem> ONION_DIAMOND_BOOTS_EMPOWERED = ITEMS.register("onion_diamond_boots_empowered",
            () -> new OnionBootsDiamondEmpoweredItem(OAArmourMaterials.DIAMOND_EMPOWERED,
                    TIERED_ARMOUR_DIAMOND.activationCost(), TIERED_ARMOUR_DIAMOND.useCost(), 0, new Item.Properties(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ARMOUR
            }));

    // netherite
    public static final RegistryObject<ArmorItem> ONION_NETHERITE_HELMET = ITEMS.register("onion_netherite_helmet",
            () -> new OnionHelmetNetheriteItem(OAArmourMaterials.NETHERITE,
                    TIERED_ARMOUR_NETHERITE.activationCost(), TIERED_ARMOUR_NETHERITE.useCost(), new Item.Properties().fireResistant(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
            }));
    public static final RegistryObject<ArmorItem> ONION_NETHERITE_HELMET_EMPOWERED = ITEMS.register("onion_netherite_helmet_empowered",
            () -> new OnionHelmetNetheriteEmpoweredItem(OAArmourMaterials.NETHERITE_EMPOWERED,
                    TIERED_ARMOUR_NETHERITE.activationCost(), TIERED_ARMOUR_NETHERITE.useCost(), 3, new Item.Properties().fireResistant(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ARMOUR
            }));

    public static final RegistryObject<ArmorItem> ONION_NETHERITE_CHESTPLATE = ITEMS.register("onion_netherite_chestplate",
            () -> new OnionChestplateNetheriteItem(OAArmourMaterials.NETHERITE,
                    TIERED_ARMOUR_NETHERITE.activationCost(), TIERED_ARMOUR_NETHERITE.useCost(), new Item.Properties().fireResistant(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
            }));
    public static final RegistryObject<ArmorItem> ONION_NETHERITE_CHESTPLATE_EMPOWERED = ITEMS.register("onion_netherite_chestplate_empowered",
            () -> new OnionChestplateNetheriteEmpoweredItem(OAArmourMaterials.NETHERITE_EMPOWERED,
                    TIERED_ARMOUR_NETHERITE.activationCost(), TIERED_ARMOUR_NETHERITE.useCost(), 2, new Item.Properties().fireResistant(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ARMOUR
            }));

    public static final RegistryObject<ArmorItem> ONION_NETHERITE_LEGGINGS = ITEMS.register("onion_netherite_leggings",
            () -> new OnionLeggingsNetheriteItem(OAArmourMaterials.NETHERITE,
                    TIERED_ARMOUR_NETHERITE.activationCost(), TIERED_ARMOUR_NETHERITE.useCost(), new Item.Properties().fireResistant(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
            }));
    public static final RegistryObject<ArmorItem> ONION_NETHERITE_LEGGINGS_EMPOWERED = ITEMS.register("onion_netherite_leggings_empowered",
            () -> new OnionLeggingsNetheriteEmpoweredItem(OAArmourMaterials.NETHERITE_EMPOWERED,
                    TIERED_ARMOUR_NETHERITE.activationCost(), TIERED_ARMOUR_NETHERITE.useCost(), 1, new Item.Properties().fireResistant(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ARMOUR
            }));

    public static final RegistryObject<ArmorItem> ONION_NETHERITE_BOOTS = ITEMS.register("onion_netherite_boots",
            () -> new OnionBootsNetheriteItem(OAArmourMaterials.NETHERITE,
                    TIERED_ARMOUR_NETHERITE.activationCost(), TIERED_ARMOUR_NETHERITE.useCost(), new Item.Properties().fireResistant(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_2,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERABLE_ITEM_3
            }));
    public static final RegistryObject<ArmorItem> ONION_NETHERITE_BOOTS_EMPOWERED = ITEMS.register("onion_netherite_boots_empowered",
            () -> new OnionBootsNetheriteEmpoweredItem(OAArmourMaterials.NETHERITE_EMPOWERED,
                    TIERED_ARMOUR_NETHERITE.activationCost(), TIERED_ARMOUR_NETHERITE.useCost(), 0, new Item.Properties().fireResistant(),
                    new String[] {
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM,
                            ItemHoverTranslatables.EMPOWERED_ITEM_DROPPED,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ITEM_1,
                            ItemHoverTranslatables.ITEM_ONION_EMPOWERED_ARMOUR
            }));


    public static void register(IEventBus eventBus) {
        ITEMS.register(eventBus);
    }

}
