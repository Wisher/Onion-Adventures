/*
 * Copyright (c) 2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.client.curio;

import net.aliphaticus.onionadventures.OnionAdventures;
import net.aliphaticus.onionadventures.client.curio.model.HeadModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.client.event.EntityRenderersEvent;

import java.util.function.Supplier;

public class CurioLayers {

    public static final ModelLayerLocation CHAKRA_MONOCLE = createLayerLocation("chakra_monocle");

    public static ModelLayerLocation createLayerLocation(String name) {
        return new ModelLayerLocation(new ResourceLocation(OnionAdventures.MODID, name), name);
    }

    private static Supplier<LayerDefinition> layer(MeshDefinition mesh, int textureWidth, int textureHeight) {
        return () -> LayerDefinition.create(mesh, textureWidth, textureHeight);
    }

    private static void register(EntityRenderersEvent.RegisterLayerDefinitions event,
                                 ModelLayerLocation layerLocation,
                                 Supplier<LayerDefinition> layer) {
        event.registerLayerDefinition(layerLocation, layer);
    }

    public static void register(EntityRenderersEvent.RegisterLayerDefinitions event) {
        register(event, CHAKRA_MONOCLE, layer(HeadModel.createMonocle(), 16, 16));
    }

}
