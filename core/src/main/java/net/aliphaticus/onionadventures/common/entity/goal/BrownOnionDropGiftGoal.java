/*
 * Copyright (c) 2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.common.entity.goal;

import net.aliphaticus.onionadventures.common.entity.custom.mobs.BrownOnionEntity;
import net.aliphaticus.onionadventures.misc.EnchantmentHolder;
import net.minecraft.network.chat.Component;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.item.*;
import net.minecraft.world.item.alchemy.Potion;
import net.minecraft.world.item.alchemy.PotionUtils;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.item.enchantment.EnchantmentHelper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class BrownOnionDropGiftGoal extends Goal {

    private final BrownOnionEntity entity;
    private final int cooldown;
    private final int minFriendliness;
    private final float chanceToDrop;
    private final HashMap<Item, Integer> items = new HashMap<>();
    private final HashMap<EnchantmentHolder, Integer> enchantments = new HashMap<>();
    private final HashMap<Potion, Integer> potions = new HashMap<>();

    private int cooldownRemaining;
    private boolean canDropGift = false;
    private int itemI;
    private List<Item> validItems;

    public BrownOnionDropGiftGoal(BrownOnionEntity entity, int cooldown, int minFriendliness, float chanceToDrop,
                                  HashMap<Item, Integer> items, HashMap<EnchantmentHolder, Integer> enchantments,
                                  HashMap<Potion, Integer> potions) {
        this.entity = entity;
        this.cooldown = cooldown;
        this.minFriendliness = minFriendliness;
        this.chanceToDrop = chanceToDrop;
        this.items.putAll(items);
        this.enchantments.putAll(enchantments);
        this.potions.putAll(potions);

        setCooldownRemaining();
    }

    @Override
    public boolean canUse() {
        // if friendliness is high enough, reduce cooldown
        if (this.entity.getFriendliness() >= minFriendliness) {
            // if cooldown is 0, can try to drop a gift
            if (--this.cooldownRemaining <= 0) {
                setCooldownRemaining();
                this.canDropGift = true;
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean canContinueToUse() {
        return false;
    }

    @Override
    public void start() {
        // make a list of items that have a mapped friendliness value less than or equal to the onions friendliness
        validItems = this.items.keySet().stream().filter(item -> {
                    return this.items.get(item) <= this.entity.getFriendliness();
                }
        ).collect(Collectors.toList());

        setRandomItem();
    }

    @Override
    public void stop() {
        this.validItems = null;
        this.canDropGift = false;
    }

    @Override
    public void tick() {
        if (canDropGift) {
            if (this.entity.level.getRandom().nextFloat() < this.chanceToDrop) {
                this.entity.level.addFreshEntity(new ItemEntity(
                        this.entity.level,
                        this.entity.getX(),
                        this.entity.getY(),
                        this.entity.getZ(),
                        getGift()
                ));
            }
        }
    }

    private void setRandomItem() {
        int max = this.validItems.size();
        Random random = new Random();
        this.itemI = random.nextInt(max);
    }

    private void setCooldownRemaining() {
        int max = 110;
        int min = 90;
        Random random = new Random();
        float modifier = ((float) (random.nextInt(max - min) + min)) / 100;
        this.cooldownRemaining = Math.round(cooldown * modifier);
    }

    private ItemStack getGift() {
        if (this.validItems.get(itemI) == Items.ENCHANTED_BOOK) {
            ItemStack book = new ItemStack(Items.ENCHANTED_BOOK);

            // apply a random enchantment that is mapped to a friendliness less than or equal to the onions friendliness
            List<EnchantmentHolder> validEnchantments = this.enchantments.keySet().stream()
                    .filter(enchantmentHolder -> {
                                return this.enchantments.get(enchantmentHolder) <= this.entity.getFriendliness();
                            }
                    ).collect(Collectors.toList());

            EnchantmentHelper.setEnchantments(getRandomEnchantment(validEnchantments), book);

            return book;

        } else if (this.validItems.get(itemI) instanceof PotionItem potionItem) {
            List<Potion> validPotions = this.potions.keySet().stream()
                    .filter(potion -> {
                        return this.potions.get(potion) <= this.entity.getFriendliness();
                    }).collect(Collectors.toList());

            ItemStack potion = new ItemStack(potionItem);
            PotionUtils.setCustomEffects(potion, getRandomPotion(validPotions));
            potion.setHoverName(Component.literal(getPotionName(potion)));
            return potion;
        } else {
            return new ItemStack(this.validItems.get(itemI));
        }
    }

    private Map<Enchantment, Integer> getRandomEnchantment(List<EnchantmentHolder> enchantments) {
        // get random enchantment
        int max = enchantments.size();
        Random random = new Random();
        EnchantmentHolder enchantmentHolder = enchantments.get(random.nextInt(max));

        // set the enchantment
        Map<Enchantment, Integer> enchantment = new HashMap<>();
        enchantment.put(enchantmentHolder.enchantment(), enchantmentHolder.level());

        return enchantment;
    }

    private List<MobEffectInstance> getRandomPotion(List<Potion> potions) {
        int max = potions.size();
        Random random = new Random();
        return potions.get(random.nextInt(max)).getEffects();
    }

    private String getPotionName(ItemStack potion) {
        String prefix;
        String middle = " of ";
        StringBuilder suffix = new StringBuilder();

        if (potion.getItem() instanceof LingeringPotionItem) {
            prefix = "Lingering Potion";
        } else if (potion.getItem() instanceof SplashPotionItem) {
            prefix = "Splash Potion";
        } else {
            prefix = "Potion";
        }

        // Strip MobEffect DescriptionID down to just the name of the effect, capitalising each word
        Pattern suffixPattern = Pattern.compile("\\w+$");
        Matcher suffixMatch = suffixPattern.matcher(PotionUtils.getMobEffects(potion).get(0).getDescriptionId());
        if (suffixMatch.find()) {

            suffix = new StringBuilder(suffixMatch.group());

            // capitalise the first letter
            suffix = new StringBuilder(suffix.substring(0, 1).toUpperCase() + suffix.substring(1));

            // capitalise following words if they exist
            String suffixX = suffix.toString();
            Pattern suffixExtPattern = Pattern.compile("[a-zA-Z]+");
            Matcher suffixExtMatch = suffixExtPattern.matcher(suffixX);
            int matches = 0;
            while (suffixExtMatch.find()) {
                if (matches > 0) {
                    String temp = suffixExtMatch.group();
                    suffix.append(" ").append(temp.substring(0, 1).toUpperCase()).append(temp.substring(1));
                } else {
                    suffix = new StringBuilder(suffixExtMatch.group());
                }
                matches++;
            }

        }

        return prefix + middle + suffix;
    }

}
