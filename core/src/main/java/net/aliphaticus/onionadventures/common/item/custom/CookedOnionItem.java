/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.common.item.custom;

import net.aliphaticus.onionadventures.client.lang.ItemHoverTranslatables;
import net.aliphaticus.onionadventures.client.lang.OnionLoreTranslatables;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.Component;
import net.minecraft.util.RandomSource;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class CookedOnionItem extends Item {

    public CookedOnionItem(Properties properties) {
        super(properties);
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void appendHoverText(@NotNull ItemStack itemStack, @Nullable Level level, @NotNull List<Component> components,
                                @NotNull TooltipFlag flag) {

        if (Screen.hasShiftDown()) {
            components.add((Component.translatable(ItemHoverTranslatables.ITEM_COOKED_ONION)));
        } else {
            components.add((Component.translatable(ItemHoverTranslatables.ITEM_HOVER_BASE_EXPANDABLE)));
        }

        super.appendHoverText(itemStack, level, components, flag);
    }


    @Override
    public @NotNull InteractionResultHolder<ItemStack> use(Level level, @NotNull Player player, @NotNull InteractionHand hand) {
        if (!level.isClientSide() && hand == InteractionHand.MAIN_HAND) {
            // output the lore
            outputRandomLore(player);

            // set the cool-down
            player.getCooldowns().addCooldown(this, 20);
        }


        return super.use(level, player, hand);
    }

    private void outputRandomLore(Player player) {
        player.sendSystemMessage(Component.translatable(getRandomLore()));
    }

    private int getRandomNumber() {
        return RandomSource.createNewThreadLocalInstance().nextInt(OnionLoreTranslatables.LORE_COUNT);
    }

    private String getRandomLore() {
        String loreString = switch (getRandomNumber()) {
            case 0 -> OnionLoreTranslatables.ONION_LORE_1;
            case 1 -> OnionLoreTranslatables.ONION_LORE_2;
            case 2 -> OnionLoreTranslatables.ONION_LORE_3;
            case 3 -> OnionLoreTranslatables.ONION_LORE_4;
            case 4 -> OnionLoreTranslatables.ONION_LORE_5;
            case 5 -> OnionLoreTranslatables.ONION_LORE_6;
            case 6 -> OnionLoreTranslatables.ONION_LORE_7;
            case 7 -> OnionLoreTranslatables.ONION_LORE_8;
            case 8 -> OnionLoreTranslatables.ONION_LORE_9;
            case 9 -> OnionLoreTranslatables.ONION_LORE_10;
            case 10 -> OnionLoreTranslatables.ONION_LORE_11;
            case 11 -> OnionLoreTranslatables.ONION_LORE_12;
            case 12 -> OnionLoreTranslatables.ONION_LORE_13;
            case 13 -> OnionLoreTranslatables.ONION_LORE_14;
            case 14 -> OnionLoreTranslatables.ONION_LORE_15;
            default -> OnionLoreTranslatables.ONION_LORE_ERROR;
        };

        return loreString;
    }

}
