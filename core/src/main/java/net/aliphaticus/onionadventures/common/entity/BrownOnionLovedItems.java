/*
 * Copyright (c) 2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.common.entity;

import net.aliphaticus.onionadventures.common.item.OAItems;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;

import java.util.HashMap;

public final class BrownOnionLovedItems {

    private static final HashMap<Item, Integer> LOVED_ITEMS = new HashMap<>();

    static {
        LOVED_ITEMS.put(new ItemStack(OAItems.SOLID_CHAKRA.get()).getItem(), 1);
        LOVED_ITEMS.put(new ItemStack(OAItems.SOLID_CHAKRA_DENSE.get()).getItem(), 9);
    }

    public static HashMap<Item, Integer> getLovedItems() {
        return LOVED_ITEMS;
    }

}
