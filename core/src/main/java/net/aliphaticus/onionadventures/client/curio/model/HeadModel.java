/*
 * Copyright (c) 2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.client.curio.model;

import com.google.common.collect.ImmutableList;
import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.core.Direction;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.LivingEntity;

import java.util.Set;
import java.util.function.Function;

public class HeadModel extends HumanoidModel<LivingEntity> {

    public HeadModel(ModelPart modelPart, Function<ResourceLocation, RenderType> renderType) {
        super(modelPart, renderType);
    }

    public HeadModel(ModelPart modelPart) {
        this(modelPart, RenderType::entityCutoutNoCull);
    }

    @Override
    protected Iterable<ModelPart> headParts() {
        return ImmutableList.of(head);
    }

    @Override
    protected Iterable<ModelPart> bodyParts() {
        return ImmutableList.of();
    }

    public static MeshDefinition createEmptyDefinition(CubeListBuilder face) {
        MeshDefinition mesh = createMesh(CubeDeformation.NONE, 0);

        mesh.getRoot().addOrReplaceChild(
                "head",
                face,
                PartPose.ZERO
        );

        return mesh;
    }

    public static MeshDefinition createMonocle() {
        CubeListBuilder face = CubeListBuilder.create();

        face.texOffs(0, 0);
        face.addBox(15.0F, -2.5F, -14.1F, -16.0F, -16.0F, 0.0F, Set.of(Direction.NORTH));

        return createEmptyDefinition(face);
    }
}
