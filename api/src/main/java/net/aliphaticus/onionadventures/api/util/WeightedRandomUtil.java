/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 *
 * Permission & a License is hereby granted to use this code under the conditions that:
 * 1) The project using this code is open-source and accessible publicly via a service such as GitHub, GitLab, etc.
 * 2) The Project using this code is copy-left under a license such as MIT, GNU General Public License v3.0, etc.
 */

package net.aliphaticus.onionadventures.api.util;

import net.minecraft.world.level.block.Block;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Utility Class for generating random results in a weighted distribution
 */
public class WeightedRandomUtil {

    /**
     * An entry in the list of entries, has a block and a weight
     * @param <T> Block
     */
    private class BlockEntry<T extends Block> {
        int weight;
        T Block;
    }

    private List<BlockEntry> entries = new ArrayList<>();
    private int accumulatedWeight = 0;
    private Random rand = new Random();


    /**
     * Add a block to the entries to be drawn from
     * @param block Block to add to the entries
     * @param weight Likely-hood of the block being picked as a weight
     * @param <T> Block
     */
    public <T extends Block> void addBlockEntry(T block, int weight) {
        accumulatedWeight += weight;
        BlockEntry<T> b = new BlockEntry<T>();
        b.Block = block;
        b.weight = accumulatedWeight;
        entries.add(b);
    }

    /**
     * Retrieve a random, weighted result from the list of entries
     * @return Block
     * @param <T> Block
     */
    public <T extends Block> T getRandomBlock() {
        int randomWeight = rand.nextInt(accumulatedWeight + 1);

        for (BlockEntry<T> entry: entries) {
            if (entry.weight >= randomWeight) {
                return entry.Block;
            }
        }
        return null; // should only occur if no entries
    }

}
