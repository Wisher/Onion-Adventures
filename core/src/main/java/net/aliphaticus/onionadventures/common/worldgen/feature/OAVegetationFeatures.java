/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.common.worldgen.feature;

import net.aliphaticus.onionadventures.common.block.OABlocks;
import net.aliphaticus.onionadventures.common.block.custom.WildOnionBlock;
import net.aliphaticus.onionadventures.common.util.worldgen.OAFeatureUtils;
import net.minecraft.core.HolderGetter;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.data.worldgen.placement.PlacementUtils;
import net.minecraft.resources.ResourceKey;
import net.minecraft.util.random.SimpleWeightedRandomList;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.configurations.FeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.RandomPatchConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.SimpleBlockConfiguration;
import net.minecraft.world.level.levelgen.feature.stateproviders.WeightedStateProvider;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;

public class OAVegetationFeatures {

    public static final ResourceKey<ConfiguredFeature<?, ?>> WILD_ONION_PLAINS = OAFeatureUtils.createKey("wild_onion_plains");
    public static final ResourceKey<ConfiguredFeature<?, ?>> WILD_ONION_COLD = OAFeatureUtils.createKey("wild_onion_cold");


    public static void bootstrap(BootstapContext<ConfiguredFeature<?,?>> context) {
        HolderGetter<PlacedFeature> placedFeatureGetter = context.lookup(Registries.PLACED_FEATURE);

        // placed feature holders
//        final Holder<PlacedFeature> TREE_KEY = placedFeatureGetter.getOrThrow(OAVegetationPlacements.TREE); // example of a placed feature holder


        // register features
        // Wild Onions
        register(context, OAVegetationFeatures.WILD_ONION_PLAINS, Feature.RANDOM_PATCH,
                new RandomPatchConfiguration(1, 2, 2, PlacementUtils.onlyWhenEmpty(
                        Feature.SIMPLE_BLOCK,
                        new SimpleBlockConfiguration(new WeightedStateProvider(
                                 new SimpleWeightedRandomList.Builder<BlockState>()
                                         .add(OABlocks.WILD_ONIONS.get().defaultBlockState().setValue(WildOnionBlock.AGE, 0), 5)
                                         .add(OABlocks.WILD_ONIONS.get().defaultBlockState().setValue(WildOnionBlock.AGE, 1), 3)
                                         .add(OABlocks.WILD_ONIONS.get().defaultBlockState().setValue(WildOnionBlock.AGE, 2), 2)

                        ))
                )));
        register(context, OAVegetationFeatures.WILD_ONION_COLD, Feature.RANDOM_PATCH,
                new RandomPatchConfiguration(1, 5, 3, PlacementUtils.onlyWhenEmpty(
                        Feature.SIMPLE_BLOCK,
                        new SimpleBlockConfiguration(new WeightedStateProvider(
                                new SimpleWeightedRandomList.Builder<BlockState>()
                                        .add(OABlocks.WILD_ONIONS.get().defaultBlockState().setValue(WildOnionBlock.AGE, 0), 8)
                                        .add(OABlocks.WILD_ONIONS.get().defaultBlockState().setValue(WildOnionBlock.AGE, 1), 2)

                        ))
                )));

    }

    private static <FC extends FeatureConfiguration, F extends Feature<FC>> void register(
            BootstapContext<ConfiguredFeature<?, ?>> context, ResourceKey<ConfiguredFeature<?, ?>> configuredFeatureKey,
            F feature, FC configuration) {
        context.register(configuredFeatureKey, new ConfiguredFeature<>(feature, configuration));
    }
}
