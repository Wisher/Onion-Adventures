## Summary

(one line summary of your ticket)

## Ticket Type

(is your ticket a: bug report, balancing suggestion, new feature suggestion, other)

## Description

(explain in detail what your ticket entails, if its a bug include steps to reproduce)

## Examples

(if relevant, link any related examples)