/*
 * Copyright (c) 2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.common.item.custom.projectile;

import net.aliphaticus.onionadventures.api.item.AbstractChakraEmpoweredItem;
import net.aliphaticus.onionadventures.api.item.IChakraEmpoweredItem;
import net.aliphaticus.onionadventures.api.item.nbt.OANBTTagNames;
import net.aliphaticus.onionadventures.client.lang.ItemHoverTranslatables;
import net.aliphaticus.onionadventures.common.entity.custom.projectile.RedOnionEmpoweredProjectile;
import net.aliphaticus.onionadventures.common.item.OAItems;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.stats.Stats;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class RedOnionEmpoweredItem extends AbstractChakraEmpoweredItem implements IChakraEmpoweredItem {

    private final int maxAge;
    private final float explosionRadius;

    public RedOnionEmpoweredItem(int activationCost, int useCost, int maxAge, float explosionRadius, String[] hoverTranslatable, Properties properties) {
        super(activationCost, useCost, hoverTranslatable, properties);

        this.maxAge = maxAge;
        this.explosionRadius = explosionRadius;
    }

    public int getMaxAge() {
        return maxAge;
    }

    public float getExplosionRadius() {
        return explosionRadius;
    }

    @Override
    public void inventoryTick(ItemStack itemStack, Level level, Entity entity, int p_41407_, boolean p_41408_) {
        super.inventoryTick(itemStack, level, entity, p_41407_, p_41408_);

        addAge(itemStack);

        if (getAge(itemStack) >= maxAge) {
            explode(level, entity, itemStack);
        }


    }

    @Override
    public boolean onEntityItemUpdate(ItemStack itemStack, ItemEntity entity) {

        addAge(itemStack);

        if (getAge(itemStack) >= maxAge) {
            explode(entity.getLevel(), entity, itemStack);
        }

        return super.onEntityItemUpdate(itemStack, entity);
    }

    @Override
    public @NotNull InteractionResultHolder<ItemStack> use(@NotNull Level level, @NotNull Player player,
                                                           @NotNull InteractionHand hand) {

        ItemStack itemStack = player.getItemInHand(hand);
        level.playSound((Player) null, player.getX(), player.getY(), player.getZ(), SoundEvents.SNOWBALL_THROW,
                SoundSource.NEUTRAL, 0.5F, 0.4F / level.getRandom().nextFloat() * 0.4F + 0.8F);
        if (!level.isClientSide()) {
            RedOnionEmpoweredProjectile redOnionProjectile = new RedOnionEmpoweredProjectile(level, player, maxAge, explosionRadius);
            redOnionProjectile.setItem(itemStack);
            redOnionProjectile.shootFromRotation(player, player.getXRot(), player.getYRot(), 0.0F, 1.5F, 1.0F);
            level.addFreshEntity(redOnionProjectile);
        }

        player.awardStat(Stats.ITEM_USED.get(this));
        if (!player.getAbilities().instabuild) {
            itemStack.shrink(1);
        }

        return InteractionResultHolder.sidedSuccess(itemStack, level.isClientSide());
    }

    @Override
    public boolean shouldCauseReequipAnimation(ItemStack oldStack, ItemStack newStack, boolean slotChanged) {
        return false;
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void appendHoverText(@NotNull ItemStack itemStack, @Nullable Level level, @NotNull List<Component> components,
                                @NotNull TooltipFlag flag) {

        if (Screen.hasShiftDown()) {
            addToShiftHoverText(components);
        } else {
            components.add((Component.translatable(ItemHoverTranslatables.ITEM_HOVER_BASE_EXPANDABLE)));
        }

        super.appendHoverText(itemStack, level, components, flag);
    }

    @Override
    public ItemStack createUpdatedItem(int damage) {
        return new ItemStack(OAItems.RED_ONION.get());
    }

    private void addAge(ItemStack itemStack) {

        if (itemStack.getTag() == null) {
            CompoundTag nbt = new CompoundTag();
            nbt.putInt(OANBTTagNames.AGE, 1);
            itemStack.setTag(nbt);
        }

        if (!itemStack.getTag().contains(OANBTTagNames.AGE)) {
            CompoundTag nbt = itemStack.getTag();
            nbt.putInt(OANBTTagNames.AGE, 1);
            itemStack.setTag(nbt);
        } else {
            CompoundTag nbt = itemStack.getTag();
            int age = nbt.getInt(OANBTTagNames.AGE) + 1;
            nbt.putInt(OANBTTagNames.AGE, age);
            itemStack.setTag(nbt);
        }

    }

    private int getAge(ItemStack itemStack) {
        return itemStack.getTag().getInt(OANBTTagNames.AGE);
    }

    private void explode(Level level, Entity entity, ItemStack itemStack) {
        level.explode(null, entity.getX(), entity.getY(), entity.getZ(), explosionRadius, false, Level.ExplosionInteraction.BLOCK);
        itemStack.shrink(1);
    }
}
