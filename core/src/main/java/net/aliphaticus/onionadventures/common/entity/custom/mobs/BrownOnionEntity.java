/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.common.entity.custom.mobs;

import net.aliphaticus.onionadventures.api.entity.goal.ApproachLivingEntityGoal;
import net.aliphaticus.onionadventures.api.util.PlayerChakraUtils;
import net.aliphaticus.onionadventures.common.entity.BrownOnionGiftEnchantments;
import net.aliphaticus.onionadventures.common.entity.BrownOnionGiftItems;
import net.aliphaticus.onionadventures.common.entity.BrownOnionGiftPotionEffects;
import net.aliphaticus.onionadventures.common.entity.BrownOnionLovedItems;
import net.aliphaticus.onionadventures.common.entity.goal.*;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.tags.DamageTypeTags;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.AgeableMob;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.*;
import net.minecraft.world.entity.ai.goal.target.NearestAttackableTargetGoal;
import net.minecraft.world.entity.animal.Animal;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.level.Level;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import software.bernie.geckolib.animatable.GeoEntity;
import software.bernie.geckolib.core.animatable.GeoAnimatable;
import software.bernie.geckolib.core.animatable.instance.AnimatableInstanceCache;
import software.bernie.geckolib.core.animatable.instance.SingletonAnimatableInstanceCache;
import software.bernie.geckolib.core.animation.*;
import software.bernie.geckolib.core.object.PlayState;

public class BrownOnionEntity extends Animal implements GeoEntity {

    // TODO: add friendliness int, increases based on goals, affects things
    private static final EntityDataAccessor<Integer> FRIENDLINESS = SynchedEntityData.defineId(BrownOnionEntity.class, EntityDataSerializers.INT);
    private static final EntityDataAccessor<Integer> FRIENDLINESS_GAUGE = SynchedEntityData.defineId(BrownOnionEntity.class, EntityDataSerializers.INT);
    private final int CHAKRA = 5;
    private final int FRIENDLINESS_GAUGE_START = 10;

    private int friendlinessGaugeCap = FRIENDLINESS_GAUGE_START;

    private AnimatableInstanceCache cache = new SingletonAnimatableInstanceCache(this);

    public BrownOnionEntity(EntityType<? extends Animal> entityType, Level level) {
        super(entityType, level);
        this.xpReward = 2;
    }

    public static AttributeSupplier setAttributes() {
        return Animal.createMobAttributes()
                .add(Attributes.MAX_HEALTH, 15D)
                .add(Attributes.ATTACK_DAMAGE, 3.0F)
                .add(Attributes.ATTACK_SPEED, 1.5F)
                .add(Attributes.ATTACK_KNOCKBACK, 1D)
                .add(Attributes.MOVEMENT_SPEED, 0.2F)
                .build();
    }

    @Override
    protected void registerGoals() {

        this.goalSelector.addGoal(1, new FloatGoal(this));
        this.goalSelector.addGoal(2, new MeleeAttackGoal(this, 1.2D, false));
        this.goalSelector.addGoal(4, new WaterAvoidingRandomStrollGoal(this, 1.0D));
        this.goalSelector.addGoal(5, new RandomLookAroundGoal(this));

        // take items to increase friendliness
        this.goalSelector.addGoal(6, new BrownOnionPickupLovedItemGoal(this, 200, BrownOnionLovedItems.getLovedItems()));

        // group up and seek red onions
        this.goalSelector.addGoal(7, new FollowMobGoal(this, 1.0D, 2.0F, 32F));
        this.goalSelector.addGoal(8, new ApproachLivingEntityGoal<RedOnionEntity>(
                this, RedOnionEntity.class, 1.0F, 12.0F, 32.0F, 64.0F,
                0.00016F)); // 1 / (20ticks x 600s) ie on average once every 5m

        // heal randomly
        this.goalSelector.addGoal(9, new HealMobGoal(this, 3.0F, 0.005F));

        // drop gifts
        this.goalSelector.addGoal(10, new BrownOnionDropGiftGoal(
                this,
                6000,
                2,
                0.25F,
                BrownOnionGiftItems.getGiftItems(),
                BrownOnionGiftEnchantments.getGiftEnchantments(),
                BrownOnionGiftPotionEffects.getPotionEffects()
        ));

        // repair items
        this.goalSelector.addGoal(11, new BrownOnionRepairItemGoal(
                this,
                24000,
                2,
                3.5D,
                0.20F
        ));

        // grant chakra
        this.goalSelector.addGoal(12, new BrownOnionGrantChakraGoal(
                this,
                5,
                6000,
                250,
                0.20F,
                3.5D
        ));

        this.targetSelector.addGoal(2, new NearestAttackableTargetGoal<>(this, RedOnionEntity.class, true));
        this.targetSelector.addGoal(3, new NearestAttackableTargetGoal<>(this, Monster.class, true));
    }

    @Override
    public boolean hurt(DamageSource damageSource, float damage) {
        if (damageSource.is(DamageTypeTags.IS_EXPLOSION)) {
            damage *= 0.55;
        }

        return super.hurt(damageSource, damage);
    }

    @Override
    public void die(DamageSource damageSource) {
        // if killed by a player
        if (damageSource.getEntity() instanceof ServerPlayer player) {
            // take chakra, 0.4% (1/250th of chakra) or 5 whichever is greater
            PlayerChakraUtils.subChakra(
                    player,
                    Math.max(
                            ((int) Math.round(PlayerChakraUtils.getChakra(player) * 0.004)),
                            CHAKRA
                    )
            );
        }

        super.die(damageSource);
    }

    @Override
    public boolean doHurtTarget(@NotNull Entity entity) {
        if (super.doHurtTarget(entity)) {
            heal();
            return true;
        }

        return false;
    }

    @Nullable
    @Override
    public AgeableMob getBreedOffspring(@NotNull ServerLevel level, @NotNull AgeableMob ageableMob) {
        return null;
    }

    public Integer getFriendliness() {
        return this.entityData.get(FRIENDLINESS);
    }

    public Integer getFriendlinessGauge() {
        return this.entityData.get(FRIENDLINESS_GAUGE);
    }

    public void setFriendliness(int amount) {
        this.entityData.set(FRIENDLINESS, amount);
    }

    public void setFriendlinessGauge(int amount) {
        this.entityData.set(FRIENDLINESS_GAUGE, amount);
    }

    public void addFriendliness(int amountToAdd) {
        this.entityData.set(FRIENDLINESS_GAUGE, getFriendlinessGauge() + amountToAdd);

        if (this.friendlinessGaugeCap == FRIENDLINESS_GAUGE_START) {
            this.friendlinessGaugeCap = getFriendliness() + FRIENDLINESS_GAUGE_START;
        }

        if (this.getFriendlinessGauge() >= friendlinessGaugeCap) {
            this.entityData.set(FRIENDLINESS, getFriendliness() + 1);
            this.entityData.set(FRIENDLINESS_GAUGE, 0);
        }
    }

    @Override
    protected void defineSynchedData() {
        super.defineSynchedData();
        this.entityData.define(FRIENDLINESS, 1);
        this.entityData.define(FRIENDLINESS_GAUGE, 0);
    }

    @Override
    public void addAdditionalSaveData(@NotNull CompoundTag nbt) {
        super.addAdditionalSaveData(nbt);
        nbt.putInt("friendliness", this.getFriendliness());
        nbt.putInt("friendliness_gauge", this.getFriendlinessGauge());
    }

    @Override
    public void readAdditionalSaveData(@NotNull CompoundTag nbt) {
        super.readAdditionalSaveData(nbt);
        this.setFriendliness(nbt.getInt("friendliness"));
        this.setFriendlinessGauge(nbt.getInt("friendliness_gauge"));
    }

    @Override
    public int getExperienceReward() {
        return this.xpReward;
    }

    @Override
    public void registerControllers(AnimatableManager.ControllerRegistrar controllerRegistrar) {
        controllerRegistrar.add(new AnimationController<>(this, "controller", 0, this::predicate));
    }

    private <T extends GeoAnimatable> PlayState predicate(AnimationState<T> tAnimationState) {
        if (tAnimationState.isMoving()) {
            tAnimationState.getController().setAnimation(RawAnimation.begin().then("animation.brown_onion.walk", Animation.LoopType.LOOP));
            return PlayState.CONTINUE;
        }

        tAnimationState.getController().setAnimation(RawAnimation.begin().then("animation.brown_onion.idle", Animation.LoopType.PLAY_ONCE));
        return PlayState.CONTINUE;
    }

    @Override
    public AnimatableInstanceCache getAnimatableInstanceCache() {
        return cache;
    }

    private void heal() {
        float amountToHeal = Math.min(5, (float) Math.round(this.getAttributeBaseValue(Attributes.ATTACK_DAMAGE) / 2));
        super.heal(amountToHeal);
    }
}
