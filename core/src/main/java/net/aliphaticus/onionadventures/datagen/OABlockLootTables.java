/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.datagen;

import net.aliphaticus.onionadventures.common.block.OABlocks;
import net.aliphaticus.onionadventures.common.block.custom.OnionCropBlock;
import net.aliphaticus.onionadventures.common.block.custom.WildOnionBlock;
import net.aliphaticus.onionadventures.common.item.OAItems;
import net.minecraft.advancements.critereon.StatePropertiesPredicate;
import net.minecraft.data.loot.BlockLootSubProvider;
import net.minecraft.util.valueproviders.UniformInt;
import net.minecraft.world.flag.FeatureFlags;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.enchantment.Enchantments;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.storage.loot.LootPool;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.entries.LootItem;
import net.minecraft.world.level.storage.loot.functions.ApplyBonusCount;
import net.minecraft.world.level.storage.loot.functions.SetItemCountFunction;
import net.minecraft.world.level.storage.loot.predicates.LootItemBlockStatePropertyCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;
import net.minecraft.world.level.storage.loot.providers.number.ConstantValue;
import net.minecraft.world.level.storage.loot.providers.number.NumberProvider;
import net.minecraft.world.level.storage.loot.providers.number.UniformGenerator;
import net.minecraftforge.registries.RegistryObject;
import org.jetbrains.annotations.NotNull;

import java.util.Set;

public class OABlockLootTables extends BlockLootSubProvider {

    protected OABlockLootTables() {
        super(Set.of(), FeatureFlags.REGISTRY.allFlags());
    }

    @Override
    protected void generate() {
        // Custom Blocks
        // Onion Crop
        add(OABlocks.ONION_CROP.get(),
                (block) -> createCropDrops(OABlocks.ONION_CROP.get(),
                        OAItems.ONION.get(), OAItems.ONION_SEEDS.get(),
                        LootItemBlockStatePropertyCondition.hasBlockStateProperties(OABlocks.ONION_CROP.get())
                                .setProperties(StatePropertiesPredicate.Builder.properties().hasProperty(OnionCropBlock.AGE, OnionCropBlock.MAX_AGE))
                )
        );

        // Wild Onions
        add(OABlocks.WILD_ONIONS.get(),
                block -> createCropDropsWildOnion(OABlocks.WILD_ONIONS.get(),
                        OAItems.ONION.get(), OAItems.ONION_SEEDS.get(),
                        LootItemBlockStatePropertyCondition.hasBlockStateProperties(OABlocks.WILD_ONIONS.get())
                                .setProperties(StatePropertiesPredicate.Builder.properties().hasProperty(WildOnionBlock.AGE, WildOnionBlock.MAX_AGE - 2)),
                        LootItemBlockStatePropertyCondition.hasBlockStateProperties(OABlocks.WILD_ONIONS.get())
                                .setProperties(StatePropertiesPredicate.Builder.properties().hasProperty(WildOnionBlock.AGE, WildOnionBlock.MAX_AGE - 1)),
                        LootItemBlockStatePropertyCondition.hasBlockStateProperties(OABlocks.WILD_ONIONS.get())
                                .setProperties(StatePropertiesPredicate.Builder.properties().hasProperty(WildOnionBlock.AGE, WildOnionBlock.MAX_AGE))
                )
        );

        // Chakra Obelisk
        dropSelf(OABlocks.CHAKRA_OBELISK.get());

    }

    private LootTable.Builder createCropDropsWildOnion(Block block, Item itemDrop, Item itemSeed, LootItemCondition.Builder builder, LootItemCondition.Builder builder1, LootItemCondition.Builder builder2) {
        return this.applyExplosionDecay(
                block, LootTable.lootTable()
                        // 1st age pools
                        .withPool(LootPool.lootPool().when(builder)
                                .add(LootItem.lootTableItem(itemDrop))
                        ).withPool(LootPool.lootPool().when(builder)
                                .add(LootItem.lootTableItem(itemSeed))
                        )
                        // 2nd age pools
                        .withPool(LootPool.lootPool().when(builder1)
                                .add(LootItem.lootTableItem(itemDrop))
                        ).withPool(LootPool.lootPool().when(builder1)
                                .add(LootItem.lootTableItem(itemSeed).apply(SetItemCountFunction.setCount(ConstantValue.exactly(2.0f))))
                        )
                        // 3rd age pools
                        .withPool(LootPool.lootPool().when(builder2)
                                .add(LootItem.lootTableItem(itemDrop)
                                        .apply(SetItemCountFunction.setCount(ConstantValue.exactly(3.0f)))
                                        .apply(ApplyBonusCount.addBonusBinomialDistributionCount(Enchantments.BLOCK_FORTUNE, 0.5714286F, 1)))
                        ).withPool(LootPool.lootPool().when(builder2)
                        .add(LootItem.lootTableItem(itemSeed)
                                .apply(SetItemCountFunction.setCount(ConstantValue.exactly(2.0f)))
                                .apply(ApplyBonusCount.addBonusBinomialDistributionCount(Enchantments.BLOCK_FORTUNE, 0.5714286F, 2)))
        ));
    }

    @Override
    protected @NotNull Iterable<Block> getKnownBlocks() {
        return OABlocks.BLOCKS.getEntries().stream().map(RegistryObject::get)::iterator;
    }

}
