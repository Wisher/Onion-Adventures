/*
 * Copyright (c) 2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 *
 * Permission & a License is hereby granted to use this code under the conditions that:
 * 1) The project using this code is open-source and accessible publicly via a service such as GitHub, GitLab, etc.
 * 2) The Project using this code is copy-left under a license such as MIT, GNU General Public License v3.0, etc.
 */

package net.aliphaticus.onionadventures.api.item.nbt;

/**
 * NBT Tag names that are commonly used
 */
public class OANBTTagNames {

    /**
     * Used when there is a unique ID
     */
    public static final String UUID = "uuid";

    /**
     * Used for when there is an age association
     */
    public static final String AGE = "age";

}
