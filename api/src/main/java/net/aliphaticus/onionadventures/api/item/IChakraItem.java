/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 *
 * Permission & a License is hereby granted to use this code under the conditions that:
 * 1) The project using this code is open-source and accessible publicly via a service such as GitHub, GitLab, etc.
 * 2) The Project using this code is copy-left under a license such as MIT, GNU General Public License v3.0, etc.
 */

package net.aliphaticus.onionadventures.api.item;

import net.aliphaticus.onionadventures.api.item.nbt.OANBTTagNames;
import net.aliphaticus.onionadventures.api.networking.OAAPIPackets;
import net.aliphaticus.onionadventures.api.networking.packet.DropInventoryItemC2SPacket;
import net.aliphaticus.onionadventures.api.networking.packet.SetInventoryItemC2SPacket;
import net.aliphaticus.onionadventures.api.util.OALoggingUtils;
import net.aliphaticus.onionadventures.api.util.PlayerChakraUtils;
import net.minecraft.ChatFormatting;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;

import java.util.List;
import java.util.UUID;

/**
 * Interface with some default methods for items that use Players Chakra
 */
public interface IChakraItem {

    /**
     * The cost for Empowering an item
     * @return cost
     */
    int getActivationCost();

    /**
     * The cost for using an Empowered item
     * @return cost
     */
    int getUseCost();

    /**
     * Whether an item is Empowered
     * @return Empowered status
     */
    boolean getEmpowered();

    /**
     * Get the Translatable Strings for appending hover text
     * @return Array of Hover Translatable Component Strings
     */
    String[] getHoverTranslatable();

    /**
     * The inverse item, e.g. empowered item if not empowered
     * @param damage damage to set on returned item
     * @return The inverted item
     */
    ItemStack createUpdatedItem(int damage);

    /**
     * Removes the old item and replaces it with the new, equivalent item
     * @param player Player who holds the item
     * @param itemSlot Slot of the original item
     * @param stack New item to replace the original
     */
    default void copyItem(Player player, int itemSlot, ItemStack stack) {
        OALoggingUtils.logDebugInfo("Setting " + player.getName().getString() + "'s slot " +
                itemSlot + " to " + stack.getDisplayName().getString());

        // check original stack and new stack counts are the same
        ItemStack originalStack = player.getInventory().getItem(itemSlot);
        int originalCount = originalStack.getCount();
        int newCount = Math.min(stack.getMaxStackSize(), stack.getCount());

        if (originalCount == newCount) {
            OAAPIPackets.sendToServer(new SetInventoryItemC2SPacket(itemSlot, stack));

        // if counts don't match
        } else {
            // set stacks to their new sizes
            stack.setCount(newCount);
            originalStack.shrink(newCount);

            // set the new stack in the selected slot and move the old stack to another slot
            // check if there's free inventory space for the old stack
            if (player.getInventory().getFreeSlot() != -1) {
                int freeSlot = player.getInventory().getFreeSlot();
                OAAPIPackets.sendToServer(new SetInventoryItemC2SPacket(freeSlot, originalStack));

            // if there's no room in inventory, drop the original stack
            } else {
                OAAPIPackets.sendToServer(new DropInventoryItemC2SPacket(originalStack));
            }

            // add the new stack in the original slot
            OAAPIPackets.sendToServer(new SetInventoryItemC2SPacket(itemSlot, stack));
        }

    }

    /**
     * Inverts the Empowered status of an item
     * @param player Player to update the item on
     * @param stack Original item
     */
    default void updateItem(Player player, ItemStack stack) {
        OALoggingUtils.logDebugInfo("Attempting to Dis/Empower " + player.getName().getString() + "'s " +
                stack.getDisplayName().getString());

        // get item stack (for inv slot and updates)
        int damage = stack.getDamageValue();
        ItemStack stack1 = createUpdatedItem(damage);
        stack1.setCount(stack.getCount());
        copyOrSetUUID(stack1, stack);
        int itemSlot = findSlotMatchingItemStackCount(player.getInventory(), stack);

        // dis/empower tool
        // if empowering, check chakra
        if (!getEmpowered()) {
            // calculate empower cost
            int empowerCost = 0;

            if (stack.getCount() > stack1.getMaxStackSize()) {
                empowerCost = stack1.getMaxStackSize() * getActivationCost();
            } else {
                empowerCost = stack.getCount() * getActivationCost();
            }

            if (hasEnoughChakra(empowerCost + getUseCost(), (ServerPlayer) player)) {
                // if has enough chakra to empower
                PlayerChakraUtils.subChakra((ServerPlayer) player, empowerCost);

                // update finalStack as item has changed
                copyItem(player, itemSlot, stack1);
            } else {
                // add cool-down if failed to empower
                player.getCooldowns().addCooldown(stack.getItem(), 20);
            }
        } else {
            // dis-empower tool / update finalStack as item has changed
            copyItem(player, itemSlot, stack1);
        }

        // add cool-down
        player.getCooldowns().addCooldown(stack1.getItem(), 20);
    }


    /**
     * Checks if Player Chakra is equal to or greater than the cost supplied
     * @param cost to use item
     * @param player player to check the chakra of
     * @return true if enough Chakra present

     */
    default boolean hasEnoughChakra(int cost, ServerPlayer player) {
        // check chakra
        return PlayerChakraUtils.getChakra(player) >= cost;
    }

    /**
     * Text to append to the hover text when holding shift
     * <br>For the third and fourth entry  (elements 2,3), appends them into a single line with the chakra context cost in the middle
     * @param components List of text components to append
     */
    default void addToShiftHoverText(List<Component> components) {
        for (int i = 0; i < components.size(); i++) {
            // if line which says "costs x chakra for x/y purpose"
            if (i == 2) {
                components.add(
                        (Component.translatable(getHoverTranslatable()[i])
                                .append(getContextChakraCost()).withStyle(ChatFormatting.LIGHT_PURPLE)
                                .withStyle(ChatFormatting.ITALIC).withStyle(ChatFormatting.BOLD)
                                .append(Component.translatable(getHoverTranslatable()[i + 1]))
                        )
                );
                i++;

            // else is a normal line
            } else {
                components.add(
                        (Component.translatable(getHoverTranslatable()[i])
                        )
                );
            }
        }
    }

    /**
     * Gets the Activation cost for Base item or Use cost for Empowered Item
     * @return either Activation or Use Chakra cost
     */
    default String getContextChakraCost() {
        // if empowered
        if (this instanceof IChakraEmpoweredItem iChakraEmpoweredItem) {
            return String.valueOf(iChakraEmpoweredItem.getUseCost());

        // if not empowered
        } else {
            return String.valueOf(this.getActivationCost());
        }
    }

    /**
     * Add NBT Tag for UUID to the ItemStack from another one or make one if it doesn't exist
     * @param finalStack ItemStack to apply uuid tag to
     * @param source ItemStack to copy UUID tag from if it exists
     */
    default void copyOrSetUUID(ItemStack finalStack, ItemStack source) {
        // if a non-stackable item
        if (source.getMaxStackSize() == 1) {

            // is no uuid tag exists
            if (!source.getTag().contains(OANBTTagNames.UUID)) {

                CompoundTag nbt = source.getTag().copy();
                nbt.putUUID(OANBTTagNames.UUID, UUID.randomUUID());
                finalStack.setTag(nbt);

            // if a uuid tag exists
            } else {
                finalStack.setTag(source.getTag().copy());
            }
        }
    }

    /**
     * Find the slot with the matching count of items
     * <br>Beware, may return -1, which is out of bounds for the inventory if no matching stack is found
     * @param inventory Inventory of the player to check
     * @param itemStack The stack to find
     * @return number of the slot or -1 if not found
     */
    default int findSlotMatchingItemStackCount(Inventory inventory, ItemStack itemStack) {
        int slot = -1;
        for (int i = 0; i < inventory.items.size(); ++i) {
            if (
                    !inventory.items.get(i).isEmpty() &&
                            ItemStack.isSameItemSameTags(itemStack, inventory.items.get(i)) &&
                            itemStack.getCount() == inventory.items.get(i).getCount()
            ) {
                slot = i;
                break;
            }
        }
        return slot;
    }

}
