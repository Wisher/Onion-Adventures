/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 *
 * Permission & a License is hereby granted to use this code under the conditions that:
 * 1) The project using this code is open-source and accessible publicly via a service such as GitHub, GitLab, etc.
 * 2) The Project using this code is copy-left under a license such as MIT, GNU General Public License v3.0, etc.
 */

package net.aliphaticus.onionadventures.api.networking;

import net.aliphaticus.onionadventures.api.OnionAdventuresApi;
import net.aliphaticus.onionadventures.api.networking.packet.*;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerPlayer;
import net.minecraftforge.network.NetworkDirection;
import net.minecraftforge.network.NetworkRegistry;
import net.minecraftforge.network.PacketDistributor;
import net.minecraftforge.network.simple.SimpleChannel;

/**
 * Class which registers the packets for the API
 */
public class OAAPIPackets {

    private static SimpleChannel INSTANCE;

    private static int packetId = 0;

    /**
     * Iterate the packet id so all packets are uniquely marked
     * @return the packet ID int
     */
    private static int id() {
        return packetId++;
    }

    /**
     * Register the types of packets, their direction, en/decoders, consumer thread
     */
    public static void register() {
        SimpleChannel net = NetworkRegistry.ChannelBuilder
                .named(new ResourceLocation(OnionAdventuresApi.MODID, "packets"))
                .networkProtocolVersion(() -> "1.1")
                .clientAcceptedVersions(s -> true)
                .serverAcceptedVersions(s -> true)
                .simpleChannel();

        INSTANCE = net;

        // C2S Packets
        // Get Chakra
        net.messageBuilder(GetChakraC2SPacket.class, id(), NetworkDirection.PLAY_TO_SERVER)
                .decoder(GetChakraC2SPacket::new)
                .encoder(GetChakraC2SPacket::toBytes)
                .consumerMainThread(GetChakraC2SPacket::handle)
                .add();

        // Add Chakra
        net.messageBuilder(AddChakraC2SPacket.class, id(), NetworkDirection.PLAY_TO_SERVER)
                .decoder(AddChakraC2SPacket::new)
                .encoder(AddChakraC2SPacket::toBytes)
                .consumerMainThread(AddChakraC2SPacket::handle)
                .add();

        // Sub Chakra
        net.messageBuilder(SubChakraC2SPacket.class, id(), NetworkDirection.PLAY_TO_SERVER)
                .decoder(SubChakraC2SPacket::new)
                .encoder(SubChakraC2SPacket::toBytes)
                .consumerMainThread(SubChakraC2SPacket::handle)
                .add();

        // Set Chakra
        net.messageBuilder(SetChakraC2SPacket.class, id(), NetworkDirection.PLAY_TO_SERVER)
                .decoder(SetChakraC2SPacket::new)
                .encoder(SetChakraC2SPacket::toBytes)
                .consumerMainThread(SetChakraC2SPacket::handle)
                .add();

        // Set Inventory Item
        net.messageBuilder(SetInventoryItemC2SPacket.class, id(), NetworkDirection.PLAY_TO_SERVER)
                .decoder(SetInventoryItemC2SPacket::new)
                .encoder(SetInventoryItemC2SPacket::toBytes)
                .consumerMainThread(SetInventoryItemC2SPacket::handle)
                .add();

        // Drop Inventory Item
        net.messageBuilder(DropInventoryItemC2SPacket.class, id(), NetworkDirection.PLAY_TO_SERVER)
                .decoder(DropInventoryItemC2SPacket::new)
                .encoder(DropInventoryItemC2SPacket::toBytes)
                .consumerMainThread(DropInventoryItemC2SPacket::handle)
                .add();


        // S2C Packets
        // Sync Chakra
        net.messageBuilder(ChakraDataSyncS2CPacket.class, id(), NetworkDirection.PLAY_TO_CLIENT)
                .decoder(ChakraDataSyncS2CPacket::new)
                .encoder(ChakraDataSyncS2CPacket::toBytes)
                .consumerMainThread(ChakraDataSyncS2CPacket::handle)
                .add();

    }

    /**
     * Send a packet to the server
     * @param packet packet to be sent
     * @param <PKT> class of the packet
     */
    public static <PKT> void sendToServer(PKT packet) {
        INSTANCE.sendToServer(packet);
    }

    /**
     * Send a packet to the player
     * @param packet packet to be sent
     * @param player Player to receive packet
     * @param <PKT> class of the packet
     */
    public static <PKT> void sendToPlayer(PKT packet, ServerPlayer player) {
        INSTANCE.send(PacketDistributor.PLAYER.with(() -> player), packet);
    }

}
