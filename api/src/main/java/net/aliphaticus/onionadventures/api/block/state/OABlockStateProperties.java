/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 *
 * Permission & a License is hereby granted to use this code under the conditions that:
 * 1) The project using this code is open-source and accessible publicly via a service such as GitHub, GitLab, etc.
 * 2) The Project using this code is copy-left under a license such as MIT, GNU General Public License v3.0, etc.
 */

package net.aliphaticus.onionadventures.api.block.state;

import net.minecraft.world.level.block.state.properties.IntegerProperty;

/**
 * Common BlockStates that are used
 * <br>Largely BlockStates not available or trusted in {@link net.minecraft.world.level.block.state.properties.BlockStateProperties BlockStateProperties}
 */
public class OABlockStateProperties {

    // Crops
    // Max Age
    /**
     * Max Age of 2 (3 stages)
     */
    public static final int MAX_AGE_2 = 2;
    /**
     * Max Age of 5 (6 stages)
     */
    public static final int MAX_AGE_5 = 5;

    // IntergerProperty Age
    /**
     * Max age of 2, minimum age of 0 (3 stages)
     */
    public static final IntegerProperty AGE_2 = IntegerProperty.create("age", 0, 2);
    /**
     * Max age of 5, minimum age of 0 (6 stages)
     */
    public static final IntegerProperty AGE_5 = IntegerProperty.create("age", 0, 5);

}
