/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.common.worldgen.placement;

import net.aliphaticus.onionadventures.common.util.worldgen.OAPlacementUtils;
import net.aliphaticus.onionadventures.common.worldgen.feature.OAVegetationFeatures;
import net.minecraft.core.Holder;
import net.minecraft.core.HolderGetter;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.data.worldgen.placement.VegetationPlacements;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraft.world.level.levelgen.placement.PlacementModifier;

import java.util.List;

public class OAVegetationPlacements {

    public static final ResourceKey<PlacedFeature> WILD_ONION_PLAINS = OAPlacementUtils.createKey("wild_onion_plains");
    public static final ResourceKey<PlacedFeature> WILD_ONION_COLD = OAPlacementUtils.createKey("wild_onion_cold");

    public static void bootstrap(BootstapContext<PlacedFeature> context) {

        HolderGetter<ConfiguredFeature<?, ?>> configuredFeatureGetter = context.lookup(Registries.CONFIGURED_FEATURE);

        // configured feature holders
        final Holder<ConfiguredFeature<?, ?>> WILD_ONION_PLAINS = configuredFeatureGetter.getOrThrow(OAVegetationFeatures.WILD_ONION_PLAINS);
        final Holder<ConfiguredFeature<?, ?>> WILD_ONION_COLD = configuredFeatureGetter.getOrThrow(OAVegetationFeatures.WILD_ONION_COLD);


        // register features
        // Wild Onions
        register(context, OAVegetationPlacements.WILD_ONION_PLAINS, WILD_ONION_PLAINS, VegetationPlacements.worldSurfaceSquaredWithCount(3));
        register(context, OAVegetationPlacements.WILD_ONION_COLD, WILD_ONION_COLD, VegetationPlacements.worldSurfaceSquaredWithCount(2));
    }

    protected static void register(BootstapContext<PlacedFeature> context, ResourceKey<PlacedFeature> placedFeatureKey,
                                   Holder<ConfiguredFeature<?, ?>> configuredFeature, PlacementModifier... modifiers) {
        register(context, placedFeatureKey, configuredFeature, List.of(modifiers));
    }

    protected static void register(BootstapContext<PlacedFeature> context, ResourceKey<PlacedFeature> placedFeatureKey,
                                   Holder<ConfiguredFeature<?, ?>> configuredFeature, List<PlacementModifier> modifiers) {
        context.register(placedFeatureKey, new PlacedFeature(configuredFeature, modifiers));
    }

}
