/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 *
 * Permission & a License is hereby granted to use this code under the conditions that:
 * 1) The project using this code is open-source and accessible publicly via a service such as GitHub, GitLab, etc.
 * 2) The Project using this code is copy-left under a license such as MIT, GNU General Public License v3.0, etc.
 */

package net.aliphaticus.onionadventures.api.networking.packet;

import net.aliphaticus.onionadventures.api.capability.chakra.PlayerOnionChakra;
import net.aliphaticus.onionadventures.api.util.PlayerChakraUtils;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.server.level.ServerPlayer;
import net.minecraftforge.network.NetworkEvent;

import java.util.function.Supplier;

/**
 * Sets the Players {@link PlayerOnionChakra PlayerOnionChakra} to the specified value
 */
public class SetChakraC2SPacket extends C2SPacket {

    /**
     * Value to set the {@link PlayerOnionChakra PlayerOnionChakra} to
     */
    private final int chakra;

    public SetChakraC2SPacket(int chakra) {
        this.chakra = chakra;
    }

    public SetChakraC2SPacket(FriendlyByteBuf buf) {
        this.chakra = buf.readInt();
    }

    @Override
    public void toBytes(FriendlyByteBuf buf) {
        buf.writeInt(chakra);
    }

    @Override
    public void handle(Supplier<NetworkEvent.Context> supplier) {
        NetworkEvent.Context context = supplier.get();
        context.enqueueWork(() -> {
           //On the server

            // get player
            ServerPlayer player = context.getSender();

            // add chakra, sync chakra
            PlayerChakraUtils.setChakra(player, chakra);
        });

        // inform network packet was handled
        context.setPacketHandled(true);
    }
}
