/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.datagen;

import net.aliphaticus.onionadventures.common.entity.OAEntities;
import net.aliphaticus.onionadventures.common.item.OAItems;
import net.minecraft.data.loot.EntityLootSubProvider;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.flag.FeatureFlags;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.storage.loot.LootPool;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.entries.LootItem;
import net.minecraft.world.level.storage.loot.functions.SetItemCountFunction;
import net.minecraft.world.level.storage.loot.functions.SetItemDamageFunction;
import net.minecraft.world.level.storage.loot.providers.number.UniformGenerator;
import net.minecraftforge.registries.RegistryObject;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.stream.Stream;

public class OAEntityLootTables extends EntityLootSubProvider {

    protected OAEntityLootTables() {
        super(FeatureFlags.REGISTRY.allFlags());
    }

    @Override
    public void generate() {

        // Onions
        // Red Onion
        add(OAEntities.RED_ONION.get(),
                LootTable.lootTable().withPool(
                        mobTableList(List.of(
                                OAItems.ONION_IRON_AXE.get(), 
                                OAItems.ONION_IRON_SWORD.get(),
                                OAItems.ONION_IRON_PICKAXE.get(),
                                OAItems.ONION_IRON_SHOVEL.get(),
                                OAItems.ONION_IRON_HOE.get(),
                                OAItems.ONION_IRON_BOOTS.get(),
                                OAItems.ONION_IRON_LEGGINGS.get(),
                                OAItems.ONION_IRON_CHESTPLATE.get(),
                                OAItems.ONION_IRON_HELMET.get(),
                                OAItems.ONION_GOLD_AXE.get(),
                                OAItems.ONION_GOLD_SWORD.get(),
                                OAItems.ONION_GOLD_PICKAXE.get(),
                                OAItems.ONION_GOLD_SHOVEL.get(),
                                OAItems.ONION_GOLD_HOE.get(),
                                OAItems.ONION_GOLD_BOOTS.get(),
                                OAItems.ONION_GOLD_LEGGINGS.get(),
                                OAItems.ONION_GOLD_CHESTPLATE.get(),
                                OAItems.ONION_GOLD_HELMET.get()
                        ), 5, OAItems.SOLID_CHAKRA.get())
                ).withPool(
                        LootPool.lootPool()
                                .add(LootItem.lootTableItem(OAItems.SOLID_CHAKRA.get()).setWeight(95)
                                        .apply(SetItemCountFunction.setCount(UniformGenerator.between(1.0f, 2.0f))))
                                .add(LootItem.lootTableItem(OAItems.SOLID_CHAKRA_DENSE.get()).setWeight(5))
                ).withPool(
                        LootPool.lootPool()
                                .add(LootItem.lootTableItem(OAItems.RED_ONION.get()))
                )
        );

        // Brown Onion
        add(OAEntities.BROWN_ONION.get(),
                LootTable.lootTable().withPool(
                        LootPool.lootPool().add(LootItem.lootTableItem(OAItems.ONION.get()))
                )
        );

    }

    private LootPool.Builder mobTableList(List<Item> items, int weight, @Nullable Item defaultItem) {
        LootPool.Builder lootPool = new LootPool.Builder();
        items.forEach(item -> {
            lootPool.add(LootItem.lootTableItem(item)
                    .apply(SetItemDamageFunction.setDamage(UniformGenerator.between(0.15F, 0.85F)))
                    .setWeight(weight)
            );
        });

        if (defaultItem != null) {
            lootPool.add(LootItem.lootTableItem(defaultItem).setWeight(items.size() * weight * 3));
        }

        return lootPool;
    }

    @Override
    protected Stream<EntityType<?>> getKnownEntityTypes() {
        return OAEntities.ENTITY_TYPES.getEntries().stream().map(RegistryObject::get);
    }
}
