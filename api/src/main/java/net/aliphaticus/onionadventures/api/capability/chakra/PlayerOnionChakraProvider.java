/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 *
 * Permission & a License is hereby granted to use this code under the conditions that:
 * 1) The project using this code is open-source and accessible publicly via a service such as GitHub, GitLab, etc.
 * 2) The Project using this code is copy-left under a license such as MIT, GNU General Public License v3.0, etc.
 */

package net.aliphaticus.onionadventures.api.capability.chakra;

import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * The Actual Capability Provider for giving the player chakra, {@link PlayerOnionChakra}
 */
public class PlayerOnionChakraProvider implements ICapabilityProvider, INBTSerializable<CompoundTag> {

    /**
     * The {@link PlayerOnionChakra} Capability
     */
    public static Capability<PlayerOnionChakra> PLAYER_ONION_CHAKRA = CapabilityManager
            .get(new CapabilityToken<PlayerOnionChakra>() {});

    /**
     * The {@link PlayerOnionChakra}, allows manipulation of the chakra value
     */
    private PlayerOnionChakra chakra = null;

    private final LazyOptional<PlayerOnionChakra> optional = LazyOptional.of(this::createPlayerOnionChakra);

    /**
     * If Chakra is null, instantiate a new object
     * @return PlayerOnionChakra Instance
     */
    private PlayerOnionChakra createPlayerOnionChakra() {
        if (this.chakra == null) {
            this.chakra = new PlayerOnionChakra();
        }

        return this.chakra;
    }


    @Override
    public @NotNull <T> LazyOptional<T> getCapability(@NotNull Capability<T> cap, @Nullable Direction side) {
        if (cap == PLAYER_ONION_CHAKRA) {
            return optional.cast();
        }

        return LazyOptional.empty();
    }


    /**
     * Save the NBT data to disk
     * @return CompoundTag
     */
    @Override
    public CompoundTag serializeNBT() {
        CompoundTag nbt = new CompoundTag();
        createPlayerOnionChakra().saveNBTData(nbt);
        return nbt;
    }

    /**
     * Retrieve the NBT data from disk
     * @param nbt CompoundTag
     */
    @Override
    public void deserializeNBT(CompoundTag nbt) {
        createPlayerOnionChakra().loadNBTData(nbt);
    }
}
