/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.datagen;

import net.aliphaticus.onionadventures.OnionAdventures;
import net.aliphaticus.onionadventures.common.block.OABlocks;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.CropBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.client.model.generators.BlockStateProvider;
import net.minecraftforge.client.model.generators.ConfiguredModel;
import net.minecraftforge.client.model.generators.ModelFile;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.registries.RegistryObject;

import java.util.function.Function;

public class OABlockStateProvider extends BlockStateProvider {

    public OABlockStateProvider(PackOutput output, ExistingFileHelper exFileHelper) {
        super(output, OnionAdventures.MODID, exFileHelper);
    }

    @Override
    protected void registerStatesAndModels() {
//        blockWithItem(ModBlocks.ONION_CROP); // example of block with an item
//        blockWithoutItem(ModBlocks.ONION_CROP); // example of a block without an item (crop doesn't work as it has states)

        // Custom Blocks
        cropBlock(OABlocks.ONION_CROP);
        cropBlockWithItem(OABlocks.WILD_ONIONS);
    }

    private void blockWithItem(RegistryObject<Block> blockRegistryObject) {
        simpleBlock(blockRegistryObject.get(), cubeAll(blockRegistryObject.get()));
        simpleBlockItem(blockRegistryObject.get(), cubeAll(blockRegistryObject.get()));
    }

    private void blockWithoutItem(RegistryObject<Block> blockRegistryObject) {
        simpleBlock(blockRegistryObject.get(), cubeAll(blockRegistryObject.get()));
    }

    private void cropBlock(RegistryObject<Block> blockRegistryObject)  {
        cropBlock(blockRegistryObject.get(), blockState -> {
            return new ModelFile(new ResourceLocation(
                    OnionAdventures.MODID, "block/" + blockRegistryObject.getId().getPath() + "_stage"
                    + blockState.getValue(((CropBlock) blockState.getBlock()).getAgeProperty())
            )) {
                @Override
                protected boolean exists() {
                    return true;
                }
            };
        });
    }

    private void cropBlockWithItem(RegistryObject<Block> blockRegistryObject) {
        cropBlock(blockRegistryObject);

        simpleBlockItem(
                blockRegistryObject.get().defaultBlockState()
                        .setValue(
                                ((CropBlock) blockRegistryObject.get()).getAgeProperty(),
                                ((CropBlock) blockRegistryObject.get()).getMaxAge()).getBlock()
                , cubeAll(blockRegistryObject.get().defaultBlockState()
                        .setValue(
                                ((CropBlock) blockRegistryObject.get()).getAgeProperty(),
                                ((CropBlock) blockRegistryObject.get()).getMaxAge()).getBlock())
        );
    }

    public void cropBlock(Block block, Function<BlockState, ModelFile> modelFunc) {
        getVariantBuilder(block)
                .forAllStates(state -> ConfiguredModel.builder()
                        .modelFile(modelFunc.apply(state))
                        .build()
                );
    }

}
