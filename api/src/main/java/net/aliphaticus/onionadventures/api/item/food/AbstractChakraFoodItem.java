/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 *
 * Permission & a License is hereby granted to use this code under the conditions that:
 * 1) The project using this code is open-source and accessible publicly via a service such as GitHub, GitLab, etc.
 * 2) The Project using this code is copy-left under a license such as MIT, GNU General Public License v3.0, etc.
 */

package net.aliphaticus.onionadventures.api.item.food;

import net.aliphaticus.onionadventures.api.capability.chakra.PlayerOnionChakra;
import net.aliphaticus.onionadventures.api.item.AbstractChakraItem;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import org.jetbrains.annotations.NotNull;

/**
 * Parent class for empower-able food items
 */
public abstract class AbstractChakraFoodItem extends AbstractChakraItem {

    /**
     * Invokes the {@link AbstractChakraItem} constructor
     * @param activationCost {@link PlayerOnionChakra PlayerOnionChakra} cost to activate
     * @param useCost {@link PlayerOnionChakra PlayerOnionChakra} cost to use
     * @param hoverTranslatable Hover text String[]
     * @param properties Item {@link net.minecraft.world.item.Item.Properties Properties}
     */
    public AbstractChakraFoodItem(int activationCost, int useCost, String[] hoverTranslatable, Properties properties) {
        super(activationCost, useCost, hoverTranslatable, properties);
    }

    @Override
    public @NotNull InteractionResultHolder<ItemStack> use(@NotNull Level level, @NotNull Player player, @NotNull InteractionHand hand) {

        // get food item
        ItemStack stack = player.getItemInHand(hand);

        // if not hungry, empower
        if (player.isShiftKeyDown()) {
            return super.use(level, player, hand);

        // else, eat
        } else {
            if (player.canEat(stack.getFoodProperties(player).canAlwaysEat())) {
                player.startUsingItem(hand);
                return InteractionResultHolder.consume(stack);
            } else {
                return InteractionResultHolder.fail(stack);
            }
        }
    }
}
