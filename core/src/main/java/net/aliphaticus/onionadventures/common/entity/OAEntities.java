/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.common.entity;

import net.aliphaticus.onionadventures.OnionAdventures;
import net.aliphaticus.onionadventures.common.entity.custom.mobs.BrownOnionEntity;
import net.aliphaticus.onionadventures.common.entity.custom.mobs.RedOnionEntity;
import net.aliphaticus.onionadventures.common.entity.custom.projectile.RedOnionEmpoweredProjectile;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobCategory;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class OAEntities {

    public static final DeferredRegister<EntityType<?>> ENTITY_TYPES =
            DeferredRegister.create(ForgeRegistries.ENTITY_TYPES, OnionAdventures.MODID);


    // ########## Custom Entities ##########

    public static final RegistryObject<EntityType<BrownOnionEntity>> BROWN_ONION =
            ENTITY_TYPES.register("brown_onion",
                    () -> EntityType.Builder.of(BrownOnionEntity::new, MobCategory.CREATURE)
                            .sized(1.0F, 1.0F)
                            .build(new ResourceLocation(OnionAdventures.MODID, "brown_onion").toString()));

    public static final RegistryObject<EntityType<RedOnionEntity>> RED_ONION =
            ENTITY_TYPES.register("red_onion",
                    () -> EntityType.Builder.of(RedOnionEntity::new, MobCategory.MONSTER)
                            .sized(1.0F, 1.0F)
                            .build(new ResourceLocation(OnionAdventures.MODID, "red_onion").toString()));


    // ########## Projectiles ##########

    public static final RegistryObject<EntityType<RedOnionEmpoweredProjectile>> RED_ONION_PROJECTILE =
            ENTITY_TYPES.register("red_onion_projectile",
                    () -> EntityType.Builder.<RedOnionEmpoweredProjectile>of(RedOnionEmpoweredProjectile::new, MobCategory.MISC)
                            .sized(0.25F, 0.25F)
                            .clientTrackingRange(4)
                            .updateInterval(10)
                            .build(new ResourceLocation(OnionAdventures.MODID, "red_onion_projectile").toString()));


    private static <T extends Entity> RegistryObject<EntityType<T>> registerEntity(String name, EntityType.Builder<T> builder) {
        return ENTITY_TYPES.register(name,
                () -> builder.build(new ResourceLocation(OnionAdventures.MODID, name).toString())
        );
    }

    public static void register(IEventBus eventBus) {
        ENTITY_TYPES.register(eventBus);
    }

}
