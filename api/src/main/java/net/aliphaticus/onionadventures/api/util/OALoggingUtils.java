/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 *
 * Permission & a License is hereby granted to use this code under the conditions that:
 * 1) The project using this code is open-source and accessible publicly via a service such as GitHub, GitLab, etc.
 * 2) The Project using this code is copy-left under a license such as MIT, GNU General Public License v3.0, etc.
 */

package net.aliphaticus.onionadventures.api.util;

import com.mojang.logging.LogUtils;
import org.slf4j.Logger;

/**
 * Logging utils for anything using this API
 */
public class OALoggingUtils {

    /**
     * The {@link Logger}
     */
    private static final Logger LOGGER = LogUtils.getLogger();

    /**
     * The current mod version
     */
    private static final String MOD_VERSION = OALoggingUtils.class.getPackage().getSpecificationVersion();

    /**
     * If the version is null (ie running from the code) or a snapshot version, log the message supplied
     * @param msg Message to log
     */
    public static void logDebugInfo(String msg) {
        if (MOD_VERSION == null || MOD_VERSION.endsWith("SNAPSHOT")) {
            LOGGER.info(msg);
        }
    }

}
