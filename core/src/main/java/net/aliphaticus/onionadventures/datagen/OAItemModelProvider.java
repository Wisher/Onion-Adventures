/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.datagen;

import net.aliphaticus.onionadventures.OnionAdventures;
import net.aliphaticus.onionadventures.common.item.OAItems;
import net.aliphaticus.onionadventures.common.item.OAItemsCurios;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.Item;
import net.minecraftforge.client.model.generators.ItemModelBuilder;
import net.minecraftforge.client.model.generators.ItemModelProvider;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.registries.RegistryObject;

public class OAItemModelProvider extends ItemModelProvider {

    public OAItemModelProvider(PackOutput output, ExistingFileHelper existingFileHelper) {
        super(output, OnionAdventures.MODID, existingFileHelper);
    }

    @Override
    protected void registerModels() {
        // Standard Items
        simpleItem(OAItems.ONION);
        simpleItem(OAItems.RED_ONION);
        simpleItem(OAItems.BUNCH_OF_ONIONS);
        simpleItem(OAItems.ONION_SEEDS);
        withExistingParent(OAItems.BROWN_ONION_SPAWN_EGG.getId().getPath(), mcLoc("item/template_spawn_egg"));
        withExistingParent(OAItems.RED_ONION_SPAWN_EGG.getId().getPath(), mcLoc("item/template_spawn_egg"));

        // Custom Items
        simpleItem(OAItems.ROASTED_ONION);
        simpleItemEmpowered(OAItems.ROASTED_ONION_EMPOWERED, OAItems.ROASTED_ONION);
        simpleItemEmpowered(OAItems.RED_ONION_EMPOWERED, OAItems.RED_ONION);
        simpleItem(OAItems.COOKED_ONION);
        simpleItem(OAItems.ONION_CHAKRA_LEDGER);
        simpleItem(OAItems.SOLID_CHAKRA);
        simpleItem(OAItems.SOLID_CHAKRA_DENSE);

        // Tools
        // base
        handheldItem(OAItems.ONION_PICKAXE);
        handheldItemEmpowered(OAItems.ONION_PICKAXE_EMPOWERED, OAItems.ONION_PICKAXE);
        handheldItem(OAItems.ONION_SHOVEL);
        handheldItemEmpowered(OAItems.ONION_SHOVEL_EMPOWERED, OAItems.ONION_SHOVEL);
        handheldItem(OAItems.ONION_AXE);
        handheldItemEmpowered(OAItems.ONION_AXE_EMPOWERED, OAItems.ONION_AXE);
        handheldItem(OAItems.ONION_HOE);
        handheldItemEmpowered(OAItems.ONION_HOE_EMPOWERED, OAItems.ONION_HOE);
        // iron
        handheldItem(OAItems.ONION_IRON_PICKAXE);
        handheldItemEmpowered(OAItems.ONION_IRON_PICKAXE_EMPOWERED, OAItems.ONION_IRON_PICKAXE);
        handheldItem(OAItems.ONION_IRON_SHOVEL);
        handheldItemEmpowered(OAItems.ONION_IRON_SHOVEL_EMPOWERED, OAItems.ONION_IRON_SHOVEL);
        handheldItem(OAItems.ONION_IRON_AXE);
        handheldItemEmpowered(OAItems.ONION_IRON_AXE_EMPOWERED, OAItems.ONION_IRON_AXE);
        handheldItem(OAItems.ONION_IRON_HOE);
        handheldItemEmpowered(OAItems.ONION_IRON_HOE_EMPOWERED, OAItems.ONION_IRON_HOE);
        // gold
        handheldItem(OAItems.ONION_GOLD_PICKAXE);
        handheldItemEmpowered(OAItems.ONION_GOLD_PICKAXE_EMPOWERED, OAItems.ONION_GOLD_PICKAXE);
        handheldItem(OAItems.ONION_GOLD_SHOVEL);
        handheldItemEmpowered(OAItems.ONION_GOLD_SHOVEL_EMPOWERED, OAItems.ONION_GOLD_SHOVEL);
        handheldItem(OAItems.ONION_GOLD_AXE);
        handheldItemEmpowered(OAItems.ONION_GOLD_AXE_EMPOWERED, OAItems.ONION_GOLD_AXE);
        handheldItem(OAItems.ONION_GOLD_HOE);
        handheldItemEmpowered(OAItems.ONION_GOLD_HOE_EMPOWERED, OAItems.ONION_GOLD_HOE);
        // diamond
        handheldItem(OAItems.ONION_DIAMOND_PICKAXE);
        handheldItemEmpowered(OAItems.ONION_DIAMOND_PICKAXE_EMPOWERED, OAItems.ONION_DIAMOND_PICKAXE);
        handheldItem(OAItems.ONION_DIAMOND_SHOVEL);
        handheldItemEmpowered(OAItems.ONION_DIAMOND_SHOVEL_EMPOWERED, OAItems.ONION_DIAMOND_SHOVEL);
        handheldItem(OAItems.ONION_DIAMOND_AXE);
        handheldItemEmpowered(OAItems.ONION_DIAMOND_AXE_EMPOWERED, OAItems.ONION_DIAMOND_AXE);
        handheldItem(OAItems.ONION_DIAMOND_HOE);
        handheldItemEmpowered(OAItems.ONION_DIAMOND_HOE_EMPOWERED, OAItems.ONION_DIAMOND_HOE);
        // netherite
        handheldItem(OAItems.ONION_NETHERITE_PICKAXE);
        handheldItemEmpowered(OAItems.ONION_NETHERITE_PICKAXE_EMPOWERED, OAItems.ONION_NETHERITE_PICKAXE);
        handheldItem(OAItems.ONION_NETHERITE_SHOVEL);
        handheldItemEmpowered(OAItems.ONION_NETHERITE_SHOVEL_EMPOWERED, OAItems.ONION_NETHERITE_SHOVEL);
        handheldItem(OAItems.ONION_NETHERITE_AXE);
        handheldItemEmpowered(OAItems.ONION_NETHERITE_AXE_EMPOWERED, OAItems.ONION_NETHERITE_AXE);
        handheldItem(OAItems.ONION_NETHERITE_HOE);
        handheldItemEmpowered(OAItems.ONION_NETHERITE_HOE_EMPOWERED, OAItems.ONION_NETHERITE_HOE);

        // Weapons
        // base
        handheldItem(OAItems.ONION_SWORD);
        handheldItemEmpowered(OAItems.ONION_SWORD_EMPOWERED, OAItems.ONION_SWORD);
        // iron
        handheldItem(OAItems.ONION_IRON_SWORD);
        handheldItemEmpowered(OAItems.ONION_IRON_SWORD_EMPOWERED, OAItems.ONION_IRON_SWORD);
        // gold
        handheldItem(OAItems.ONION_GOLD_SWORD);
        handheldItemEmpowered(OAItems.ONION_GOLD_SWORD_EMPOWERED, OAItems.ONION_GOLD_SWORD);
        // diamond
        handheldItem(OAItems.ONION_DIAMOND_SWORD);
        handheldItemEmpowered(OAItems.ONION_DIAMOND_SWORD_EMPOWERED, OAItems.ONION_DIAMOND_SWORD);
        // netherite
        handheldItem(OAItems.ONION_NETHERITE_SWORD);
        handheldItemEmpowered(OAItems.ONION_NETHERITE_SWORD_EMPOWERED, OAItems.ONION_NETHERITE_SWORD);

        // Armour
        // Base
        armourItem(OAItems.ONION_BOOTS);
        armourItemEmpowered(OAItems.ONION_BOOTS_EMPOWERED, OAItems.ONION_BOOTS);
        armourItem(OAItems.ONION_LEGGINGS);
        armourItemEmpowered(OAItems.ONION_LEGGINGS_EMPOWERED, OAItems.ONION_LEGGINGS);
        armourItem(OAItems.ONION_CHESTPLATE);
        armourItemEmpowered(OAItems.ONION_CHESTPLATE_EMPOWERED, OAItems.ONION_CHESTPLATE);
        armourItem(OAItems.ONION_HELMET);
        armourItemEmpowered(OAItems.ONION_HELMET_EMPOWERED, OAItems.ONION_HELMET);
        // Iron
        armourItem(OAItems.ONION_IRON_BOOTS);
        armourItemEmpowered(OAItems.ONION_IRON_BOOTS_EMPOWERED, OAItems.ONION_IRON_BOOTS);
        armourItem(OAItems.ONION_IRON_LEGGINGS);
        armourItemEmpowered(OAItems.ONION_IRON_LEGGINGS_EMPOWERED, OAItems.ONION_IRON_LEGGINGS);
        armourItem(OAItems.ONION_IRON_CHESTPLATE);
        armourItemEmpowered(OAItems.ONION_IRON_CHESTPLATE_EMPOWERED, OAItems.ONION_IRON_CHESTPLATE);
        armourItem(OAItems.ONION_IRON_HELMET);
        armourItemEmpowered(OAItems.ONION_IRON_HELMET_EMPOWERED, OAItems.ONION_IRON_HELMET);
        // Gold
        armourItem(OAItems.ONION_GOLD_BOOTS);
        armourItemEmpowered(OAItems.ONION_GOLD_BOOTS_EMPOWERED, OAItems.ONION_GOLD_BOOTS);
        armourItem(OAItems.ONION_GOLD_LEGGINGS);
        armourItemEmpowered(OAItems.ONION_GOLD_LEGGINGS_EMPOWERED, OAItems.ONION_GOLD_LEGGINGS);
        armourItem(OAItems.ONION_GOLD_CHESTPLATE);
        armourItemEmpowered(OAItems.ONION_GOLD_CHESTPLATE_EMPOWERED, OAItems.ONION_GOLD_CHESTPLATE);
        armourItem(OAItems.ONION_GOLD_HELMET);
        armourItemEmpowered(OAItems.ONION_GOLD_HELMET_EMPOWERED, OAItems.ONION_GOLD_HELMET);
        // Diamond
        armourItem(OAItems.ONION_DIAMOND_BOOTS);
        armourItemEmpowered(OAItems.ONION_DIAMOND_BOOTS_EMPOWERED, OAItems.ONION_DIAMOND_BOOTS);
        armourItem(OAItems.ONION_DIAMOND_LEGGINGS);
        armourItemEmpowered(OAItems.ONION_DIAMOND_LEGGINGS_EMPOWERED, OAItems.ONION_DIAMOND_LEGGINGS);
        armourItem(OAItems.ONION_DIAMOND_CHESTPLATE);
        armourItemEmpowered(OAItems.ONION_DIAMOND_CHESTPLATE_EMPOWERED, OAItems.ONION_DIAMOND_CHESTPLATE);
        armourItem(OAItems.ONION_DIAMOND_HELMET);
        armourItemEmpowered(OAItems.ONION_DIAMOND_HELMET_EMPOWERED, OAItems.ONION_DIAMOND_HELMET);
        // Netherite
        armourItem(OAItems.ONION_NETHERITE_BOOTS);
        armourItemEmpowered(OAItems.ONION_NETHERITE_BOOTS_EMPOWERED, OAItems.ONION_NETHERITE_BOOTS);
        armourItem(OAItems.ONION_NETHERITE_LEGGINGS);
        armourItemEmpowered(OAItems.ONION_NETHERITE_LEGGINGS_EMPOWERED, OAItems.ONION_NETHERITE_LEGGINGS);
        armourItem(OAItems.ONION_NETHERITE_CHESTPLATE);
        armourItemEmpowered(OAItems.ONION_NETHERITE_CHESTPLATE_EMPOWERED, OAItems.ONION_NETHERITE_CHESTPLATE);
        armourItem(OAItems.ONION_NETHERITE_HELMET);
        armourItemEmpowered(OAItems.ONION_NETHERITE_HELMET_EMPOWERED, OAItems.ONION_NETHERITE_HELMET);


        // curios
        simpleItem(OAItemsCurios.CHAKRA_MONOCLE);
    }

    private ItemModelBuilder simpleItem(RegistryObject<Item> item) {
        return withExistingParent(item.getId().getPath(),
                new ResourceLocation("item/generated")).texture("layer0",
                new ResourceLocation(OnionAdventures.MODID, "item/" + item.getId().getPath()));
    }

    private ItemModelBuilder simpleItemEmpowered(RegistryObject<Item> itemEmpowered, RegistryObject<Item> itemDisEmpowered) {
        return withExistingParent(itemEmpowered.getId().getPath(),
                new ResourceLocation("item/generated")).texture("layer0",
                new ResourceLocation(OnionAdventures.MODID, "item/" + itemDisEmpowered.getId().getPath()));
    }

    private ItemModelBuilder handheldItem(RegistryObject<Item> item) {
        return withExistingParent(item.getId().getPath(),
                new ResourceLocation("item/handheld")).texture("layer0",
                new ResourceLocation(OnionAdventures.MODID, "item/" + item.getId().getPath()));
    }

    private ItemModelBuilder handheldItemEmpowered(RegistryObject<Item> itemEmpowered, RegistryObject<Item> itemDisEmpowered) {
        return withExistingParent(itemEmpowered.getId().getPath(),
                new ResourceLocation("item/handheld")).texture("layer0",
                new ResourceLocation(OnionAdventures.MODID, "item/" + itemDisEmpowered.getId().getPath()));
    }

    private ItemModelBuilder armourItem(RegistryObject<ArmorItem> item) {
        return withExistingParent(item.getId().getPath(),
                new ResourceLocation("item/generated")).texture("layer0",
                new ResourceLocation(OnionAdventures.MODID, "item/" + item.getId().getPath()));
    }

    private ItemModelBuilder armourItemEmpowered(RegistryObject<ArmorItem> itemEmpowered, RegistryObject<ArmorItem> itemDisEmpowered) {
        return withExistingParent(itemEmpowered.getId().getPath(),
                new ResourceLocation("item/generated")).texture("layer0",
                new ResourceLocation(OnionAdventures.MODID, "item/" + itemDisEmpowered.getId().getPath()));
    }

}
