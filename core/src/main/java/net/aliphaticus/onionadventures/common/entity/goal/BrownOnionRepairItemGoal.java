/*
 * Copyright (c) 2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.common.entity.goal;

import net.aliphaticus.onionadventures.common.entity.custom.mobs.BrownOnionEntity;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.item.ItemStack;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class BrownOnionRepairItemGoal extends Goal {


    private final BrownOnionEntity entity;
    private final int cooldown;
    private final int minFriendliness;
    private final double distance;
    private final float chanceToRepair;

    private int cooldownRemaining;
    private boolean canRepair = false;
    private ServerPlayer player;
    private ItemStack itemToRepair;

    public BrownOnionRepairItemGoal(BrownOnionEntity entity, int cooldown, int minFriendliness, double distance,
                                    float chanceToRepair) {
        this.entity = entity;
        this.cooldown = cooldown;
        this.minFriendliness = minFriendliness;
        this.distance = distance;
        this.chanceToRepair = chanceToRepair;

        setCooldownRemaining();
    }

    @Override
    public boolean canUse() {

        // if the onion has enough friendliness, reduce the cooldown
        if (this.entity.getFriendliness() >= this.minFriendliness) {
            // if the cooldown is finished, the onion can try to repair when a player approaches
            if (--this.cooldownRemaining <= 0) {
                setCooldownRemaining();
                this.canRepair = true;
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean canContinueToUse() {
        return this.canRepair;
    }

    @Override
    public void stop() {
        this.player = null;
        this.itemToRepair = null;
    }

    @Override
    public void tick() {
        setPlayer();

        if (this.player != null) {
            setItemToRepair();
        }

        if (this.canRepair && this.itemToRepair != null) {

            if (this.entity.level.getRandom().nextFloat() < this.chanceToRepair) {
                repairItem();
            }

            this.canRepair = false;
        }
    }

    private void setCooldownRemaining() {
        int max = 110;
        int min = 90;
        Random random = new Random();
        float modifier = ((float) (random.nextInt(max - min) + min)) / 100;
        this.cooldownRemaining = Math.round(cooldown * modifier);
    }

    private void setPlayer() {
        List<ServerPlayer> players = this.entity.level.getEntitiesOfClass(
                ServerPlayer.class,
                this.entity.getBoundingBox().inflate(distance)
        );

        for (ServerPlayer player :
                players) {
            this.player = player;
            return;
        }
    }

    private void setItemToRepair() {
        List<ItemStack> items = this.player.getInventory().items.stream()
                .filter(itemStack -> {
                    return itemStack.getDamageValue() > 0;
                }).collect(Collectors.toList());

        if (items.size() > 0) {
            int max = items.size();
            Random random = new Random();

            this.itemToRepair = items.get(random.nextInt(max));
        }

    }

    private void repairItem() {
        // (x * (x + 1)) / 2, where friendliness is x
        int amountToRepair = (this.entity.getFriendliness() *
                (this.entity.getFriendliness() + 1)
        ) / 2;

        this.itemToRepair.setDamageValue(Math.max(0, this.itemToRepair.getDamageValue() - amountToRepair));
    }

}
