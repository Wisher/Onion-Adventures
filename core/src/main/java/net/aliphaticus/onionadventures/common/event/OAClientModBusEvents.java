/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.common.event;

import net.aliphaticus.onionadventures.OnionAdventures;
import net.aliphaticus.onionadventures.api.util.ModCompatUtils;
import net.aliphaticus.onionadventures.client.curio.CurioLayers;
import net.aliphaticus.onionadventures.client.entity.item.RedOnionProjectileRenderer;
import net.aliphaticus.onionadventures.client.entity.mob.BrownOnionRenderer;
import net.aliphaticus.onionadventures.client.entity.mob.RedOnionRenderer;
import net.aliphaticus.onionadventures.client.gui.HUDHandler;
import net.aliphaticus.onionadventures.common.entity.OAEntities;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.EntityRenderersEvent;
import net.minecraftforge.client.event.RegisterGuiOverlaysEvent;
import net.minecraftforge.client.gui.overlay.VanillaGuiOverlay;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = OnionAdventures.MODID, bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class OAClientModBusEvents {

    // register mob/item entity models
    @SubscribeEvent
    public static void registerRenderers(final EntityRenderersEvent.RegisterRenderers event) {
        // mobs
        event.registerEntityRenderer(OAEntities.BROWN_ONION.get(), BrownOnionRenderer::new);
        event.registerEntityRenderer(OAEntities.RED_ONION.get(), RedOnionRenderer::new);

        // items
        event.registerEntityRenderer(OAEntities.RED_ONION_PROJECTILE.get(), RedOnionProjectileRenderer::new);
    }

    // register curio layers
    @SubscribeEvent
    public static void registerLayerDefinitions(EntityRenderersEvent.RegisterLayerDefinitions event) {
        if (ModCompatUtils.isCuriosLoaded()) {
            CurioLayers.register(event);
        }
    }

    // register gui overlays
    @SubscribeEvent
    public static void registerGuiOverlays(RegisterGuiOverlaysEvent event) {

        // register curio GUI elements
        if (ModCompatUtils.isCuriosLoaded()) {
            event.registerAbove(VanillaGuiOverlay.EXPERIENCE_BAR.id(), "hud",
                    (gui, poseStack, partialTick, width, height) -> HUDHandler.onDrawScreenPost(poseStack));
        }

    }

}
