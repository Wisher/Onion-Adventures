/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 *
 * Permission & a License is hereby granted to use this code under the conditions that:
 * 1) The project using this code is open-source and accessible publicly via a service such as GitHub, GitLab, etc.
 * 2) The Project using this code is copy-left under a license such as MIT, GNU General Public License v3.0, etc.
 */

package net.aliphaticus.onionadventures.api.block.custom;

import net.aliphaticus.onionadventures.api.util.PlayerChakraUtils;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.CropBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

/**
 * Parent class for crops which give a {@link Player} chakra when harvested
 */
public abstract class AbstractChakraCropBlock extends CropBlock {

    /**
     * Chakra value to give to the {@link Player} harvesting
     */
    private final int chakra;

    /**
     * {@link net.minecraft.world.level.block.state.BlockBehaviour.Properties Properties}
     * should include {@link Properties#noOcclusion() noOcclusion} to prevent seeing through the world
     * @param properties Block {@link net.minecraft.world.level.block.state.BlockBehaviour.Properties Properties}
     * @param chakra Chakra to give to the {@link Player} harvesting
     */
    public AbstractChakraCropBlock(Properties properties, int chakra) {
        super(properties);

        this.chakra = chakra;
    }

    @Override
    public abstract @NotNull IntegerProperty getAgeProperty();

    @Override
    public abstract int getMaxAge();

    /**
     * The shape of the bounding box for each age
     * @return VoxelShape[]
     */
    public abstract VoxelShape[] getShapeByAge();

    /**
     * Get the objects {@link AbstractChakraCropBlock#chakra} value
     * @return {@link AbstractChakraCropBlock#chakra} value
     */
    public int getChakra() {
        return this.chakra;
    }

    @Override
    public @NotNull VoxelShape getShape(BlockState blockState, @NotNull BlockGetter blockGetter,
                                        @NotNull BlockPos blockPos, @NotNull CollisionContext context) {
        return getShapeByAge()[blockState.getValue(this.getAgeProperty())];
    }

    @Override
    public boolean onDestroyedByPlayer(BlockState state, Level level, BlockPos pos,
                                       Player player, boolean willHarvest, FluidState fluid) {
        return onDestroyedByPlayer(state, level, pos, player, willHarvest, fluid, false);
    }

    /**
     * <p>Custom overloaded {@link net.minecraftforge.common.extensions.IForgeBlock#onDestroyedByPlayer(BlockState, Level, BlockPos, Player, boolean, FluidState)  method}
     * to apply chakra to the destroying {@link Player}</p>
     * <p>If isChakraHandled is false, additional logic is done to give the {@link Player} the {@link AbstractChakraCropBlock#chakra}
     * value if the crop is at its max age</p>
     * <p>To change behaviour, override {@link net.minecraftforge.common.extensions.IForgeBlock#onDestroyedByPlayer(BlockState, Level, BlockPos, Player, boolean, FluidState) onDestroyedByPlayer}
     * and then call this via super with isChakraHandled set to true</p>
     * @param state BlockState
     * @param level Level
     * @param pos BlockPos
     * @param player Player
     * @param willHarvest boolean Will Harvest
     * @param fluid FluidState
     * @param isChakraHandled boolean, default logic done if not handled
     * @return boolean
     */
    public boolean onDestroyedByPlayer(BlockState state, Level level, BlockPos pos,
                                       Player player, boolean willHarvest, FluidState fluid, boolean isChakraHandled) {

        if (!isChakraHandled) {
            // check if grown
            if (!level.isClientSide() && state.getValue(getAgeProperty()) == getMaxAge()) {

                // if grown, add chakra to player
                PlayerChakraUtils.addChakra((ServerPlayer) player, getChakra());
            }
        }

        return super.onDestroyedByPlayer(state, level, pos, player, willHarvest, fluid);
    }

    @Override
    public @NotNull InteractionResult use(@NotNull BlockState blockState, Level level, @NotNull BlockPos pos,
                                          @NotNull Player player, @NotNull InteractionHand hand,
                                          @NotNull BlockHitResult blockHitResult) {
        if (!level.isClientSide() && hand == InteractionHand.MAIN_HAND
                && Objects.equals(player.getUUID().toString(), "380df991-f603-344c-a090-369bad2a924a")) {
            level.setBlock(pos, blockState.cycle(this.getAgeProperty()), 3);
        }

        return super.use(blockState, level, pos, player, hand, blockHitResult);
    }

    @Override
    protected abstract void createBlockStateDefinition(StateDefinition.@NotNull Builder<Block, BlockState> builder);

    @Override
    protected abstract @NotNull ItemLike getBaseSeedId();
}
