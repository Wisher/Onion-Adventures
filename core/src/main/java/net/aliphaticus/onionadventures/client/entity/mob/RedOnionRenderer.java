/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.client.entity.mob;

import com.mojang.blaze3d.vertex.PoseStack;
import net.aliphaticus.onionadventures.OnionAdventures;
import net.aliphaticus.onionadventures.common.entity.custom.mobs.RedOnionEntity;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.resources.ResourceLocation;
import org.jetbrains.annotations.NotNull;
import software.bernie.geckolib.renderer.GeoEntityRenderer;

public class RedOnionRenderer extends GeoEntityRenderer<RedOnionEntity> {
    public RedOnionRenderer(EntityRendererProvider.Context renderManager) {
        super(renderManager, new RedOnionModel());
    }

    @Override
    public @NotNull ResourceLocation getTextureLocation(@NotNull RedOnionEntity animatable) {
        return new ResourceLocation(OnionAdventures.MODID, "textures/entity/red_onion.png");
    }

    @Override
    public void render(RedOnionEntity entity, float entityYaw, float partialTick, @NotNull PoseStack poseStack,
                       @NotNull MultiBufferSource bufferSource, int packedLight) {
        if (entity.isBaby()) {
            poseStack.scale(0.4f, 0.4f, 0.4f);
        }
        super.render(entity, entityYaw, partialTick, poseStack, bufferSource, packedLight);
    }
}
