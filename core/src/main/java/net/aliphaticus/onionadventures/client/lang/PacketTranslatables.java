/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.client.lang;

public final class PacketTranslatables {

    /**
     * Prepended to the message informing player of their chakra
     */
    public static final String CHAKRA_REMAINING = "packetmsg.onionadventures.chakra_remaining";

}
