/*
 * Copyright (c) 2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.common.item;

import net.aliphaticus.onionadventures.OnionAdventures;
import net.aliphaticus.onionadventures.common.item.custom.curio.ChakraMonocleItem;
import net.minecraft.world.item.Item;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class OAItemsCurios {

    public static final DeferredRegister<Item> CURIOS_ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, OnionAdventures.MODID);

    public static final RegistryObject<Item> CHAKRA_MONOCLE = CURIOS_ITEMS.register("chakra_monocle",
            () -> new ChakraMonocleItem(new Item.Properties().stacksTo(1)));



    public static void register(IEventBus eventBus) {
        CURIOS_ITEMS.register(eventBus);
    }

}
