/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.common.block.custom;

import net.aliphaticus.onionadventures.api.capability.chakra.PlayerOnionChakraProvider;
import net.aliphaticus.onionadventures.api.util.PlayerChakraUtils;
import net.aliphaticus.onionadventures.client.lang.ItemHoverTranslatables;
import net.aliphaticus.onionadventures.client.lang.MessageTranslatables;
import net.aliphaticus.onionadventures.common.item.OAItems;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.HorizontalDirectionalBlock;
import net.minecraft.world.level.block.Mirror;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.BooleanOp;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.stream.Stream;

public class ChakraObeliskBlock extends HorizontalDirectionalBlock {

    public static final DirectionProperty FACING = BlockStateProperties.HORIZONTAL_FACING;

    private static final VoxelShape SHAPE = Stream.of(
            Block.box(0, 0, 0, 16, 4, 16),
            Block.box(5, 5, 5, 11, 16, 11)
    ).reduce((v1, v2) -> Shapes.join(v1, v2, BooleanOp.OR)).get();

    public ChakraObeliskBlock(Properties properties) {
        super(properties);
    }

    @Override
    public @NotNull VoxelShape getShape(@NotNull BlockState blockState, @NotNull BlockGetter blockGetter,
                                        @NotNull BlockPos blockPos, @NotNull CollisionContext context) {
        return SHAPE;
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockPlaceContext placeContext) {
        return this.defaultBlockState().setValue(FACING, placeContext.getHorizontalDirection().getOpposite());
    }

    @Override
    public @NotNull BlockState rotate(BlockState blockState, Rotation rotation) {
        return blockState.setValue(FACING, rotation.rotate(blockState.getValue(FACING)));
    }

    @Override
    public @NotNull BlockState mirror(BlockState blockState, Mirror mirror) {
        return blockState.rotate(mirror.getRotation(blockState.getValue(FACING)));
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
        builder.add(FACING);
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void appendHoverText(@NotNull ItemStack itemStack, @Nullable BlockGetter blockGetter,
                                @NotNull List<Component> components, @NotNull TooltipFlag flag) {

        if (Screen.hasShiftDown()) {
            components.add(Component.translatable(ItemHoverTranslatables.BLOCKITEM_CHAKRA_OBELISK_INFO));
        } else {
            components.add((Component.translatable(ItemHoverTranslatables.ITEM_HOVER_BASE_EXPANDABLE)));
        }

        super.appendHoverText(itemStack, blockGetter, components, flag);
    }

    @Override
    public @NotNull InteractionResult use(@NotNull BlockState blockState, Level level, @NotNull BlockPos blockPos,
                                          @NotNull Player player, @NotNull InteractionHand hand,
                                          @NotNull BlockHitResult hitResult) {

        // is on server and main hand
        if (!level.isClientSide() && hand == InteractionHand.MAIN_HAND) {

            // if player has chakra capability
            if (((ServerPlayer) player).getCapability(PlayerOnionChakraProvider.PLAYER_ONION_CHAKRA).isPresent()) {

                // if player has shift down, give dense solid chakra
                if (player.isShiftKeyDown()) {

                    // if the player has enough chakra
                    if (PlayerChakraUtils.getChakra((ServerPlayer) player) >= 90) {
                        PlayerChakraUtils.subChakra((ServerPlayer) player, 90);
                        level.addFreshEntity(
                                new ItemEntity(
                                        level, blockPos.getX(), blockPos.getY(), blockPos.getZ(), new ItemStack(OAItems.SOLID_CHAKRA_DENSE.get())
                                )
                        );

                    // if not enough chakra, inform player
                    } else {
                        ((ServerPlayer) player).sendSystemMessage(Component.translatable(MessageTranslatables.CHAKRA_OBELISK_NOT_ENOUGH_CHAKRA));
                    }

                // else if not sneaking, give solid chakra
                } else {

                    // if has enough chakra
                    if (PlayerChakraUtils.getChakra((ServerPlayer) player) >= 10) {
                        PlayerChakraUtils.subChakra((ServerPlayer) player, 10);
                        level.addFreshEntity(
                                new ItemEntity(
                                        level, blockPos.getX(), blockPos.getY(), blockPos.getZ(), new ItemStack(OAItems.SOLID_CHAKRA.get())
                                )
                        );

                    // if not enough chakra, inform player
                    } else {
                        ((ServerPlayer) player).sendSystemMessage(Component.translatable(MessageTranslatables.CHAKRA_OBELISK_NOT_ENOUGH_CHAKRA));
                    }
                }
            }
        }

        return InteractionResult.PASS;
    }

}
