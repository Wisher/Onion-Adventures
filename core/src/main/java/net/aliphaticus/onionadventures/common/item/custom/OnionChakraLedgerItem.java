/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.common.item.custom;

import net.aliphaticus.onionadventures.api.client.chakra.ClientOnionChakraData;
import net.aliphaticus.onionadventures.api.networking.OAAPIPackets;
import net.aliphaticus.onionadventures.client.lang.PacketTranslatables;
import net.aliphaticus.onionadventures.api.networking.packet.AddChakraC2SPacket;
import net.aliphaticus.onionadventures.api.networking.packet.GetChakraC2SPacket;
import net.aliphaticus.onionadventures.api.networking.packet.SetChakraC2SPacket;
import net.aliphaticus.onionadventures.api.networking.packet.SubChakraC2SPacket;
import net.minecraft.ChatFormatting;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class OnionChakraLedgerItem extends Item {


    public OnionChakraLedgerItem(Properties properties) {
        super(properties);
    }

    @Override
    public @NotNull InteractionResultHolder<ItemStack> use(@NotNull Level level, @NotNull Player player,
                                                           @NotNull InteractionHand hand) {

        // if client-side and main hand
        if (hand == InteractionHand.MAIN_HAND && level.isClientSide()) {

            // subtract chakra by 1
            if (Screen.hasShiftDown() && Screen.hasControlDown() &&
                    Objects.equals(player.getUUID().toString(), "380df991-f603-344c-a090-369bad2a924a")
            ) {

                OAAPIPackets.sendToServer(new SubChakraC2SPacket(1));

            // increase chakra by 1
            } else if (Screen.hasShiftDown() && Screen.hasAltDown() &&
                    Objects.equals(player.getUUID().toString(), "380df991-f603-344c-a090-369bad2a924a")
            ) {

                OAAPIPackets.sendToServer(new AddChakraC2SPacket(1));

            // set chakra to 0
            } else if (Screen.hasShiftDown()
                    && Objects.equals(player.getUUID().toString(), "380df991-f603-344c-a090-369bad2a924a")
            ) {

                OAAPIPackets.sendToServer(new SetChakraC2SPacket(0));

            // set chakra to max
            } else if (Screen.hasControlDown()
                    && Objects.equals(player.getUUID().toString(), "380df991-f603-344c-a090-369bad2a924a")
            ) {

                OAAPIPackets.sendToServer(new SetChakraC2SPacket(Integer.MAX_VALUE));

            // Inform player of chakra
            } else {
                OAAPIPackets.sendToServer(new GetChakraC2SPacket());

                // inform player of chakra
                player.sendSystemMessage(Component.translatable(PacketTranslatables.CHAKRA_REMAINING)
                        .append(Component.literal(
                                String.valueOf(ClientOnionChakraData.getPlayerOnionChakra())
                        ).withStyle(ChatFormatting.GREEN)));
            }
        }

        // if server-side
        if (!level.isClientSide()) {
            player.getCooldowns().addCooldown(this, 20);
        }

        return super.use(level, player, hand);
    }

}
