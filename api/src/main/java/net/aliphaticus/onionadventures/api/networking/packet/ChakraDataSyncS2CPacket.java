/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 *
 * Permission & a License is hereby granted to use this code under the conditions that:
 * 1) The project using this code is open-source and accessible publicly via a service such as GitHub, GitLab, etc.
 * 2) The Project using this code is copy-left under a license such as MIT, GNU General Public License v3.0, etc.
 */

package net.aliphaticus.onionadventures.api.networking.packet;

import net.aliphaticus.onionadventures.api.client.chakra.ClientOnionChakraData;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.network.NetworkEvent;

import java.util.function.Supplier;

/**
 * Packet to sync the clients chakra, {@link ClientOnionChakraData}
 */
public class ChakraDataSyncS2CPacket extends S2CPacket {

    /**
     * Value to set {@link ClientOnionChakraData} to
     */
    private final int chakra;

    public ChakraDataSyncS2CPacket(int chakra) {
        this.chakra = chakra;
    }

    public ChakraDataSyncS2CPacket(FriendlyByteBuf buf) {
        this.chakra = buf.readInt();
    }

    @Override
    public void toBytes(FriendlyByteBuf buf) {
        buf.writeInt(chakra);
    }

    @Override
    public void handle(Supplier<NetworkEvent.Context> supplier) {
        NetworkEvent.Context context = supplier.get();
        context.enqueueWork(() -> {
            // On the Server

            // set the clients chakra
            DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> () -> ClientOnionChakraData.set(chakra));

        });

        // inform network packet was handled
        context.setPacketHandled(true);
    }
}
