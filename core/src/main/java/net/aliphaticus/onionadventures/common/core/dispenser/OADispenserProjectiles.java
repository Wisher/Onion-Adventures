/*
 * Copyright (c) 2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.common.core.dispenser;

import net.aliphaticus.onionadventures.common.entity.custom.projectile.RedOnionEmpoweredProjectile;
import net.aliphaticus.onionadventures.common.item.OAItems;
import net.minecraft.Util;
import net.minecraft.core.Position;
import net.minecraft.core.dispenser.AbstractProjectileDispenseBehavior;
import net.minecraft.core.dispenser.DispenseItemBehavior;
import net.minecraft.world.entity.projectile.Projectile;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.DispenserBlock;

public class OADispenserProjectiles {

    public static final DispenseItemBehavior RED_ONION_PROJECTILE = new AbstractProjectileDispenseBehavior() {
        @Override
        protected Projectile getProjectile(Level level, Position pos, ItemStack itemStack) {
            return (Projectile) Util.make(new RedOnionEmpoweredProjectile(level, pos.x(), pos.y(), pos.z()),
                    (projectile) -> {
                        projectile.setItem(itemStack);
                    });
        }
    };

    public static void onCommonSetup() {
        DispenserBlock.registerBehavior(OAItems.RED_ONION_EMPOWERED.get(), RED_ONION_PROJECTILE);
    }

}
