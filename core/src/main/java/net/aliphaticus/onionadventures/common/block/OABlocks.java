/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.common.block;

import net.aliphaticus.onionadventures.OnionAdventures;
import net.aliphaticus.onionadventures.common.block.custom.OnionCropBlock;
import net.aliphaticus.onionadventures.common.block.custom.WildOnionBlock;
import net.aliphaticus.onionadventures.common.block.custom.ChakraObeliskBlock;
import net.aliphaticus.onionadventures.common.item.OAItems;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.material.Material;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

import java.util.function.Supplier;

public class OABlocks {

    public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, OnionAdventures.MODID);

    // ########## Blocks ##########


    // ########## Custom Blocks ##########
    public static final RegistryObject<Block> ONION_CROP = BLOCKS.register("onion_crop",
            () -> new OnionCropBlock(BlockBehaviour.Properties.copy(Blocks.WHEAT), 1));

    public static final RegistryObject<Block> WILD_ONIONS = registerBlock("wild_onions",
            () -> new WildOnionBlock(BlockBehaviour.Properties.of(Material.PLANT)
                    .noOcclusion().noCollission().sound(SoundType.CROP), 10));

    public static final RegistryObject<Block> CHAKRA_OBELISK = registerBlock("chakra_obelisk",
            () -> new ChakraObeliskBlock(BlockBehaviour.Properties.of(Material.METAL)
                    .strength(6F).requiresCorrectToolForDrops().noOcclusion()));

    // ########## Custom BlocksEntities ##########


    private static <T extends Block> RegistryObject<T> registerBlock(String name, Supplier<T> block) {
        RegistryObject<T> toReturn = BLOCKS.register(name, block);
        registerBlockItem(name, toReturn);
        return toReturn;
    }


    private static <T extends Block> RegistryObject<Item> registerBlockItem(String name, RegistryObject<T> block) {
        return OAItems.ITEMS.register(name,
                () -> new BlockItem(block.get(), new Item.Properties()));
    }

    public static void register(IEventBus eventBus) {
        BLOCKS.register(eventBus);
    }

}
