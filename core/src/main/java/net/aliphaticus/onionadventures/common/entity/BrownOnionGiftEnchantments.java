/*
 * Copyright (c) 2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.common.entity;

import net.aliphaticus.onionadventures.misc.EnchantmentHolder;
import net.minecraft.world.item.enchantment.Enchantments;

import java.util.HashMap;

public final class BrownOnionGiftEnchantments {
    
    private static final HashMap<EnchantmentHolder, Integer> GIFT_ENCHANTMENTS = new HashMap<>();
    private static final int MIN_F_TO_DROP_GIFT = 2;
    
    static {
        // efficiency
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.BLOCK_EFFICIENCY, 1), MIN_F_TO_DROP_GIFT);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.BLOCK_EFFICIENCY, 2), MIN_F_TO_DROP_GIFT + 1);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.BLOCK_EFFICIENCY, 3), MIN_F_TO_DROP_GIFT + 2);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.BLOCK_EFFICIENCY, 4), MIN_F_TO_DROP_GIFT + 3);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.BLOCK_EFFICIENCY, 5), MIN_F_TO_DROP_GIFT + 4);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.BLOCK_EFFICIENCY, 6), MIN_F_TO_DROP_GIFT + 8);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.BLOCK_EFFICIENCY, 7), MIN_F_TO_DROP_GIFT + 13);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.BLOCK_EFFICIENCY, 8), MIN_F_TO_DROP_GIFT + 18);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.BLOCK_EFFICIENCY, 9), MIN_F_TO_DROP_GIFT + 23);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.BLOCK_EFFICIENCY, 10), MIN_F_TO_DROP_GIFT + 28);

        // fortune
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.BLOCK_FORTUNE, 1), MIN_F_TO_DROP_GIFT + 2);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.BLOCK_FORTUNE, 2), MIN_F_TO_DROP_GIFT + 3);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.BLOCK_FORTUNE, 3), MIN_F_TO_DROP_GIFT + 4);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.BLOCK_FORTUNE, 4), MIN_F_TO_DROP_GIFT + 8);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.BLOCK_FORTUNE, 5), MIN_F_TO_DROP_GIFT + 18);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.BLOCK_FORTUNE, 6), MIN_F_TO_DROP_GIFT + 28);

        // looting
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.MOB_LOOTING, 1), MIN_F_TO_DROP_GIFT + 2);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.MOB_LOOTING, 2), MIN_F_TO_DROP_GIFT + 3);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.MOB_LOOTING, 3), MIN_F_TO_DROP_GIFT + 4);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.MOB_LOOTING, 4), MIN_F_TO_DROP_GIFT + 8);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.MOB_LOOTING, 5), MIN_F_TO_DROP_GIFT + 18);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.MOB_LOOTING, 6), MIN_F_TO_DROP_GIFT + 28);

        // protection
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.ALL_DAMAGE_PROTECTION, 1), MIN_F_TO_DROP_GIFT);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.ALL_DAMAGE_PROTECTION, 2), MIN_F_TO_DROP_GIFT + 1);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.ALL_DAMAGE_PROTECTION, 3), MIN_F_TO_DROP_GIFT + 2);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.ALL_DAMAGE_PROTECTION, 4), MIN_F_TO_DROP_GIFT + 3);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.ALL_DAMAGE_PROTECTION, 5), MIN_F_TO_DROP_GIFT + 4);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.ALL_DAMAGE_PROTECTION, 6), MIN_F_TO_DROP_GIFT + 8);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.ALL_DAMAGE_PROTECTION, 7), MIN_F_TO_DROP_GIFT + 18);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.ALL_DAMAGE_PROTECTION, 8), MIN_F_TO_DROP_GIFT + 28);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.ALL_DAMAGE_PROTECTION, 9), MIN_F_TO_DROP_GIFT + 38);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.ALL_DAMAGE_PROTECTION, 10), MIN_F_TO_DROP_GIFT + 48);

        // respiration
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.RESPIRATION, 1), MIN_F_TO_DROP_GIFT + 3);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.RESPIRATION, 2), MIN_F_TO_DROP_GIFT + 5);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.RESPIRATION, 3), MIN_F_TO_DROP_GIFT + 7);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.RESPIRATION, 4), MIN_F_TO_DROP_GIFT + 13);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.RESPIRATION, 5), MIN_F_TO_DROP_GIFT + 23);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.RESPIRATION, 6), MIN_F_TO_DROP_GIFT + 33);

        // thorns
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.THORNS, 1), MIN_F_TO_DROP_GIFT + 3);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.THORNS, 2), MIN_F_TO_DROP_GIFT + 5);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.THORNS, 3), MIN_F_TO_DROP_GIFT + 7);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.THORNS, 4), MIN_F_TO_DROP_GIFT + 13);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.THORNS, 5), MIN_F_TO_DROP_GIFT + 23);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.THORNS, 6), MIN_F_TO_DROP_GIFT + 33);

        // sharpness
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.SHARPNESS, 1), MIN_F_TO_DROP_GIFT);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.SHARPNESS, 2), MIN_F_TO_DROP_GIFT + 1);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.SHARPNESS, 3), MIN_F_TO_DROP_GIFT + 2);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.SHARPNESS, 4), MIN_F_TO_DROP_GIFT + 3);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.SHARPNESS, 5), MIN_F_TO_DROP_GIFT + 4);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.SHARPNESS, 6), MIN_F_TO_DROP_GIFT + 8);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.SHARPNESS, 7), MIN_F_TO_DROP_GIFT + 13);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.SHARPNESS, 8), MIN_F_TO_DROP_GIFT + 18);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.SHARPNESS, 9), MIN_F_TO_DROP_GIFT + 23);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.SHARPNESS, 10), MIN_F_TO_DROP_GIFT + 28);

        // misc
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.SILK_TOUCH, 1), MIN_F_TO_DROP_GIFT + 4);
        GIFT_ENCHANTMENTS.put(new EnchantmentHolder(Enchantments.MENDING, 1), MIN_F_TO_DROP_GIFT + 13);
    }
    
    public static HashMap<EnchantmentHolder, Integer> getGiftEnchantments() {
        return GIFT_ENCHANTMENTS;
    }
    
}
