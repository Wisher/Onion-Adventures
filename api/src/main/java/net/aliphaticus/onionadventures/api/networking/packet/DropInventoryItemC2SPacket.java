/*
 * Copyright (c) 2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 *
 * Permission & a License is hereby granted to use this code under the conditions that:
 * 1) The project using this code is open-source and accessible publicly via a service such as GitHub, GitLab, etc.
 * 2) The Project using this code is copy-left under a license such as MIT, GNU General Public License v3.0, etc.
 */

package net.aliphaticus.onionadventures.api.networking.packet;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.network.NetworkEvent;

import java.util.function.Supplier;

/**
 * Drops the given ItemStack from the Players inventory
 */
public class DropInventoryItemC2SPacket extends C2SPacket {

    /**
     * ItemStack to drop
     */
    private final ItemStack itemStack;

    public DropInventoryItemC2SPacket(ItemStack itemStack) {
        this.itemStack = itemStack;
    }

    public DropInventoryItemC2SPacket(FriendlyByteBuf buf) {
        this.itemStack = buf.readItem();
    }

    @Override
    public void toBytes(FriendlyByteBuf buf) {
        buf.writeItem(itemStack);
    }

    @Override
    public void handle(Supplier<NetworkEvent.Context> supplier) {
        NetworkEvent.Context context = supplier.get();
        context.enqueueWork(() -> {
            // On the Server

            // get player
            ServerPlayer player = context.getSender();

            player.drop(itemStack, true);
        });
    }
}
