/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 *
 * Permission & a License is hereby granted to use this code under the conditions that:
 * 1) The project using this code is open-source and accessible publicly via a service such as GitHub, GitLab, etc.
 * 2) The Project using this code is copy-left under a license such as MIT, GNU General Public License v3.0, etc.
 */

package net.aliphaticus.onionadventures.api.item;

import net.aliphaticus.onionadventures.api.capability.chakra.PlayerOnionChakra;
import net.aliphaticus.onionadventures.api.util.PlayerChakraUtils;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import org.jetbrains.annotations.NotNull;

/**
 * Parent class for items that give Chakra
 */
public class AbstractChakraGivingItem extends Item {

    /**
     * Amount to add to {@link PlayerOnionChakra PlayerOnionChakra}
     */
    private final int chakra;
    /**
     * How loud of a sound using the item should make
     */
    private final float volume;
    /**
     * What pitch variation using the item should make
     */
    private final float pitch;
    /**
     * How loud using a stack should sound
     */
    private final float shiftVolume;
    /**
     * what pitch using a stack should be
     */
    private final float shiftPitch;

    /**
     * Calls the {@link Item} constructor, adding the additional Chakra properties and sound fx
     * @param chakra Chakra to give
     * @param volume how loud of a sound to make
     * @param pitch pitch of the sound
     * @param shiftVolume how loud of a sound to make when using a stack
     * @param shiftPitch pitch of the sound to make when using a stack
     * @param properties Item {@link net.minecraft.world.item.Item.Properties Properties}
     */
    public AbstractChakraGivingItem(int chakra, float volume, float pitch, float shiftVolume, float shiftPitch, Properties properties) {
        super(properties);

        this.chakra = chakra;
        this.volume = volume;
        this.pitch = pitch;
        this.shiftVolume = shiftVolume;
        this.shiftPitch = shiftPitch;
    }

    /**
     * Gets the amount of chakra to give
     * @return int of chakra to give
     */
    public int getChakra() {
        return this.chakra;
    }

    @Override
    public @NotNull InteractionResultHolder<ItemStack> use(Level level, @NotNull Player player,
                                                           @NotNull InteractionHand hand) {

        if (!level.isClientSide()) {

            // if in main hand
            if (hand == InteractionHand.MAIN_HAND) {

                // if sneaking, and enough room to add stacks chakra, consume stack and add chakra
                if (player.isShiftKeyDown() &&
                        (PlayerChakraUtils.getChakra((ServerPlayer) player) <=
                                PlayerChakraUtils.getMaxChakra((ServerPlayer) player) - (getChakra() * player.getItemInHand(hand).getCount()))
                ) {
                    // add chakra
                    PlayerChakraUtils.addChakra((ServerPlayer) player, getChakra() * player.getItemInHand(hand).getCount());
                    // remove items
                    player.getItemInHand(hand).shrink(player.getItemInHand(hand).getCount());
                    // play sound
                    level.playSound(null, player.getOnPos(), SoundEvents.EXPERIENCE_ORB_PICKUP, SoundSource.PLAYERS, this.shiftVolume, this.shiftPitch);

                // if enough room on player to add chakra, add chakra and shrink stack
                } else if (PlayerChakraUtils.getChakra((ServerPlayer) player) <= PlayerChakraUtils.getMaxChakra((ServerPlayer) player) - getChakra()) {

                    // add chakra
                    PlayerChakraUtils.addChakra((ServerPlayer) player, getChakra());
                    // shrink stack
                    player.getItemInHand(hand).shrink(1);
                    // play sound
                    level.playSound(null, player.getOnPos(), SoundEvents.EXPERIENCE_ORB_PICKUP, SoundSource.PLAYERS, this.volume, this.pitch);

                }
            }
        }

        return super.use(level, player, hand);
    }
}
