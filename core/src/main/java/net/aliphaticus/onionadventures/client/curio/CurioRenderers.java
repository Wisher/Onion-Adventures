/*
 * Copyright (c) 2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.client.curio;

import net.aliphaticus.onionadventures.client.curio.model.HeadModel;
import net.aliphaticus.onionadventures.client.curio.renderer.CurioRenderer;
import net.aliphaticus.onionadventures.common.item.OAItems;
import net.aliphaticus.onionadventures.common.item.OAItemsCurios;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.renderer.RenderType;
import top.theillusivec4.curios.api.client.CuriosRendererRegistry;

public class CurioRenderers {

    public static void register() {

        // Head
        CuriosRendererRegistry.register(
                OAItemsCurios.CHAKRA_MONOCLE.get(),
                () ->
                        new CurioRenderer(
                                "chakra_monocle",
                                new HeadModel(bakeLayer(CurioLayers.CHAKRA_MONOCLE), RenderType::itemEntityTranslucentCull),
                                0.3F, 0.3F, 0.3F
                        )
        );

    }

    public static ModelPart bakeLayer(ModelLayerLocation layerLocation) {
        return Minecraft.getInstance().getEntityModels().bakeLayer(layerLocation);
    }

}
