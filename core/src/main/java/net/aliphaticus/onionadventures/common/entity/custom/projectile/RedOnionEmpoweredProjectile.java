/*
 * Copyright (c) 2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.common.entity.custom.projectile;

import net.aliphaticus.onionadventures.api.item.nbt.OANBTTagNames;
import net.aliphaticus.onionadventures.common.entity.OAEntities;
import net.aliphaticus.onionadventures.common.item.OAItems;
import net.aliphaticus.onionadventures.common.item.custom.projectile.RedOnionEmpoweredItem;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.projectile.ThrowableItemProjectile;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.EntityHitResult;
import org.jetbrains.annotations.NotNull;

public class RedOnionEmpoweredProjectile extends ThrowableItemProjectile {

    private final int maxAge;
    private final float explosionRadius;

    public RedOnionEmpoweredProjectile(EntityType<? extends RedOnionEmpoweredProjectile> entityType, Level level) {
        super(entityType, level);

        this.maxAge = ((RedOnionEmpoweredItem) this.getItem().getItem().asItem()).getMaxAge();
        this.explosionRadius = ((RedOnionEmpoweredItem) this.getItem().getItem().asItem()).getExplosionRadius();



    }

    public RedOnionEmpoweredProjectile(Level level, double p_37433_, double p_37434_, double p_37435_) {
        super(OAEntities.RED_ONION_PROJECTILE.get(), p_37433_, p_37434_, p_37435_, level);

        this.maxAge = ((RedOnionEmpoweredItem) this.getItem().getItem().asItem()).getMaxAge();
        this.explosionRadius = ((RedOnionEmpoweredItem) this.getItem().getItem().asItem()).getExplosionRadius();
    }

    public RedOnionEmpoweredProjectile(Level level, Player player, int maxAge, float explosionRadius) {
        super(OAEntities.RED_ONION_PROJECTILE.get(), player, level);

        this.maxAge = maxAge;
        this.explosionRadius = explosionRadius;
    }

    public RedOnionEmpoweredProjectile(Level level, LivingEntity livingEntity) {
        super(OAEntities.RED_ONION_PROJECTILE.get(), livingEntity, level);

        this.maxAge = ((RedOnionEmpoweredItem) this.getItem().getItem().asItem()).getMaxAge();
        this.explosionRadius = ((RedOnionEmpoweredItem) this.getItem().getItem().asItem()).getExplosionRadius();
    }


    @Override
    protected @NotNull Item getDefaultItem() {
        return OAItems.RED_ONION_EMPOWERED.get();
    }

    @Override
    public void tick() {
        super.tick();

        if (!getTag()) {
            CompoundTag nbt = new CompoundTag();
            nbt.putInt(OANBTTagNames.AGE, 1);
            this.getItem().setTag(nbt);
        }

        if (getTag() && !hasAgeTag()) {
            CompoundTag nbt = this.getItem().getTag();
            nbt.putInt(OANBTTagNames.AGE, 1);
            this.getItem().setTag(nbt);
        } else if (getTag() && hasAgeTag()) {
            CompoundTag nbt = this.getItem().getTag();
            int age = nbt.getInt(OANBTTagNames.AGE) + 1;
            nbt.putInt(OANBTTagNames.AGE, age);
            this.getItem().setTag(nbt);
        }

        if (getTag() && getAgeTag() > maxAge) {
            explode();
        }

    }

    @Override
    protected void onHitEntity(@NotNull EntityHitResult entityHitResult) {
        super.onHitEntity(entityHitResult);

        explode();
    }

    @Override
    protected void onHitBlock(@NotNull BlockHitResult blockHitResult) {
        super.onHitBlock(blockHitResult);

        explode();
    }

    private boolean getTag() {
        return this.getItem().getTag() != null;
    }

    private boolean hasAgeTag() {
        return this.getItem().getTag().contains(OANBTTagNames.AGE);
    }

    private int getAgeTag() {
        return this.getItem().getTag().getInt(OANBTTagNames.AGE);
    }

    private void explode() {
        this.getLevel().explode(null, this.getX(), this.getY(), this.getZ(), explosionRadius, false, Level.ExplosionInteraction.BLOCK);
        this.getItem().shrink(1);
        this.remove(RemovalReason.DISCARDED);
    }

}
