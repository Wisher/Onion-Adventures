/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.common.item.custom.tool.shovel;

import net.aliphaticus.onionadventures.api.item.IChakraEmpoweredItem;
import net.aliphaticus.onionadventures.api.item.tool.AbstractOnionChakraShovelItem;
import net.aliphaticus.onionadventures.client.lang.ItemHoverTranslatables;
import net.aliphaticus.onionadventures.common.item.OAItems;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Tier;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class OnionShovelDiamondEmpoweredItem extends AbstractOnionChakraShovelItem implements IChakraEmpoweredItem {

    public OnionShovelDiamondEmpoweredItem(Tier tier, float attackDamage, float attackSpeed, int activationCost, int useCost, Properties properties, String[] hoverTranslatable) {
        super(tier, attackDamage, attackSpeed, true, activationCost, useCost, properties, hoverTranslatable);
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void appendHoverText(@NotNull ItemStack itemStack, @Nullable Level level, @NotNull List<Component> components,
                                @NotNull TooltipFlag flag) {

        if (Screen.hasShiftDown()) {
            addToShiftHoverText(components);
        } else {
            components.add((Component.translatable(ItemHoverTranslatables.ITEM_HOVER_BASE_EXPANDABLE)));
        }

        super.appendHoverText(itemStack, level, components, flag);
    }

    @Override
    public ItemStack createUpdatedItem(int damage) {
        ItemStack itemStack = new ItemStack(OAItems.ONION_DIAMOND_SHOVEL.get());
        itemStack.setDamageValue(damage);
        return itemStack;
    }

}
