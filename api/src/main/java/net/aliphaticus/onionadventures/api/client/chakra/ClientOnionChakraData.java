/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 *
 * Permission & a License is hereby granted to use this code under the conditions that:
 * 1) The project using this code is open-source and accessible publicly via a service such as GitHub, GitLab, etc.
 * 2) The Project using this code is copy-left under a license such as MIT, GNU General Public License v3.0, etc.
 */

package net.aliphaticus.onionadventures.api.client.chakra;

import net.aliphaticus.onionadventures.api.capability.chakra.PlayerOnionChakra;

/**
 * <p>The Clients {@link PlayerOnionChakra PlayerOnionChakra#chakra} value</p>
 * <p>Used by the Client to keep track of the {@link PlayerOnionChakra PlayerOnionChakra}
 *  value, useful for things like HUD elements, or client logic for chakra</p>
 * Should be re-synced every time chakra changes via {@link net.aliphaticus.onionadventures.api.networking.packet.ChakraDataSyncS2CPacket ChakraDataSyncS2CPacket}
 */
public class ClientOnionChakraData {

    /**
     * Chakra Value
     */
    private static int playerOnionChakra;

    /**
     * Set the Players Chakra
     * @param chakra desired value of chakra
     */
    public static void set(int chakra) {
        ClientOnionChakraData.playerOnionChakra = chakra;
    }

    /**
     * Gets the Players Chakra
     * @return Player Chakra Value
     */
    public static int getPlayerOnionChakra() {
        return playerOnionChakra;
    }

}
