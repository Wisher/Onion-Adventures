/*
 * Copyright (c) 2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.client.gui;

import com.mojang.blaze3d.vertex.PoseStack;
import net.aliphaticus.onionadventures.common.item.custom.curio.ChakraMonocleItem;
import net.minecraft.client.Minecraft;

public class HUDHandler {

    private HUDHandler() {}

    public static void onDrawScreenPost(PoseStack poseStack) {
        Minecraft mc = Minecraft.getInstance();

        if (mc.options.hideGui) {
            return;
        }

        if (ChakraMonocleItem.hasMonocle(mc.player)) {
            ChakraMonocleItem.Hud.render(poseStack);
        }

    }

}
