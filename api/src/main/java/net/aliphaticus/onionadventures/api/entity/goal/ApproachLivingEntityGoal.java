/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 *
 * Permission & a License is hereby granted to use this code under the conditions that:
 * 1) The project using this code is open-source and accessible publicly via a service such as GitHub, GitLab, etc.
 * 2) The Project using this code is copy-left under a license such as MIT, GNU General Public License v3.0, etc.
 */

package net.aliphaticus.onionadventures.api.entity.goal;

import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.ai.navigation.PathNavigation;
import net.minecraft.world.level.pathfinder.BlockPathTypes;

import java.util.List;
import java.util.Random;
import java.util.function.Predicate;

/**
 * Goal for approaching any subclass of living entities
 * <br>Approaches to a random distance within a given range, and searches a given area
 * @param <T> LivingEntity Class to approach
 */
public class ApproachLivingEntityGoal <T extends LivingEntity> extends Goal {

    /**
     * The entity approaching other entities
     */
    private final Mob sourceEntity;
    /**
     * Predicate for whether it's a valid target
     */
    private final Predicate<LivingEntity> targetPredicate;
    /**
     * {@link PathNavigation}
     */
    private final PathNavigation navigation;
    /**
     * How fast to approach the target
     */
    private final float speedModifier;
    /**
     * Minimum stop distance
     */
    private final float stopMinDistance;
    /**
     * Maximum stop distance
     */
    private final float stopMaxDistance;
    /**
     * Area to search for targets
     */
    private final float areaToCheck;
    /**
     * chance per tick to approach, where 1.0F is 100%
     */
    private final float probabilityToApproach;
    /**
     * Target entity class
     */
    private final Class<T> targetEntityClass;

    /**
     * Randomly generated stop distance, selected between {@link #stopMinDistance} and {@link #stopMaxDistance}
     */
    private float stopDistance;
    /**
     * Class Type of the target entity
     */
    private T target;
    /**
     * time in ticks until recalculating the pathing (variable directly referenced from followMobGoal)?
     */
    private int timeToRecalcPath;
    /**
     * used to stop avoiding water when chasing mobs (variable directly referenced from followMobGoal)?
     */
    private float oldWaterCost;
    /**
     * Whether the target can be approached
     */
    private boolean canApproach = false;

    /**
     * @param sourceEntity the Mob seeking out the target
     * @param targetEntityClass Class of the Target {@link LivingEntity}
     * @param speedModifier Movement Speed modifier when approaching target
     * @param stopMinDistance Minimum distance to stop from target
     * @param stopMaxDistance Maximum distance to stop from target
     * @param areaToCheck area to check for targets
     * @param probabilityToApproach probability per tick to approach a target (1.0F is 100%)
     */
    public ApproachLivingEntityGoal(Mob sourceEntity, Class<T> targetEntityClass, float speedModifier, float stopMinDistance, float stopMaxDistance, float areaToCheck,
                                    float probabilityToApproach) {

        this.sourceEntity = sourceEntity;
        this.targetPredicate = (livingEntity) -> {
            return livingEntity != null && getTargetEntityClass() == livingEntity.getClass();
        };

        this.targetEntityClass = targetEntityClass;

        this.navigation = sourceEntity.getNavigation();
        this.speedModifier = speedModifier;

        this.stopMinDistance = stopMinDistance;
        this.stopMaxDistance = stopMaxDistance;
        generateStopDistance(stopMinDistance, stopMaxDistance);


        this.areaToCheck = areaToCheck;
        this.probabilityToApproach = probabilityToApproach;
    }

    @Override
    public boolean canUse() {
        // can approach
        if (canApproach) {
            List<T> list = this.sourceEntity.level.getEntitiesOfClass(
                    getTargetEntityClass(), this.sourceEntity.getBoundingBox().inflate((double) this.areaToCheck), this.targetPredicate);
            if (!list.isEmpty()) {
                for (T target : list) {
                    if (!target.isInvisible()) {
                        this.target = target;
                        return true;
                    }
                }

            // didn't find target, set to false
            } else {
                this.canApproach = false;
            }
        // run chance to approach
        } else {
            if (this.sourceEntity.getRandom().nextFloat() < probabilityToApproach) {
                canApproach = true;
            }
            return false;
        }

        return false;
    }

    @Override
    public boolean canContinueToUse() {
        return this.target != null &&
                !this.navigation.isDone() &&
                this.sourceEntity.distanceToSqr(this.target) > (double)(this.stopDistance * this.stopDistance);
    }

    @Override
    public void start() {
        this.timeToRecalcPath = 0;
        this.oldWaterCost = this.sourceEntity.getPathfindingMalus(BlockPathTypes.WATER);
        this.sourceEntity.setPathfindingMalus(BlockPathTypes.WATER, 0.0F);
    }

    @Override
    public void stop() {
        this.target = null;
        this.canApproach = false;
        generateStopDistance(this.stopMinDistance, this.stopMaxDistance);

        this.navigation.stop();
        this.sourceEntity.setPathfindingMalus(BlockPathTypes.WATER, this.oldWaterCost);
    }

    @Override
    public void tick() {
        if (this.target != null && !this.sourceEntity.isLeashed()) {
            this.sourceEntity.getLookControl().setLookAt(this.target, 10.0F, (float) this.sourceEntity.getMaxHeadXRot());

            if (--this.timeToRecalcPath <= 0) {
                this.timeToRecalcPath = this.adjustedTickDelay(10);
                double dx = this.sourceEntity.getX() - this.target.getX();
                double dy = this.sourceEntity.getY() - this.target.getY();
                double dz = this.sourceEntity.getZ() - this.target.getZ();
                double d = dx * dx + dy * dy + dz * dz;
                if (!(d <= (double) (this.stopDistance * this.stopDistance))) {
                    this.navigation.moveTo(this.target, this.speedModifier);

                // target is too close, stop
                } else {
                    this.navigation.stop();
                }
            }

        }
    }

    /**
     * Randomly generates the stop distance from the given range
     * @param stopMinDistance Minimum stop distance
     * @param stopMaxDistance Maximum stop distance
     */
    private void generateStopDistance(float stopMinDistance, float stopMaxDistance) {
        // set random distance
        Random random = new Random();
        // add 0.1 to make the random float inclusive
        float stopDistance = random.nextFloat((stopMaxDistance - stopMinDistance) + 0.1F) + stopMinDistance;
        // round to 1 decimal place in-case a distance like 1.5 was set
        this.stopDistance = (float) Math.round(stopDistance * 10) / 10;
    }

    /**
     * Gets the class of the target entity
     * @return Target Entity Class
     */
    private Class<T> getTargetEntityClass() {
        return this.targetEntityClass;
    }
}
