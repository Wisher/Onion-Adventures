# TODO

### General
- [ ] Balance Weapons
- [ ] Refactor ModItemTier to/add ModToolTier ForgeTier setup
  - reference KaupenJoe Custom Mining Level #30 1.19.2
- [ ] revise networking as per the forge docs and refactor if needed


### Items
- [ ] Smithing Table item tiers for weapons/tools/armour
  - [ ] Weapons
  - [ ] Tools
  - [ ] Armour
  - [ ] Additional Abilities (while empowered, costs chakra)
    - [ ] Hoe iron/gold+, ctrl+right-click to bonemeal crop
    - [ ] Pickaxe iron/gold+, ctrl+right-click to insta-mine silk touched block
    - [ ] Shovel iron/gold+, ctrl+right-click to convert gravel to flint (and any other %chance drop blocks->items)
    - [ ] Axe iron/gold+, ctrl+right-click to insta-kill and behead mob (non-players, non-bosses)
    - [ ] Hoe diamond+, alt+right-click to convert grass->mycelium->podzol->coarse dirt->dirt->grass
    - [ ] Pickaxe diamond+, alt+right-click to convert obsidian->crying obsidian->obsidian
    - [ ] Shovel diamond+, alt+right-click to instamine and drop random ore from gravel (weighted list, including netherite scrap)
    - [ ] Axe diamond+, alt+right-click to veinmine tree? check 2 blocks horizontally and 1 block vertically from each log found should be sufficient
    - [ ] Hoe netherite, 
    - [ ] Pickaxe netherite, chance to drop random ores when mining? (override mineblock, Block.popResource, or BlockEvent.BreakEvent, blockstate.is(Blocks.STONE etc.))
      - or reference KaupenJoe Loot Modifiers #39 1.19.2, create a custom registryObject for the loot_modifiers json to hook into, json (data) powered loot table additions, for all stones
    - [ ] Shovel netherite, 
    - [ ] axe netherite, 
    - [ ] armour, potioneffects on hotkey press that drain chakra over time, not every tier needs more effects, helmet - night vision, chestplatge regen, leggings jump boost, feet?

### Blocks
- [ ] Onion Crop, make drop multiple onions and be affected by fortune

### Block Entities
- [ ] Chakra Obelisk
  - right-clicking grabs the player and checks their chakra, if enough subtracts chakra and drops (pops?) a solid chakra
  - if player is sneaking (if possible to check serverside), pop a dense solid chakra instead?

### Mobs
- [ ] Brown Onion
- [ ] Red Onion
  - textures: "brown wraps around body, lighter towards top and bottom, with dark bits at centre of top/bottom(roots), has eyes on one side (front) and ~~mouth?~~",
  - traverse: "traverses like a slime, jumps in a direction to move, can twist around without jumping",
  - future: "onion lore creatures, warlocks, daemons, gargantuan beings"

### Future
- [ ] commands
  - to see chakra, manage it etc. for server admins / external stuff executing commands
- [ ] dimensions
  - plethora of onion lore dimensions
- [ ] mobs
  - allied and enemy mobs such as pumpkins, leaks etc.
- [ ] chakra HUD
  - via curio slot? insert an item and the HUD appears? showing current chakra
- [ ] physical chakra item
  - more uses, physical chakra conversion (to/from), dungeon loot as physical chakra etc., maybe as a currency on servers via api for external mod use?
- [ ] throwable bomb (made from red onions empowered with chakra?)
  - reference the fire charge item

### CI/CD

### BUGS

### Clean-Up
