/*
 * Copyright (c) 2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.common.entity.goal;

import net.aliphaticus.onionadventures.api.util.PlayerChakraUtils;
import net.aliphaticus.onionadventures.common.entity.custom.mobs.BrownOnionEntity;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.ai.goal.Goal;

import java.util.List;
import java.util.Random;

public class BrownOnionGrantChakraGoal extends Goal {

    private final BrownOnionEntity entity;
    private final int minFriendliness;
    private final int coolDown;
    private final int chakraCap;
    private final float chance;
    private final double distance;

    private int coolDownRemaining;
    private boolean canGrant = false;
    private List<ServerPlayer> players = null;
    private int getPlayersCoolDown = 60;

    public BrownOnionGrantChakraGoal(BrownOnionEntity entity, int minFriendliness, int coolDown, int chakraCap, float chance, double distance) {
        this.entity = entity;
        this.minFriendliness = minFriendliness;
        this.coolDown = coolDown;
        this.chakraCap = chakraCap;
        this.chance = chance;
        this.distance = distance;

        setCoolDownRemaining();
    }

    @Override
    public boolean canUse() {
        if (this.entity.getFriendliness() >= minFriendliness) {
            if (--this.coolDownRemaining <= 0) {
                setCoolDownRemaining();
                this.canGrant = true;
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean canContinueToUse() {
        return this.canGrant;
    }

    @Override
    public void stop() {
        this.players = null;
    }

    @Override
    public void tick() {

        if (--this.getPlayersCoolDown <= 0) {
            setPlayers();

            if (canGrant && this.players != null) {
                if (this.entity.level.getRandom().nextFloat() < this.chance) {
                    players.forEach(p -> {
                        PlayerChakraUtils.addChakra(p, Math.max(1, (int) Math.round((float) getChakraToGrant() / this.players.size())));
                    });
                }

                this.canGrant = false;
            }

            this.getPlayersCoolDown = 60;
        }

    }

    private void setCoolDownRemaining() {
        int max = 110;
        int min = 90;
        Random random = new Random();
        float modifier = ((float) (random.nextInt(max - min) + min)) / 100;
        this.coolDownRemaining = Math.round(this.coolDown * modifier);
    }

    private void setPlayers() {
        List<ServerPlayer> players = this.entity.level.getEntitiesOfClass(
                ServerPlayer.class,
                this.entity.getBoundingBox().inflate(this.distance)
        );

        if (players.size() > 0) {
            this.players = players;
        } else {
            this.players = null;
        }
    }

    private int getChakraToGrant() {
        return Math.min(
                this.chakraCap,
                (int) Math.round((this.entity.getFriendliness() * 1.5) + 5)
        );
    }

}
