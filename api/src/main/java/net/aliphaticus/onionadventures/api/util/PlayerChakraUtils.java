/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 *
 * Permission & a License is hereby granted to use this code under the conditions that:
 * 1) The project using this code is open-source and accessible publicly via a service such as GitHub, GitLab, etc.
 * 2) The Project using this code is copy-left under a license such as MIT, GNU General Public License v3.0, etc.
 */

package net.aliphaticus.onionadventures.api.util;

import net.aliphaticus.onionadventures.api.capability.chakra.PlayerOnionChakra;
import net.aliphaticus.onionadventures.api.capability.chakra.PlayerOnionChakraProvider;
import net.aliphaticus.onionadventures.api.networking.OAAPIPackets;
import net.aliphaticus.onionadventures.api.networking.packet.ChakraDataSyncS2CPacket;
import net.minecraft.server.level.ServerPlayer;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Utilities for simplifying modifying {@link PlayerOnionChakra PlayerOnionChakra}
 * <br>Safely sends packets to keep the {@link net.aliphaticus.onionadventures.api.client.chakra.ClientOnionChakraData ClientOnionChakraData}
 * synced when changes occur
 */
public class PlayerChakraUtils {

    /**
     * Adds and syncs chakra to a given player
     * @param player player to add the chakra to
     * @param chakra amount of chakra to add
     */
    public static void addChakra(ServerPlayer player, int chakra) {
        // add chakra to player
        player.getCapability(PlayerOnionChakraProvider.PLAYER_ONION_CHAKRA).ifPresent(
                playerOnionChakra -> {
                    playerOnionChakra.addChakra(chakra);

                    // sync clients chakra
                    OAAPIPackets.sendToPlayer(new ChakraDataSyncS2CPacket(playerOnionChakra.getChakra()), player);

                    OALoggingUtils.logDebugInfo("Added " + chakra + " chakra to " + player.getName().getString());
                }
        );
    }

    /**
     * Subtracts and syncs chakra on a given player
     * @param player player to subtract chakra from
     * @param chakra amount of chakra to subtract
     */
        public static void subChakra(ServerPlayer player, int chakra) {
            // subtract chakra from player
            player.getCapability(PlayerOnionChakraProvider.PLAYER_ONION_CHAKRA).ifPresent(
                    playerOnionChakra -> {
                        playerOnionChakra.subChakra(chakra);

                        // sync clients chakra
                        OAAPIPackets.sendToPlayer(new ChakraDataSyncS2CPacket(playerOnionChakra.getChakra()), player);

                        OALoggingUtils.logDebugInfo("Subtracted " + chakra + " chakra from " + player.getName().getString());
                    }
            );
        }

    /**
     * Retrieves and syncs the chakra of a given player
     * @param player player to get the chakra from
     * @return players chakra value
     */
        public static int getChakra(ServerPlayer player) {
            // get the players chakra
            AtomicInteger chakra = new AtomicInteger(0);
            player.getCapability(PlayerOnionChakraProvider.PLAYER_ONION_CHAKRA).ifPresent(
                    playerOnionChakra -> {
                        chakra.set(playerOnionChakra.getChakra());

                        // sync clients chakra
                        OAAPIPackets.sendToPlayer(new ChakraDataSyncS2CPacket(playerOnionChakra.getChakra()), player);

                        OALoggingUtils.logDebugInfo("Found (get) " + chakra.get() + " chakra on " + player.getName().getString());
                    }
            );

            return chakra.get();
        }

    /**
     * Retrieves the max possible chakra value
     * @param player player to get the chakra capability of
     * @return players maximum possible chakra value
     */
    public static int getMaxChakra(ServerPlayer player) {
        // get the players maximum possible chakra
        AtomicInteger maxPossibleChakra = new AtomicInteger(0);
        player.getCapability(PlayerOnionChakraProvider.PLAYER_ONION_CHAKRA).ifPresent(
                playerOnionChakra -> {
                    maxPossibleChakra.set(playerOnionChakra.getMaxChakra());

                    OALoggingUtils.logDebugInfo("Found maximum possible chakra value " + maxPossibleChakra.get() + " on " + player.getName().getString());
                }
        );

        return maxPossibleChakra.get();
    }

    /**
     * Sets and syncs the chakra of a given player to a certain amount
     * @param player Player whose chakra is being set
     * @param chakra value to set
     */
        public static void setChakra(ServerPlayer player, int chakra) {
            // set the players chakra
            player.getCapability(PlayerOnionChakraProvider.PLAYER_ONION_CHAKRA).ifPresent(
                    playerOnionChakra -> {
                        playerOnionChakra.setChakra(chakra);

                        OAAPIPackets.sendToPlayer(new ChakraDataSyncS2CPacket(playerOnionChakra.getChakra()), player);

                        OALoggingUtils.logDebugInfo("Set chakra to " + chakra + " chakra on " + player.getName().getString());
                    }
            );
        }

}
