/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.datagen;

import net.aliphaticus.onionadventures.OnionAdventures;
import net.aliphaticus.onionadventures.common.block.OABlocks;
import net.aliphaticus.onionadventures.common.item.OAItems;
import net.aliphaticus.onionadventures.common.item.OAItemsCurios;
import net.minecraft.advancements.critereon.ItemPredicate;
import net.minecraft.data.PackOutput;
import net.minecraft.data.recipes.*;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.AbstractCookingRecipe;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.level.ItemLike;
import net.minecraftforge.common.crafting.conditions.IConditionBuilder;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.function.Consumer;

public class OARecipeProvider extends RecipeProvider implements IConditionBuilder {

    public OARecipeProvider(PackOutput output) {
        super(output);
    }

    @SuppressWarnings("removal")
    @Override
    protected void buildRecipes(@NotNull Consumer<FinishedRecipe> consumer) {
        // Custom Blocks
        // Chakra Obelisk
        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, OABlocks.CHAKRA_OBELISK.get())
                .define('#', Items.IRON_INGOT)
                .define('O', OAItems.ONION.get())
                .define('X', Items.REDSTONE)
                .pattern(" # ")
                .pattern("XOX")
                .pattern("###")
                .unlockedBy("has_onion_item", inventoryTrigger(ItemPredicate.Builder.item()
                        .of(OAItems.ONION.get()).build()))
                .save(consumer);

        // Standard Items
        // onion
        ShapelessRecipeBuilder.shapeless(RecipeCategory.FOOD, OAItems.ONION.get(), 3)
                .requires(OAItems.BUNCH_OF_ONIONS.get())
                .unlockedBy("has_bunch_of_onions_item", inventoryTrigger(ItemPredicate.Builder.item()
                        .of(OAItems.BUNCH_OF_ONIONS.get()).build()))
                .save(consumer);

        // bunch of onions
        ShapelessRecipeBuilder.shapeless(RecipeCategory.FOOD, OAItems.BUNCH_OF_ONIONS.get())
                .requires(OAItems.ONION.get(), 3)
                .unlockedBy("has_onion_item", inventoryTrigger(ItemPredicate.Builder.item()
                        .of(OAItems.ONION.get()).build()))
                .save(consumer);


        // Custom Items
        // roasted onion
        foodCrafting(consumer, List.of(OAItems.ONION.get()), OAItems.ROASTED_ONION.get(), 0.35F,
                200, "roasted_onion");

        // Cooked Onion
        oreSmelting(consumer, List.of(OAItems.BUNCH_OF_ONIONS.get()), RecipeCategory.MISC, OAItems.COOKED_ONION.get(),
                0.35F, 200, "cooked_onion");
        oreBlasting(consumer, List.of(OAItems.BUNCH_OF_ONIONS.get()), RecipeCategory.MISC, OAItems.COOKED_ONION.get(),
                0.35F, 100, "cooked_onion");

        // Onion Chakra Ledger
        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, OAItems.ONION_CHAKRA_LEDGER.get())
                .define('#', Items.PAPER)
                .define('O', OAItems.COOKED_ONION.get())
                .pattern("###")
                .pattern("#O#")
                .pattern("###")
                .unlockedBy("has_cooked_onion_item", inventoryTrigger(ItemPredicate.Builder.item()
                        .of(OAItems.COOKED_ONION.get()).build()))
                .save(consumer);

        // Dense Solid Chakra
        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, OAItems.SOLID_CHAKRA_DENSE.get())
                .define('#', OAItems.SOLID_CHAKRA.get())
                .pattern("###")
                .pattern("###")
                .pattern("###")
                .unlockedBy("has_solid_chakra_item", inventoryTrigger(ItemPredicate.Builder.item()
                        .of(OAItems.SOLID_CHAKRA.get()).build()))
                .save(consumer);


        // Tools
        // Base
        // Onion Pickaxe
        ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, OAItems.ONION_PICKAXE.get())
                .define('#', OAItems.BUNCH_OF_ONIONS.get())
                .define('S', Items.STICK)
                .pattern("###")
                .pattern(" S ")
                .pattern(" S ")
                .unlockedBy("has_bunch_of_onions_item", inventoryTrigger(ItemPredicate.Builder.item()
                        .of(OAItems.BUNCH_OF_ONIONS.get()).build()))
                .save(consumer);

        // Onion Shovel
        ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, OAItems.ONION_SHOVEL.get())
                .define('#', OAItems.BUNCH_OF_ONIONS.get())
                .define('S', Items.STICK)
                .pattern(" # ")
                .pattern(" S ")
                .pattern(" S ")
                .unlockedBy("has_bunch_of_onions_item", inventoryTrigger(ItemPredicate.Builder.item()
                        .of(OAItems.BUNCH_OF_ONIONS.get()).build()))
                .save(consumer);

        // onion axe
        ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, OAItems.ONION_AXE.get())
                .define('#', OAItems.BUNCH_OF_ONIONS.get())
                .define('S', Items.STICK)
                .pattern("## ")
                .pattern("#S ")
                .pattern(" S ")
                .unlockedBy("has_bunch_of_onions_item", inventoryTrigger(ItemPredicate.Builder.item()
                        .of(OAItems.BUNCH_OF_ONIONS.get()).build()))
                .save(consumer);

        // Onion Hoe
        ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, OAItems.ONION_HOE.get())
                .define('#', OAItems.BUNCH_OF_ONIONS.get())
                .define('S', Items.STICK)
                .pattern("## ")
                .pattern(" S ")
                .pattern(" S ")
                .unlockedBy("has_bunch_of_onions_item", inventoryTrigger(ItemPredicate.Builder.item()
                        .of(OAItems.BUNCH_OF_ONIONS.get()).build()))
                .save(consumer);

//        // Iron
//        // Pickaxe
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_PICKAXE.get()),
                        Ingredient.of(Items.IRON_INGOT),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_IRON_PICKAXE.get()
                )
                .unlocks("has_iron_ingot", has(Items.IRON_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_IRON_PICKAXE.get()) + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_PICKAXE_EMPOWERED.get()),
                        Ingredient.of(Items.IRON_INGOT),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_IRON_PICKAXE.get()
                )
                .unlocks("has_iron_ingot", has(Items.IRON_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_IRON_PICKAXE.get()) + "_empowered_smithing")
                );

        // Shovel
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_SHOVEL.get()),
                        Ingredient.of(Items.IRON_INGOT),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_IRON_SHOVEL.get()
                )
                .unlocks("has_iron_ingot", has(Items.IRON_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_IRON_SHOVEL.get()) + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_SHOVEL_EMPOWERED.get()),
                        Ingredient.of(Items.IRON_INGOT),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_IRON_SHOVEL.get()
                )
                .unlocks("has_iron_ingot", has(Items.IRON_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_IRON_SHOVEL.get()) + "_empowered_smithing")
                );

        // Axe
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_AXE.get()),
                        Ingredient.of(Items.IRON_INGOT),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_IRON_AXE.get()
                )
                .unlocks("has_iron_ingot", has(Items.IRON_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_IRON_AXE.get()) + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_AXE_EMPOWERED.get()),
                        Ingredient.of(Items.IRON_INGOT),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_IRON_AXE.get()
                )
                .unlocks("has_iron_ingot", has(Items.IRON_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_IRON_AXE.get()) + "_empowered_smithing")
                );

        // Hoe
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_HOE.get()),
                        Ingredient.of(Items.IRON_INGOT),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_IRON_HOE.get()
                )
                .unlocks("has_iron_ingot", has(Items.IRON_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_IRON_HOE.get()) + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_HOE_EMPOWERED.get()),
                        Ingredient.of(Items.IRON_INGOT),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_IRON_HOE.get()
                )
                .unlocks("has_iron_ingot", has(Items.IRON_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_IRON_HOE.get()) + "_empowered_smithing")
                );

        // Gold
        // Pickaxe
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_PICKAXE.get()),
                        Ingredient.of(Items.GOLD_INGOT),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_GOLD_PICKAXE.get()
                )
                .unlocks("has_gold_ingot", has(Items.GOLD_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_GOLD_PICKAXE.get()) + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_PICKAXE_EMPOWERED.get()),
                        Ingredient.of(Items.GOLD_INGOT),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_GOLD_PICKAXE.get()
                )
                .unlocks("has_gold_ingot", has(Items.GOLD_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_GOLD_PICKAXE.get()) + "_empowered_smithing")
                );

        // Shovel
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_SHOVEL.get()),
                        Ingredient.of(Items.GOLD_INGOT),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_GOLD_SHOVEL.get()
                )
                .unlocks("has_gold_ingot", has(Items.GOLD_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_GOLD_SHOVEL.get()) + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_SHOVEL_EMPOWERED.get()),
                        Ingredient.of(Items.GOLD_INGOT),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_GOLD_SHOVEL.get()
                )
                .unlocks("has_gold_ingot", has(Items.GOLD_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_GOLD_SHOVEL.get()) + "_empowered_smithing")
                );

        // Axe
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_AXE.get()),
                        Ingredient.of(Items.GOLD_INGOT),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_GOLD_AXE.get()
                )
                .unlocks("has_gold_ingot", has(Items.GOLD_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_GOLD_AXE.get()) + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_AXE_EMPOWERED.get()),
                        Ingredient.of(Items.GOLD_INGOT),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_GOLD_AXE.get()
                )
                .unlocks("has_gold_ingot", has(Items.GOLD_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_GOLD_AXE.get()) + "_empowered_smithing")
                );

        // Hoe
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_HOE.get()),
                        Ingredient.of(Items.GOLD_INGOT),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_GOLD_HOE.get()
                )
                .unlocks("has_gold_ingot", has(Items.GOLD_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_GOLD_HOE.get()) + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_HOE_EMPOWERED.get()),
                        Ingredient.of(Items.GOLD_INGOT),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_GOLD_HOE.get()
                )
                .unlocks("has_gold_ingot", has(Items.GOLD_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_GOLD_HOE.get()) + "_empowered_smithing")
                );

        // Diamond
        // Pickaxe
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_IRON_PICKAXE.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_DIAMOND_PICKAXE.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_PICKAXE.get()) + "_from_iron_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_GOLD_PICKAXE.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_DIAMOND_PICKAXE.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_PICKAXE.get()) + "_from_gold_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_IRON_PICKAXE_EMPOWERED.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_DIAMOND_PICKAXE.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_PICKAXE.get()) + "_from_iron_empowered_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_GOLD_PICKAXE_EMPOWERED.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_DIAMOND_PICKAXE.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_PICKAXE.get()) + "_from_gold_empowered_smithing")
                );

        // Shovel
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_IRON_SHOVEL.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_DIAMOND_SHOVEL.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_SHOVEL.get()) + "_from_iron_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_GOLD_SHOVEL.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_DIAMOND_SHOVEL.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_SHOVEL.get()) + "_from_gold_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_IRON_SHOVEL_EMPOWERED.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_DIAMOND_SHOVEL.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_SHOVEL.get()) + "_from_iron_empowered_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_GOLD_SHOVEL_EMPOWERED.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_DIAMOND_SHOVEL.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_SHOVEL.get()) + "_from_gold_empowered_smithing")
                );

        // Axe
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_IRON_AXE.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_DIAMOND_AXE.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_AXE.get()) + "_from_iron_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_GOLD_AXE.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_DIAMOND_AXE.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_AXE.get()) + "_from_gold_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_IRON_AXE_EMPOWERED.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_DIAMOND_AXE.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_AXE.get()) + "_from_iron_empowered_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_GOLD_AXE_EMPOWERED.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_DIAMOND_AXE.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_AXE.get()) + "_from_gold_empowered_smithing")
                );

        // Hoe
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_IRON_HOE.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_DIAMOND_HOE.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_HOE.get()) + "_from_iron_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_GOLD_HOE.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_DIAMOND_HOE.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_HOE.get()) + "_from_gold_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_IRON_HOE_EMPOWERED.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_DIAMOND_HOE.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_HOE.get()) + "_from_iron_empowered_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_GOLD_HOE_EMPOWERED.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_DIAMOND_HOE.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_HOE.get()) + "_from_gold_empowered_smithing")
                );

        // Netherite
        // Pickaxe
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_DIAMOND_PICKAXE.get()),
                        Ingredient.of(Items.NETHERITE_INGOT),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_NETHERITE_PICKAXE.get()
                )
                .unlocks("has_netherite_ingot", has(Items.NETHERITE_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_NETHERITE_PICKAXE.get()) + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_DIAMOND_PICKAXE_EMPOWERED.get()),
                        Ingredient.of(Items.NETHERITE_INGOT),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_NETHERITE_PICKAXE.get()
                )
                .unlocks("has_netherite_ingot", has(Items.NETHERITE_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_NETHERITE_PICKAXE.get()) + "_empowered_smithing")
                );

        // Shovel
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_DIAMOND_SHOVEL.get()),
                        Ingredient.of(Items.NETHERITE_INGOT),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_NETHERITE_SHOVEL.get()
                )
                .unlocks("has_netherite_ingot", has(Items.NETHERITE_INGOT))
                        .save(consumer, new ResourceLocation(
                                OnionAdventures.MODID, getItemName(OAItems.ONION_NETHERITE_SHOVEL.get()) + "_smithing")
                        );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_DIAMOND_SHOVEL_EMPOWERED.get()),
                        Ingredient.of(Items.NETHERITE_INGOT),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_NETHERITE_SHOVEL.get()
                )
                .unlocks("has_netherite_ingot", has(Items.NETHERITE_INGOT))
                        .save(consumer, new ResourceLocation(
                                OnionAdventures.MODID, getItemName(OAItems.ONION_NETHERITE_SHOVEL.get()) + "_empowered_smithing")
                        );

        // Axe
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_DIAMOND_AXE.get()),
                        Ingredient.of(Items.NETHERITE_INGOT),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_NETHERITE_AXE.get()
                )
                .unlocks("has_netherite_ingot", has(Items.NETHERITE_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_NETHERITE_AXE.get()) + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_DIAMOND_AXE_EMPOWERED.get()),
                        Ingredient.of(Items.NETHERITE_INGOT),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_NETHERITE_AXE.get()
                )
                .unlocks("has_netherite_ingot", has(Items.NETHERITE_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_NETHERITE_AXE.get()) + "_empowered_smithing")
                );

        // Hoe
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_DIAMOND_HOE.get()),
                        Ingredient.of(Items.NETHERITE_INGOT),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_NETHERITE_HOE.get()
                )
                .unlocks("has_netherite_ingot", has(Items.NETHERITE_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_NETHERITE_HOE.get()) + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_DIAMOND_HOE_EMPOWERED.get()),
                        Ingredient.of(Items.NETHERITE_INGOT),
                        RecipeCategory.TOOLS,
                        OAItems.ONION_NETHERITE_HOE.get()
                )
                .unlocks("has_netherite_ingot", has(Items.NETHERITE_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_NETHERITE_HOE.get()) + "_empowered_smithing")
                );



        // Weapons
        // Onion Sword
        // Base
        ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, OAItems.ONION_SWORD.get())
                .define('#', OAItems.BUNCH_OF_ONIONS.get())
                .define('S', Items.STICK)
                .pattern(" # ")
                .pattern(" # ")
                .pattern(" S ")
                .unlockedBy("has_bunch_of_onions_item", inventoryTrigger(ItemPredicate.Builder.item()
                        .of(OAItems.BUNCH_OF_ONIONS.get()).build()))
                .save(consumer);

//        // Iron
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_SWORD.get()),
                        Ingredient.of(Items.IRON_INGOT),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_IRON_SWORD.get()
                )
                .unlocks("has_iron_ingot", has(Items.IRON_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_IRON_SWORD.get()) + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_SWORD_EMPOWERED.get()),
                        Ingredient.of(Items.IRON_INGOT),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_IRON_SWORD.get()
                )
                .unlocks("has_iron_ingot", has(Items.IRON_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_IRON_SWORD.get()) + "_empowered_smithing")
                );

        // Gold
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_SWORD.get()),
                        Ingredient.of(Items.GOLD_INGOT),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_GOLD_SWORD.get()
                )
                .unlocks("has_gold_ingot", has(Items.GOLD_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_GOLD_SWORD.get()) + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_SWORD_EMPOWERED.get()),
                        Ingredient.of(Items.GOLD_INGOT),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_GOLD_SWORD.get()
                )
                .unlocks("has_gold_ingot", has(Items.GOLD_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_GOLD_SWORD.get()) + "_empowered_smithing")
                );

        // Diamond
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_IRON_SWORD.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_DIAMOND_SWORD.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_SWORD.get()) + "_from_iron_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_GOLD_SWORD.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_DIAMOND_SWORD.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_SWORD.get()) + "_from_gold_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_IRON_SWORD_EMPOWERED.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_DIAMOND_SWORD.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_SWORD.get()) + "_from_iron_empowered_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_GOLD_SWORD_EMPOWERED.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_DIAMOND_SWORD.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_SWORD.get()) + "_from_gold_empowered_smithing")
                );

        // Netherite
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_DIAMOND_SWORD.get()),
                        Ingredient.of(Items.NETHERITE_INGOT),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_NETHERITE_SWORD.get()
                )
                .unlocks("has_netherite_ingot", has(Items.NETHERITE_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_NETHERITE_SWORD.get()) + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_DIAMOND_SWORD_EMPOWERED.get()),
                        Ingredient.of(Items.NETHERITE_INGOT),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_NETHERITE_SWORD.get()
                )
                .unlocks("has_netherite_ingot", has(Items.NETHERITE_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_NETHERITE_SWORD.get()) + "_empowered_smithing")
                );

        // Armour
        // Base
        // Onion Helmet
        ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, OAItems.ONION_HELMET.get())
                .define('#', OAItems.BUNCH_OF_ONIONS.get())
                .pattern("###")
                .pattern("# #")
                .unlockedBy("has_bunch_of_onions_item", inventoryTrigger(ItemPredicate.Builder.item()
                        .of(OAItems.BUNCH_OF_ONIONS.get()).build()))
                .save(consumer);

        // Onion ChestPlate
        ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, OAItems.ONION_CHESTPLATE.get())
                .define('#', OAItems.BUNCH_OF_ONIONS.get())
                .pattern("# #")
                .pattern("###")
                .pattern("###")
                .unlockedBy("has_bunch_of_onions_item", inventoryTrigger(ItemPredicate.Builder.item()
                        .of(OAItems.BUNCH_OF_ONIONS.get()).build()))
                .save(consumer);

        // Onion Leggings
        ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, OAItems.ONION_LEGGINGS.get())
                .define('#', OAItems.BUNCH_OF_ONIONS.get())
                .pattern("###")
                .pattern("# #")
                .pattern("# #")
                .unlockedBy("has_bunch_of_onions_item", inventoryTrigger(ItemPredicate.Builder.item()
                        .of(OAItems.BUNCH_OF_ONIONS.get()).build()))
                .save(consumer);

        // Onion Boots
        ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, OAItems.ONION_BOOTS.get())
                .define('#', OAItems.BUNCH_OF_ONIONS.get())
                .pattern("# #")
                .pattern("# #")
                .unlockedBy("has_bunch_of_onions_item", inventoryTrigger(ItemPredicate.Builder.item()
                        .of(OAItems.BUNCH_OF_ONIONS.get()).build()))
                .save(consumer);

        // Iron
        // Helmet
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_HELMET.get()),
                        Ingredient.of(Items.IRON_INGOT),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_IRON_HELMET.get()
                )
                .unlocks("has_iron_ingot", has(Items.IRON_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_IRON_HELMET.get()) + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_HELMET_EMPOWERED.get()),
                        Ingredient.of(Items.IRON_INGOT),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_IRON_HELMET.get()
                )
                .unlocks("has_iron_ingot", has(Items.IRON_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_IRON_HELMET.get()) + "_empowered_smithing")
                );

        // Chestplate
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_CHESTPLATE.get()),
                        Ingredient.of(Items.IRON_INGOT),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_IRON_CHESTPLATE.get()
                )
                .unlocks("has_iron_ingot", has(Items.IRON_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_IRON_CHESTPLATE.get()) + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_CHESTPLATE_EMPOWERED.get()),
                        Ingredient.of(Items.IRON_INGOT),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_IRON_CHESTPLATE.get()
                )
                .unlocks("has_iron_ingot", has(Items.IRON_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_IRON_CHESTPLATE.get()) + "_empowered_smithing")
                );

        // Leggings
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_LEGGINGS.get()),
                        Ingredient.of(Items.IRON_INGOT),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_IRON_LEGGINGS.get()
                )
                .unlocks("has_iron_ingot", has(Items.IRON_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_IRON_LEGGINGS.get()) + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_LEGGINGS_EMPOWERED.get()),
                        Ingredient.of(Items.IRON_INGOT),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_IRON_LEGGINGS.get()
                )
                .unlocks("has_iron_ingot", has(Items.IRON_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_IRON_LEGGINGS.get()) + "_empowered_smithing")
                );

        // Boots
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_BOOTS.get()),
                        Ingredient.of(Items.IRON_INGOT),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_IRON_BOOTS.get()
                )
                .unlocks("has_iron_ingot", has(Items.IRON_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_IRON_BOOTS.get()) + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_BOOTS_EMPOWERED.get()),
                        Ingredient.of(Items.IRON_INGOT),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_IRON_BOOTS.get()
                )
                .unlocks("has_iron_ingot", has(Items.IRON_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_IRON_BOOTS.get()) + "_empowered_smithing")
                );

        // Gold
        // Helmet
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_HELMET.get()),
                        Ingredient.of(Items.GOLD_INGOT),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_GOLD_HELMET.get()
                )
                .unlocks("has_gold_ingot", has(Items.GOLD_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_GOLD_HELMET.get()) + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_HELMET_EMPOWERED.get()),
                        Ingredient.of(Items.GOLD_INGOT),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_GOLD_HELMET.get()
                )
                .unlocks("has_gold_ingot", has(Items.GOLD_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_GOLD_HELMET.get()) + "_empowered_smithing")
                );

        // Chestplate
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_CHESTPLATE.get()),
                        Ingredient.of(Items.GOLD_INGOT),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_GOLD_CHESTPLATE.get()
                )
                .unlocks("has_gold_ingot", has(Items.GOLD_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_GOLD_CHESTPLATE.get()) + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_CHESTPLATE_EMPOWERED.get()),
                        Ingredient.of(Items.GOLD_INGOT),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_GOLD_CHESTPLATE.get()
                )
                .unlocks("has_gold_ingot", has(Items.GOLD_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_GOLD_CHESTPLATE.get()) + "_empowered_smithing")
                );

        // Leggings
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_LEGGINGS.get()),
                        Ingredient.of(Items.GOLD_INGOT),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_GOLD_LEGGINGS.get()
                )
                .unlocks("has_gold_ingot", has(Items.GOLD_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_GOLD_LEGGINGS.get()) + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_LEGGINGS_EMPOWERED.get()),
                        Ingredient.of(Items.GOLD_INGOT),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_GOLD_LEGGINGS.get()
                )
                .unlocks("has_gold_ingot", has(Items.GOLD_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_GOLD_LEGGINGS.get()) + "_empowered_smithing")
                );

        // Boots
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_BOOTS.get()),
                        Ingredient.of(Items.GOLD_INGOT),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_GOLD_BOOTS.get()
                )
                .unlocks("has_gold_ingot", has(Items.GOLD_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_GOLD_BOOTS.get()) + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_BOOTS_EMPOWERED.get()),
                        Ingredient.of(Items.GOLD_INGOT),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_GOLD_BOOTS.get()
                )
                .unlocks("has_gold_ingot", has(Items.GOLD_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_GOLD_BOOTS.get()) + "_empowered_smithing")
                );

        // Diamond
        // Helmet
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_IRON_HELMET.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_DIAMOND_HELMET.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_HELMET.get()) + "_from_iron" + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_GOLD_HELMET.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_DIAMOND_HELMET.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_HELMET.get()) + "_from_gold" + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_IRON_HELMET_EMPOWERED.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_DIAMOND_HELMET.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_HELMET.get()) + "_from_iron" + "_empowered_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_GOLD_HELMET_EMPOWERED.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_DIAMOND_HELMET.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_HELMET.get()) + "_from_gold" + "_empowered_smithing")
                );

        // Chestplate
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_IRON_CHESTPLATE.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_DIAMOND_CHESTPLATE.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_CHESTPLATE.get()) + "_from_iron" + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_GOLD_CHESTPLATE.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_DIAMOND_CHESTPLATE.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_CHESTPLATE.get()) + "_from_gold" + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_IRON_CHESTPLATE_EMPOWERED.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_DIAMOND_CHESTPLATE.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_CHESTPLATE.get()) + "_from_iron" + "_empowered_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_GOLD_CHESTPLATE_EMPOWERED.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_DIAMOND_CHESTPLATE.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_CHESTPLATE.get()) + "_from_gold" + "_empowered_smithing")
                );


        // Leggings
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_IRON_LEGGINGS.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_DIAMOND_LEGGINGS.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_LEGGINGS.get()) + "_from_iron" + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_GOLD_LEGGINGS.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_DIAMOND_LEGGINGS.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_LEGGINGS.get()) + "_from_gold" + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_IRON_LEGGINGS_EMPOWERED.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_DIAMOND_LEGGINGS.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_LEGGINGS.get()) + "_from_iron" + "_empowered_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_GOLD_LEGGINGS_EMPOWERED.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_DIAMOND_LEGGINGS.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_LEGGINGS.get()) + "_from_gold" + "_empowered_smithing")
                );

        // Boots
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_IRON_BOOTS.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_DIAMOND_BOOTS.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_BOOTS.get()) + "_from_iron" + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_GOLD_BOOTS.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_DIAMOND_BOOTS.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_BOOTS.get()) + "_from_gold" + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_IRON_BOOTS_EMPOWERED.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_DIAMOND_BOOTS.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_BOOTS.get()) + "_from_iron" + "_empowered_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_GOLD_BOOTS_EMPOWERED.get()),
                        Ingredient.of(Items.DIAMOND),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_DIAMOND_BOOTS.get()
                )
                .unlocks("has_diamond_gem", has(Items.DIAMOND))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_DIAMOND_BOOTS.get()) + "_from_gold" + "_empowered_smithing")
                );

        // Netherite
        // Helmet
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_DIAMOND_HELMET.get()),
                        Ingredient.of(Items.NETHERITE_INGOT),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_NETHERITE_HELMET.get()
                )
                .unlocks("has_netherite_ingot", has(Items.NETHERITE_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_NETHERITE_HELMET.get()) + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_DIAMOND_HELMET_EMPOWERED.get()),
                        Ingredient.of(Items.NETHERITE_INGOT),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_NETHERITE_HELMET.get()
                )
                .unlocks("has_netherite_ingot", has(Items.NETHERITE_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_NETHERITE_HELMET.get()) + "_empowered_smithing")
                );

        // Chestplate
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_DIAMOND_CHESTPLATE.get()),
                        Ingredient.of(Items.NETHERITE_INGOT),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_NETHERITE_CHESTPLATE.get()
                )
                .unlocks("has_netherite_ingot", has(Items.NETHERITE_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_NETHERITE_CHESTPLATE.get()) + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_DIAMOND_CHESTPLATE_EMPOWERED.get()),
                        Ingredient.of(Items.NETHERITE_INGOT),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_NETHERITE_CHESTPLATE.get()
                )
                .unlocks("has_netherite_ingot", has(Items.NETHERITE_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_NETHERITE_CHESTPLATE.get()) + "_empowered_smithing")
                );

        // Leggings
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_DIAMOND_LEGGINGS.get()),
                        Ingredient.of(Items.NETHERITE_INGOT),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_NETHERITE_LEGGINGS.get()
                )
                .unlocks("has_netherite_ingot", has(Items.NETHERITE_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_NETHERITE_LEGGINGS.get()) + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_DIAMOND_LEGGINGS_EMPOWERED.get()),
                        Ingredient.of(Items.NETHERITE_INGOT),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_NETHERITE_LEGGINGS.get()
                )
                .unlocks("has_netherite_ingot", has(Items.NETHERITE_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_NETHERITE_LEGGINGS.get()) + "_empowered_smithing")
                );

        // Boots
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_DIAMOND_BOOTS.get()),
                        Ingredient.of(Items.NETHERITE_INGOT),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_NETHERITE_BOOTS.get()
                )
                .unlocks("has_netherite_ingot", has(Items.NETHERITE_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_NETHERITE_BOOTS.get()) + "_smithing")
                );
        LegacyUpgradeRecipeBuilder.smithing(
                        Ingredient.of(OAItems.ONION_DIAMOND_BOOTS_EMPOWERED.get()),
                        Ingredient.of(Items.NETHERITE_INGOT),
                        RecipeCategory.COMBAT,
                        OAItems.ONION_NETHERITE_BOOTS.get()
                )
                .unlocks("has_netherite_ingot", has(Items.NETHERITE_INGOT))
                .save(consumer, new ResourceLocation(
                        OnionAdventures.MODID, getItemName(OAItems.ONION_NETHERITE_BOOTS.get()) + "_empowered_smithing")
                );


        // curios
        // Chakra Monocle
        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, OAItemsCurios.CHAKRA_MONOCLE.get())
                .define('#', Items.GLASS)
                .define('O', OAItems.ONION_CHAKRA_LEDGER.get())
                .pattern(" # ")
                .pattern("#O#")
                .pattern(" # ")
                .unlockedBy("has_chakra_ledger_item", inventoryTrigger(ItemPredicate.Builder.item()
                        .of(OAItems.ONION_CHAKRA_LEDGER.get()).build()))
                .save(consumer);
    }

    protected static void oreSmelting(@NotNull Consumer<FinishedRecipe> consumer, List<ItemLike> itemInputs,
                                      @NotNull RecipeCategory recipeCategory, @NotNull ItemLike itemOutput,
                                      float experience, int smeltTime, @NotNull String group) {

        oreCooking(consumer, RecipeSerializer.SMELTING_RECIPE, itemInputs, recipeCategory, itemOutput, experience,
                smeltTime, group, "_from_smelting");
    }

    protected static void oreBlasting(@NotNull Consumer<FinishedRecipe> consumer, List<ItemLike> itemInputs,
                                      @NotNull RecipeCategory recipeCategory, @NotNull ItemLike itemOutput,
                                      float experience, int smeltTime, @NotNull String group) {

        oreCooking(consumer, RecipeSerializer.BLASTING_RECIPE, itemInputs, recipeCategory, itemOutput, experience,
                smeltTime, group, "_from_blasting");
    }

    protected static void foodCrafting(Consumer<FinishedRecipe> consumer, List<ItemLike> itemInputs, ItemLike itemOutput,
                                       float experience, int smeltTime, String group) {

        oreSmelting(consumer, itemInputs, RecipeCategory.FOOD, itemOutput, experience, smeltTime, group);
        foodSmoking(consumer, itemInputs, RecipeCategory.FOOD, itemOutput, experience, smeltTime / 2, group);
        foodCampfire(consumer, itemInputs, RecipeCategory.FOOD, itemOutput, experience, smeltTime * 3, group);
    }

    protected static void foodSmoking(Consumer<FinishedRecipe> consumer, List<ItemLike> itemInputs, RecipeCategory recipeCategory,
                                      ItemLike itemOutput, float experience, int smeltTime, String group) {

        oreCooking(consumer, RecipeSerializer.SMOKING_RECIPE, itemInputs, recipeCategory, itemOutput, experience,
                smeltTime, group, "_from_smoking");
    }

    protected static void foodCampfire(Consumer<FinishedRecipe> consumer, List<ItemLike> itemInputs, RecipeCategory recipeCategory,
                                      ItemLike itemOutput, float experience, int smeltTime, String group) {

        oreCooking(consumer, RecipeSerializer.CAMPFIRE_COOKING_RECIPE, itemInputs, recipeCategory, itemOutput, experience,
                smeltTime, group, "_from_campfire");
    }

    protected static void oreCooking(@NotNull Consumer<FinishedRecipe> consumer,
                                     @NotNull RecipeSerializer<? extends AbstractCookingRecipe> serializer,
                                     List<ItemLike> itemInputs, @NotNull RecipeCategory recipeCategory, @NotNull ItemLike itemOutput,
                                     float experience, int cookingTime, @NotNull String group, String crafting_type) {

        for (ItemLike itemlike : itemInputs) {
            SimpleCookingRecipeBuilder.generic(
                            Ingredient.of(itemlike), recipeCategory, itemOutput, experience, cookingTime, serializer
                    )
                    .group(group)
                    .unlockedBy(
                            getHasName(itemlike), has(itemlike)
                    )
                    .save(consumer, new ResourceLocation(OnionAdventures.MODID, getItemName(itemOutput)) + crafting_type + "_" + getItemName(itemlike));
        }
    }

}
