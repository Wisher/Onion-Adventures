/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 *
 * Permission & a License is hereby granted to use this code under the conditions that:
 * 1) The project using this code is open-source and accessible publicly via a service such as GitHub, GitLab, etc.
 * 2) The Project using this code is copy-left under a license such as MIT, GNU General Public License v3.0, etc.
 */

package net.aliphaticus.onionadventures.api.item.armour;

import net.aliphaticus.onionadventures.api.item.IChakraEmpoweredArmourItem;
import net.aliphaticus.onionadventures.api.util.OALoggingUtils;
import net.aliphaticus.onionadventures.api.util.PlayerChakraUtils;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.ItemStack;

import java.util.function.Consumer;

/**
 * Parent class for empowered Armour items
 */
public abstract class AbstractOnionChakraArmourEmpoweredItem extends AbstractOnionChakraArmourItem implements IChakraEmpoweredArmourItem {

    /**
     * Slot of the Armour, used for updating the inventory
     */
    private final int armourSlot;

    /**
     * Invokes the {@link ArmorItem} constructor, and sets the additional properties of {@link net.aliphaticus.onionadventures.api.item.IChakraEmpoweredArmourItem IChakraEmpoweredArmourItem}
     * @param armorMaterial Can be a custom material
     * @param type For the Armour slot
     * @param activationCost Chakra cost to activate
     * @param useCost Chakra cost to use
     * @param armourSlot Slot the armour piece is in
     * @param properties Item {@link net.minecraft.world.item.Item.Properties Properties}
     * @param hoverTranslatable Shift Hover text
     */
    public AbstractOnionChakraArmourEmpoweredItem(ArmorMaterial armorMaterial, ArmorItem.Type type, int activationCost,
                                                  int useCost, int armourSlot, Properties properties, String[] hoverTranslatable) {
        super(armorMaterial, type, true, activationCost, useCost, properties, hoverTranslatable);

        // todo: ArmorItem.type.getslot.getIndex instead
        this.armourSlot = armourSlot;
    }

    @Override
    public <T extends LivingEntity> int damageItem(ItemStack stack, int amount, T entity, Consumer<T> onBroken) {
        // implement chakra system
        OALoggingUtils.logDebugInfo("Attempting to damage " + ((ServerPlayer) entity).getName().getString() + "'s "
                + stack.getDisplayName().getString());

        // if not enough chakra remaining, convert item then damage
        if (!hasEnoughChakra(getUseCost(), (ServerPlayer) entity)) {
            OALoggingUtils.logDebugInfo("Damaging then dis-empowering " + ((ServerPlayer) entity).getName().getString() + "'s " +
                    stack.getDisplayName().getString() + " by " + amount);

            stack.setDamageValue(stack.getDamageValue() + amount);
            updateItemEquipped((ServerPlayer) entity, stack, armourSlot);
            return 0;

            // else-if has enough chakra, don't damage and check remaining chakra
        } else {
            PlayerChakraUtils.subChakra((ServerPlayer) entity, getUseCost());

            // if not enough chakra remaining, convert item
            if (!hasEnoughChakra(getUseCost(), (ServerPlayer) entity)) {
                OALoggingUtils.logDebugInfo("Dis-Empowering & not damaging " + ((ServerPlayer) entity).getName().getString() + "'s " +
                        stack.getDisplayName().getString());

//                stack.setDamageValue(stack.getDamageValue() + amount);
                updateItemEquipped((ServerPlayer) entity, stack, armourSlot);
                return 0;
            } else {
                // don't damage as enough chakra existed to absorb damage
                OALoggingUtils.logDebugInfo("Not damaging " + ((ServerPlayer) entity).getName().getString() +
                        "'s " + stack.getDisplayName().getString());

                return super.damageItem(stack, 0, entity, onBroken);
            }
        }
    }

}
