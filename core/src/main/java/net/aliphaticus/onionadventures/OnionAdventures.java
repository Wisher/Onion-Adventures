/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures;

import com.mojang.logging.LogUtils;
import net.aliphaticus.onionadventures.api.networking.OAAPIPackets;
import net.aliphaticus.onionadventures.api.util.ModCompatUtils;
import net.aliphaticus.onionadventures.client.curio.CurioRenderers;
import net.aliphaticus.onionadventures.common.block.OABlocks;
import net.aliphaticus.onionadventures.common.core.dispenser.OADispenserProjectiles;
import net.aliphaticus.onionadventures.common.entity.OAEntities;
import net.aliphaticus.onionadventures.common.item.OACreativeModeTabs;
import net.aliphaticus.onionadventures.common.item.OAItemTier;
import net.aliphaticus.onionadventures.common.item.OAItems;
import net.aliphaticus.onionadventures.common.item.OAItemsCurios;
import net.minecraft.core.registries.Registries;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.CreativeModeTabEvent;
import net.minecraftforge.event.server.ServerStartingEvent;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import org.slf4j.Logger;

// The value here should match an entry in the META-INF/mods.toml file
@Mod(OnionAdventures.MODID)
public class OnionAdventures
{
    // Define mod id in a common place for everything to reference
    public static final String MODID = "onionadventures";

    // Directly reference a slf4j logger
    private static final Logger LOGGER = LogUtils.getLogger();

    // DeferredRegisters which don't fit into their own files
    public static final DeferredRegister<ConfiguredFeature<?, ?>> CONFIGURED_FEATURE_REGISTER = DeferredRegister.create(Registries.CONFIGURED_FEATURE, MODID);
    public static final DeferredRegister<PlacedFeature> PLACED_FEATURE_REGISTER = DeferredRegister.create(Registries.PLACED_FEATURE, MODID);

    public OnionAdventures() {
        IEventBus modEventBus = FMLJavaModLoadingContext.get().getModEventBus();

        // register the items
        OAItems.register(modEventBus);
        if (ModCompatUtils.isCuriosLoaded()) {
            OAItemsCurios.register(modEventBus);
        }


        // register the blocks
        OABlocks.register(modEventBus);

        OAEntities.register(modEventBus);

        // register the configured features
        CONFIGURED_FEATURE_REGISTER.register(modEventBus);

        // register the placed features
        PLACED_FEATURE_REGISTER.register(modEventBus);

        // Register the commonSetup method for mod-loading
        modEventBus.addListener(this::commonSetup);

        // Register ourselves for server and other game events we are interested in
        MinecraftForge.EVENT_BUS.register(this);

        // register creative tabs
        modEventBus.addListener(this::addCreative);
    }

    private void commonSetup(final FMLCommonSetupEvent event) {

        OAItemTier.onCommonSetup();
        OADispenserProjectiles.onCommonSetup();
        
        if (ModCompatUtils.isCuriosLoaded()) {
            LOGGER.info("Onion Adventures: Found Curios, integrating");
        }
        
        event.enqueueWork(() -> {
            OAAPIPackets.register();
        });

    }

    private void addCreative(CreativeModeTabEvent.BuildContents event) {
        if (event.getTab() == OACreativeModeTabs.ONION_ADVENTURES_TAB) {
            // items
            event.accept(OAItems.ONION);
            event.accept(OAItems.RED_ONION);
            event.accept(OAItems.ROASTED_ONION);
            event.accept(OAItems.BUNCH_OF_ONIONS);
            event.accept(OAItems.ONION_SEEDS);
            event.accept(OAItems.BROWN_ONION_SPAWN_EGG);
            event.accept(OAItems.RED_ONION_SPAWN_EGG);

            event.accept(OAItems.COOKED_ONION);
            event.accept(OAItems.ONION_CHAKRA_LEDGER);
            event.accept(OAItems.SOLID_CHAKRA);
            event.accept(OAItems.SOLID_CHAKRA_DENSE);

            // blocks
            event.accept(OABlocks.WILD_ONIONS);

            // block entities
            event.accept(OABlocks.CHAKRA_OBELISK);

            // curios
            if (ModCompatUtils.isCuriosLoaded()) {
                event.accept(OAItemsCurios.CHAKRA_MONOCLE);
            }

        }

        if (event.getTab() == OACreativeModeTabs.ONION_ADVENTURES_TOOL_TAB) {
            event.accept(OAItems.ONION_PICKAXE);
            event.accept(OAItems.ONION_SHOVEL);
            event.accept(OAItems.ONION_AXE);
            event.accept(OAItems.ONION_HOE);
            event.accept(OAItems.ONION_IRON_PICKAXE);
            event.accept(OAItems.ONION_IRON_SHOVEL);
            event.accept(OAItems.ONION_IRON_AXE);
            event.accept(OAItems.ONION_IRON_HOE);
            event.accept(OAItems.ONION_GOLD_PICKAXE);
            event.accept(OAItems.ONION_GOLD_SHOVEL);
            event.accept(OAItems.ONION_GOLD_AXE);
            event.accept(OAItems.ONION_GOLD_HOE);
            event.accept(OAItems.ONION_DIAMOND_PICKAXE);
            event.accept(OAItems.ONION_DIAMOND_SHOVEL);
            event.accept(OAItems.ONION_DIAMOND_AXE);
            event.accept(OAItems.ONION_DIAMOND_HOE);
            event.accept(OAItems.ONION_NETHERITE_PICKAXE);
            event.accept(OAItems.ONION_NETHERITE_SHOVEL);
            event.accept(OAItems.ONION_NETHERITE_AXE);
            event.accept(OAItems.ONION_NETHERITE_HOE);
        }

        if (event.getTab() == OACreativeModeTabs.ONION_ADVENTURES_WEAPON_TAB) {
            event.accept(OAItems.ONION_SWORD);
            event.accept(OAItems.ONION_IRON_SWORD);
            event.accept(OAItems.ONION_GOLD_SWORD);
            event.accept(OAItems.ONION_DIAMOND_SWORD);
            event.accept(OAItems.ONION_NETHERITE_SWORD);
        }

        if (event.getTab() == OACreativeModeTabs.ONION_ADVENTURES_ARMOUR_TAB) {
            event.accept(OAItems.ONION_HELMET);
            event.accept(OAItems.ONION_CHESTPLATE);
            event.accept(OAItems.ONION_LEGGINGS);
            event.accept(OAItems.ONION_BOOTS);

            event.accept(OAItems.ONION_IRON_HELMET);
            event.accept(OAItems.ONION_IRON_CHESTPLATE);
            event.accept(OAItems.ONION_IRON_LEGGINGS);
            event.accept(OAItems.ONION_IRON_BOOTS);

            event.accept(OAItems.ONION_GOLD_HELMET);
            event.accept(OAItems.ONION_GOLD_CHESTPLATE);
            event.accept(OAItems.ONION_GOLD_LEGGINGS);
            event.accept(OAItems.ONION_GOLD_BOOTS);

            event.accept(OAItems.ONION_DIAMOND_HELMET);
            event.accept(OAItems.ONION_DIAMOND_CHESTPLATE);
            event.accept(OAItems.ONION_DIAMOND_LEGGINGS);
            event.accept(OAItems.ONION_DIAMOND_BOOTS);

            event.accept(OAItems.ONION_NETHERITE_HELMET);
            event.accept(OAItems.ONION_NETHERITE_CHESTPLATE);
            event.accept(OAItems.ONION_NETHERITE_LEGGINGS);
            event.accept(OAItems.ONION_NETHERITE_BOOTS);
        }
    }

    // You can use SubscribeEvent and let the Event Bus discover methods to call
    @SubscribeEvent
    public void onServerStarting(ServerStartingEvent event)
    {
        // Do something when the server starts
        LOGGER.info("HELLO from OnionAdventures - Server Starting");
    }

    // You can use EventBusSubscriber to automatically register all static methods in the class annotated with @SubscribeEvent
    @Mod.EventBusSubscriber(modid = MODID, bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
    public static class ClientModEvents
    {
        @SubscribeEvent
        public static void onClientSetup(FMLClientSetupEvent event)
        {
            // Some client setup code
            LOGGER.info("HELLO FROM OnionAdventures - Client Setup");
            if (ModCompatUtils.isCuriosLoaded()) {
                CurioRenderers.register();
            }
        }
    }
}
