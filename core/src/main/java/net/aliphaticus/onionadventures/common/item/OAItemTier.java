/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.common.item;

import net.aliphaticus.onionadventures.OnionAdventures;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.Tier;
import net.minecraft.world.item.Tiers;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraftforge.common.TierSortingRegistry;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.function.Supplier;

public enum OAItemTier implements Tier {

    BASE(60, 2.0F, 0.0F, 0, 10, () -> {
        return Ingredient.of(OAItems.ONION.get());
    }),
    BASE_EMPOWERED(60, 4.0F, 3.0F, 2, 10, () -> {
        return Ingredient.of(OAItems.ONION.get());
    }),
    IRON(100, 6.0F, 2.0F, 2, 15, () -> {
        return Ingredient.of(Items.IRON_INGOT);
    }),
    IRON_EMPOWERED(100, 9.0F, 5.0F, 3, 15, () -> {
        return Ingredient.of(Items.IRON_INGOT);
    }),
    GOLD(80, 12.0F, 1.0F, 2, 50, () -> {
        return Ingredient.of(Items.GOLD_INGOT);
    }),
    GOLD_EMPOWERED(80, 16.0F, 4.0F, 3, 50, () -> {
        return Ingredient.of(Items.GOLD_INGOT);
    }),
    DIAMOND(150, 8.0F, 4.0F, 3, 20, () -> {
        return Ingredient.of(Items.DIAMOND);
    }),
    DIAMOND_EMPOWERED(150, 12.0F, 7.0F, 4, 20, () -> {
        return Ingredient.of(Items.DIAMOND);
    }),
    NETHERITE(200, 11.0F, 6.0F, 4, 25, () -> {
        return Ingredient.of(Items.NETHERITE_INGOT);
    }),
    NETHERITE_EMPOWERED(200, 18.0F, 10.0F, 5, 25, () -> {
        return Ingredient.of(Items.NETHERITE_INGOT);
    });

    private final int maxUses;
    private final float efficiency;
    private final float attackDamage;
    private final int harvestLevel;
    private final int enchantability;
    private final Ingredient repairIngredient;

    OAItemTier(int maxUses, float efficiency, float attackDamage,
               int harvestLevel, int enchantability, Supplier<Ingredient> repairIngredient) {

        this.maxUses = maxUses;
        this.efficiency = efficiency;
        this.attackDamage = attackDamage;
        this.harvestLevel = harvestLevel;
        this.enchantability = enchantability;
        this.repairIngredient = repairIngredient.get();

    }

    @Override
    public int getUses() {
        return this.maxUses;
    }

    @Override
    public float getSpeed() {
        return this.efficiency;
    }

    @Override
    public float getAttackDamageBonus() {
        return this.attackDamage;
    }

    @Override
    public int getLevel() {
        return this.harvestLevel;
    }

    @Override
    public int getEnchantmentValue() {
        return this.enchantability;
    }

    @Override
    public @NotNull Ingredient getRepairIngredient() {
        return this.repairIngredient;
    }

    public static void onCommonSetup() {
        TierSortingRegistry.registerTier(BASE, new ResourceLocation(OnionAdventures.MODID, "base"),
                List.of(Tiers.WOOD), List.of());
        TierSortingRegistry.registerTier(BASE_EMPOWERED, new ResourceLocation(OnionAdventures.MODID, "base_empowered"),
                List.of(Tiers.STONE), List.of());

        TierSortingRegistry.registerTier(IRON, new ResourceLocation(OnionAdventures.MODID, "iron"),
                List.of(Tiers.IRON), List.of());
        TierSortingRegistry.registerTier(IRON_EMPOWERED, new ResourceLocation(OnionAdventures.MODID, "iron_empowered"),
                List.of(Tiers.DIAMOND), List.of());

        TierSortingRegistry.registerTier(GOLD, new ResourceLocation(OnionAdventures.MODID, "gold"),
                List.of(Tiers.IRON), List.of());
        TierSortingRegistry.registerTier(GOLD_EMPOWERED, new ResourceLocation(OnionAdventures.MODID, "gold_empowered"),
                List.of(Tiers.DIAMOND), List.of());

        TierSortingRegistry.registerTier(DIAMOND, new ResourceLocation(OnionAdventures.MODID, "diamond"),
                List.of(Tiers.DIAMOND), List.of());
        TierSortingRegistry.registerTier(DIAMOND_EMPOWERED, new ResourceLocation(OnionAdventures.MODID, "diamond_empowered"),
                List.of(Tiers.NETHERITE), List.of());

        TierSortingRegistry.registerTier(NETHERITE, new ResourceLocation(OnionAdventures.MODID, "netherite"),
                List.of(Tiers.NETHERITE), List.of());
        TierSortingRegistry.registerTier(NETHERITE_EMPOWERED, new ResourceLocation(OnionAdventures.MODID, "netherite_empowered"),
                List.of(NETHERITE), List.of());
    }

}
