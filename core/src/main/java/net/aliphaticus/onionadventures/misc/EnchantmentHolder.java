/*
 * Copyright (c) 2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.misc;

import net.minecraft.world.item.enchantment.Enchantment;

public record EnchantmentHolder(Enchantment enchantment, int level) {
}
