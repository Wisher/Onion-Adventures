/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.common.entity.custom.mobs;

import net.aliphaticus.onionadventures.api.entity.goal.ApproachLivingEntityGoal;
import net.aliphaticus.onionadventures.api.util.PlayerChakraUtils;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.*;
import net.minecraft.world.entity.ai.goal.target.NearestAttackableTargetGoal;
import net.minecraft.world.entity.animal.Animal;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import software.bernie.geckolib.animatable.GeoEntity;
import software.bernie.geckolib.core.animatable.GeoAnimatable;
import software.bernie.geckolib.core.animatable.instance.AnimatableInstanceCache;
import software.bernie.geckolib.core.animatable.instance.SingletonAnimatableInstanceCache;
import software.bernie.geckolib.core.animation.*;
import software.bernie.geckolib.core.object.PlayState;

public class RedOnionEntity extends Monster implements GeoEntity {

    private static final EntityDataAccessor<Integer> AGE = SynchedEntityData.defineId(RedOnionEntity.class, EntityDataSerializers.INT);
    private static final int MAX_AGE = 72000; // 3 in-game days

    private AnimatableInstanceCache cache = new SingletonAnimatableInstanceCache(this);

    private final int CHAKRA = 5;

    public RedOnionEntity(EntityType<? extends Monster> entityType, Level level) {
        super(entityType, level);
        this.xpReward = 5;
    }

    public static AttributeSupplier setAttributes() {
        return Monster.createMobAttributes()
                .add(Attributes.FOLLOW_RANGE, 16D)
                .add(Attributes.MAX_HEALTH, 15D)
                .add(Attributes.ATTACK_DAMAGE, 3.0F)
                .add(Attributes.ATTACK_SPEED, 1.5F)
                .add(Attributes.ATTACK_KNOCKBACK, 2D)
                .add(Attributes.MOVEMENT_SPEED, 0.2F)
                .build();
    }

    @Override
    protected void registerGoals() {
        this.goalSelector.addGoal(1, new FloatGoal(this));
        this.goalSelector.addGoal(2, new MeleeAttackGoal(this, 1.2D, false));
        this.goalSelector.addGoal(5, new WaterAvoidingRandomStrollGoal(this, 1.0D));
        this.goalSelector.addGoal(6, new RandomLookAroundGoal(this));

        // group up and seek brown onions
        this.goalSelector.addGoal(7, new FollowMobGoal(this, 1.0D, 2.0F, 32F));
        this.goalSelector.addGoal(8, new ApproachLivingEntityGoal<BrownOnionEntity>(
                this, BrownOnionEntity.class,  1.0F, 12.0F, 32.0F, 64.0F,
                0.0002083F)); // 1 / (20ticks x 240s) ie on average once every 4m
        this.goalSelector.addGoal(9, new ApproachLivingEntityGoal<ServerPlayer>(
                this, ServerPlayer.class,  1.0F, 24.0F, 32.0F, 80.0F,
                0.0001388F)); // 1 / (20ticks x 360s) ie on average once every 6m

        this.targetSelector.addGoal(2, new NearestAttackableTargetGoal<>(this, Player.class, true));
        this.targetSelector.addGoal(3, new NearestAttackableTargetGoal<>(this, BrownOnionEntity.class, true));
        this.targetSelector.addGoal(4, new NearestAttackableTargetGoal<>(this, Animal.class, true));
    }

    @Override
    public void die(DamageSource damageSource) {
        // if killed by a player
        if (damageSource.getEntity() instanceof ServerPlayer player) {
            // give chakra
            PlayerChakraUtils.addChakra(player, CHAKRA);
        }

        super.die(damageSource);
    }

    public Integer getOnionAge() {
        return this.entityData.get(AGE);
    }

    public void setOnionAge(int amount) {
        this.entityData.set(AGE, amount);
    }

    public void addOnionAge(int amountToAdd) {
        this.entityData.set(AGE, getOnionAge() + amountToAdd);
    }

    @Override
    protected void defineSynchedData() {
        super.defineSynchedData();
        this.entityData.define(AGE, 0);
    }

    @Override
    public void addAdditionalSaveData(CompoundTag nbt) {
        super.addAdditionalSaveData(nbt);
        nbt.putInt("onion_age", this.getOnionAge());
    }

    @Override
    public void readAdditionalSaveData(CompoundTag nbt) {
        super.readAdditionalSaveData(nbt);
        this.setOnionAge(nbt.getInt("onion_age"));
    }

    @Override
    public boolean removeWhenFarAway(double p_21542_) {
        return false;
    }

    @Override
    public int getExperienceReward() {
        return this.xpReward;
    }

    @Override
    public void tick() {
        super.tick();
        addOnionAge(1);

        if (getOnionAge() >=  MAX_AGE) {
            this.remove(RemovalReason.DISCARDED);
        }

    }

    @Override
    public void registerControllers(AnimatableManager.ControllerRegistrar controllerRegistrar) {
        controllerRegistrar.add(new AnimationController<>(this, "controller", 0, this::predicate));
    }

    private <T extends GeoAnimatable> PlayState predicate(AnimationState<T> tAnimationState) {
        if (tAnimationState.isMoving()) {
            tAnimationState.getController().setAnimation(RawAnimation.begin().then("animation.red_onion.walk", Animation.LoopType.LOOP));
            return PlayState.CONTINUE;
        }

        tAnimationState.getController().setAnimation(RawAnimation.begin().then("animation.red_onion.idle", Animation.LoopType.PLAY_ONCE));
        return PlayState.CONTINUE;
    }

    @Override
    public boolean canBeLeashed(Player player) {
        return false;
    }

    @Override
    public AnimatableInstanceCache getAnimatableInstanceCache() {
        return cache;
    }
}
