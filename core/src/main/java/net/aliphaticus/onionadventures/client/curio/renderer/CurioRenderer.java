/*
 * Copyright (c) 2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.client.curio.renderer;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.aliphaticus.onionadventures.OnionAdventures;
import net.minecraft.client.model.EntityModel;
import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.ItemRenderer;
import net.minecraft.client.renderer.entity.RenderLayerParent;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.ItemStack;
import top.theillusivec4.curios.api.SlotContext;
import top.theillusivec4.curios.api.client.ICurioRenderer;

public class CurioRenderer implements ICurioRenderer {

    private final ResourceLocation texture;
    private final HumanoidModel<LivingEntity> model;
    private final float xScale;
    private final float yScale;
    private final float zScale;

    public CurioRenderer(String texturePath, HumanoidModel<LivingEntity> model) {
        this(new ResourceLocation(OnionAdventures.MODID, "textures/item/" + texturePath + ".png"), model);
    }

    public CurioRenderer(String texturePath, HumanoidModel<LivingEntity> model, float xScale, float yScale, float zScale) {
        this(new ResourceLocation(OnionAdventures.MODID, "textures/item/" + texturePath + ".png"), model, xScale, yScale, zScale);
    }

    public CurioRenderer(ResourceLocation texture, HumanoidModel<LivingEntity> model) {
        this.texture = texture;
        this.model = model;
        this.xScale = 1.0F;
        this.yScale = 1.0F;
        this.zScale = 1.0F;
    }

    public CurioRenderer(ResourceLocation texture, HumanoidModel<LivingEntity> model, float xScale, float yScale, float zScale) {
        this.texture = texture;
        this.model = model;
        this.xScale = xScale;
        this.yScale = yScale;
        this.zScale = zScale;
    }

    protected ResourceLocation getTexture() {
        return texture;
    }

    protected HumanoidModel<LivingEntity> getModel() {
        return model;
    }


    @Override
    public <T extends LivingEntity, M extends EntityModel<T>> void render(
            ItemStack itemStack, SlotContext slotContext,
            PoseStack poseStack,
            RenderLayerParent<T, M> renderLayerParent,
            MultiBufferSource multiBufferSource,
            int light, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks,
            float netHeadYaw, float headPitch) {

        HumanoidModel<LivingEntity> model = getModel();
        ICurioRenderer.followBodyRotations(slotContext.entity(), model);
        render(poseStack, multiBufferSource, light, itemStack.hasFoil());
    }

    protected void render(PoseStack poseStack, MultiBufferSource buffer, int light, boolean hasFoil) {
        RenderType renderType = model.renderType(getTexture());
        VertexConsumer vertexBuilder = ItemRenderer.getFoilBuffer(buffer, renderType, false, hasFoil);
        poseStack.scale(xScale, yScale, zScale);
        model.renderToBuffer(poseStack, vertexBuilder, light, OverlayTexture.NO_OVERLAY, 1, 1, 1, 1);
    }

}
