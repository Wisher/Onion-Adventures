- [Changelog](#changelog)
  - [1.19.4](#1194)
    - [0.0.10.0](#00100)
    - [0.0.9.0](#0090)
    - [0.0.8.0](#0080)
    - [0.0.7.0](#0070)
    - [0.0.6.1](#0061)
    - [0.0.6.0](#0060)
  - [1.19.3](#1193)
    - [0.0.5.0](#0050)
    - [0.0.4.0](#0040)
    - [0.0.3.0](#0030)
    - [0.0.2.1](#0021)
    - [0.0.2.0](#0020)
    - [0.0.1.0](#0010)
# Changelog

## 1.19.4

### 0.0.10.0

Increased cost to empower Red Onion

Prevented the Red Onion from being leashed

Added AI for Finding living entities, ie Red and Brown onions will now seek each other out, Red onions will also seek out players

Added Guidebook, depends on Patchouli being installed, can be crafted and is also given to players when they first join a world

Brown Onions now will consume nearby Solid Chakra and Dense Solid chakra, this increases their friendliness

Brown Onions will drop gifts when they have enough friendlines, higher friendliness increases the range of gifts they drop

Brown Onions will repair items in nearby players inventories when they have enough friendliness, repairing more damage the higher their friendliness

Added lifesteal and self-healing to brown onions

Fix bug with player chakra not being copied on dimension change

Brown onions now take only 55% damage from explosions, to reduce chances of creepers wiping out the local population

Red Onions now have an AGE NBT Tag, they will despawn after 3 ingame days

Brown Onions now periodically grant chakra to nearby players, scales with friendliness

Brown Onions now have a FRIENDLINESS NBT Tag 

Players now lose chakra on death, 5 or 2.5% whichever is highest

decrease random chakra to one on average every 5 minutes

### 0.0.9.0

Fix bug with empowered items not consuming chakra when they had less then the use cost and the use cost was greater than 1

Added `chakra` command, which has subcommands get, broadcast, set, add, sub  

Added Chakra Monocle Curio Charm

Added Red Onion

Added Red Onion to Red Onion mob loottable (garunteed)

Added Volatile Red Onion (throwable that explodes after 10s or on contact with entity/block)

Added Curios API integration

Fixed bug with armour duplicating due to new packet inventory manipulation

### 0.0.8.0

fix bug with missing empowered armour textures

Add drops to entities (Red, Brown Onions)
Update drops from Wild Onions

Improve/Expand API Javadoc

Cleanup the code

Fix mod version, now other mods can easily specify this mods version as a dependency

### 0.0.7.0

Added recipes for smithing empowered tools (disempowers them)

### 0.0.6.1

added some additional onion lore to the cooked onion

### 0.0.6.0

Updated to Minecraft 1.19.4, Forge 45.1.0

Added GeckoLib as a required dependency

Updated License 
Granted limited use for the API package License

Added mod jars (api and whole mod) to GitLabs Maven repository

Added Brown and Red Onion mobs
Added some behavious and world spawning for the onion mobs

Fixed missing translation key for Onion Adventures Weapons Tab
Fixed bug with eating a Roasted Onion when empowering it

Add issues to Mods.TOML

Make Hover Text on Chakra Obelisk Client only

## 1.19.3

### 0.0.5.0

Fixed Boundbox of Wild Onions and Onion Crop
Fixed bug with empowered food being picked up and craashing the game
Fixed Enchantments and other NBT Tags not being copied when Dis/Empowering items
Fixed bug with dropped Chakra Empowerable itemstacks being set to 1 instead of their original count

Added Smithing Tiered Armours (Iron, Gold, Diamond, Netherite)
Added Smithing Tool Tiers
Added Chakra Obelisk, used to get Solid and Dense Solid Chakra
Rebalanced Existing Armour Tiers
Rebalanced Existing Tool Tiers
Added Empowered Roast Onion
Added Solid and Dense Solid Chakra, when used gives chakra
Added Hover text to Roasted/Empowered Roasted Onion
Removed Effects from normal Roast Onion
Roasted Onion now costs 50 chakra to activate (each if stacked)

Updated API to check item count aswell as tags when scanning inventory for dis/empowering items
Updated API Classnames
Upadted CoreMod classnames

Added a creative tab for armours and moved armour into that tab
Added a creative tab for tools and moved tools into that tab
Added a creative tab for weapons and moved weapons into that tab

### 0.0.4.0

BugFix, fixed chakra calculation logic on WildOnions

Created and implemented API, this allows other modders to make hook into Onion Adventures

### 0.0.3.0

Added Wild Onons

Added World Generation

Added Wild Onions to World Generation, 2 variants, one for #forge:is_plains, another for #forge:is_cold/overworld

Converted Data to be generated via code, except biome_modifiers

### 0.0.2.1

Bugfix, fixed empowering the wrong item sometimes

### 0.0.2.0

Added debug logging

### 0.0.1.0

Initial Alpha Release