/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.common.item;

import net.aliphaticus.onionadventures.OnionAdventures;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.util.LazyLoadedValue;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.Ingredient;
import org.jetbrains.annotations.NotNull;

import java.util.function.Supplier;

public enum OAArmourMaterials implements ArmorMaterial {
    BASE(
            "onion_base",
            10,
            new int[] {1, 3, 2, 1},
            5,
            SoundEvents.ARMOR_EQUIP_LEATHER,
            0F,
            0F,
            () -> Ingredient.of(OAItems.ONION.get())
    ),
    BASE_EMPOWERED(
            "onion_base_empowered",
            10,
            new int[] {2, 5, 4, 1},
            5,
            SoundEvents.ARMOR_EQUIP_LEATHER,
            0F,
            0F,
            () -> Ingredient.of(OAItems.ONION.get())
    ),
    IRON(
            "onion_iron",
            18,
            new int[] {2, 6, 5, 2},
            10,
            SoundEvents.ARMOR_EQUIP_LEATHER,
            0F,
            0F,
            () -> Ingredient.of(Items.IRON_INGOT)
    ),
    IRON_EMPOWERED(
            "onion_iron_empowered",
            18,
            new int[] {3, 7, 6, 3},
            10,
            SoundEvents.ARMOR_EQUIP_LEATHER,
            1F,
            0.025F,
            () -> Ingredient.of(Items.IRON_INGOT)
    ),
    GOLD(
            "onion_gold",
            14,
            new int[] {2, 5, 3, 2},
            50,
            SoundEvents.ARMOR_EQUIP_LEATHER,
            0F,
            0F,
            () -> Ingredient.of(Items.GOLD_INGOT)
    ),
    GOLD_EMPOWERED(
            "onion_gold_empowered",
            14,
            new int[] {3, 6, 5, 3},
            50,
            SoundEvents.ARMOR_EQUIP_LEATHER,
            1F,
            0.01F,
            () -> Ingredient.of(Items.GOLD_INGOT)
    ),
    DIAMOND(
            "onion_diamond",
            22,
            new int[] {3, 8, 6, 3},
            17,
            SoundEvents.ARMOR_EQUIP_LEATHER,
            2F,
            0.02F,
            () -> Ingredient.of(Items.DIAMOND)
    ),
    DIAMOND_EMPOWERED(
            "onion_diamond_empowered",
            22,
            new int[] {4, 10, 8, 5},
            17,
            SoundEvents.ARMOR_EQUIP_LEATHER,
            3F,
            0.05F,
            () -> Ingredient.of(Items.DIAMOND)
    ),
    NETHERITE(
            "onion_netherite",
            36,
            new int[] {4, 9, 7, 4},
            25,
            SoundEvents.ARMOR_EQUIP_LEATHER,
            3F,
            0.1F,
            () -> Ingredient.of(Items.NETHERITE_INGOT)
    ),
    NETHERITE_EMPOWERED(
            "onion_netherite_empowered",
            36,
            new int[] {6, 12, 10, 7},
            25,
            SoundEvents.ARMOR_EQUIP_LEATHER,
            6F,
            0.15F,
            () -> Ingredient.of(Items.NETHERITE_INGOT)
    );

    private static final int[] HEALTH_PER_SLOT = new int[]{10, 10, 10, 10};
    private final String name;
    private final int durabilityMultiplier;
    private final int[] slotDefence;
    private final int enchantmentValue;
    private final SoundEvent equipSound;
    private final float toughness;
    private final float knockbackResistance;
    private final LazyLoadedValue<Ingredient> repairIngredient;

    private OAArmourMaterials(String name, int durabilityMultiplier, int[] slotDefense, int enchantmentValue,
                              SoundEvent equipSound, float toughness, float knockbackResistance,
                              Supplier<Ingredient> repairMaterial) {
        this.name = name;
        this.durabilityMultiplier = durabilityMultiplier;
        this.slotDefence = slotDefense;
        this.enchantmentValue = enchantmentValue;
        this.equipSound = equipSound;
        this.toughness = toughness;
        this.knockbackResistance = knockbackResistance;
        this.repairIngredient = new LazyLoadedValue<>(repairMaterial);
    }

    @Override
    public int getDurabilityForType(ArmorItem.Type type) {
        return HEALTH_PER_SLOT[type.getSlot().getIndex()] * this.durabilityMultiplier;
    }

    @Override
    public int getDefenseForType(ArmorItem.Type type) {
        return this.slotDefence[type.getSlot().getIndex()];
    }

    public int getEnchantmentValue() {
        return this.enchantmentValue;
    }

    public @NotNull SoundEvent getEquipSound() {
        return this.equipSound;
    }

    public @NotNull Ingredient getRepairIngredient() {
        return this.repairIngredient.get();
    }

    /**
     * name is prepended to armour model filename, e.g. onion_base_layer_1.png
     *
     * @return name
     */
    public @NotNull String getName() {
        return OnionAdventures.MODID + ":" + this.name;
    }

    public float getToughness() {
        return this.toughness;
    }

    public float getKnockbackResistance() {
        return this.knockbackResistance;
    }
    }
