/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 *
 * Permission & a License is hereby granted to use this code under the conditions that:
 * 1) The project using this code is open-source and accessible publicly via a service such as GitHub, GitLab, etc.
 * 2) The Project using this code is copy-left under a license such as MIT, GNU General Public License v3.0, etc.
 */

package net.aliphaticus.onionadventures.api.item;

import net.aliphaticus.onionadventures.api.capability.chakra.PlayerOnionChakra;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import org.jetbrains.annotations.NotNull;

/**
 * Base class for empower-able Chakra Items
 */
public abstract class AbstractChakraItem extends Item implements IChakraItem {

    /**
     * {@link PlayerOnionChakra PlayerOnionChakra} cost to activate
     */
    private final int activationCost;
    /**
     * {@link PlayerOnionChakra PlayerOnionChakra} cost to use the item
     */
    private final int useCost;
    /**
     * Shift hover text
     */
    private final String[] hoverTranslatable;

    /**
     * Calls the {@link Item} constructor and implements the additional {@link IChakraItem} parameters
     * @param activationCost {@link PlayerOnionChakra PlayerOnionChakra} cost to activate
     * @param useCost {@link PlayerOnionChakra PlayerOnionChakra} cost to use the item
     * @param hoverTranslatable Shift hover text String[]
     * @param properties Item {@link net.minecraft.world.item.Item.Properties Properties}
     */
    public AbstractChakraItem(int activationCost, int useCost, String[] hoverTranslatable, Properties properties) {
        super(properties);

        this.activationCost = activationCost;
        this.useCost = useCost;
        this.hoverTranslatable = hoverTranslatable;
    }

    @Override
    public int getActivationCost() {
        return this.activationCost;
    }

    @Override
    public int getUseCost() {
        return this.useCost;
    }

    @Override
    public boolean getEmpowered() {
        return false;
    }

    @Override
    public String[] getHoverTranslatable() {
        return this.hoverTranslatable;
    }

    @Override
    public @NotNull InteractionResultHolder<ItemStack> use(@NotNull Level level, @NotNull Player player,
                                                           @NotNull InteractionHand hand) {

        if (hand == InteractionHand.MAIN_HAND && !level.isClientSide()) {
            updateItem(player, player.getItemInHand(hand));
        }

        return InteractionResultHolder.pass(player.getItemInHand(hand));
    }

    @Override
    public boolean isFoil(@NotNull ItemStack stack) {
        return getEmpowered();
    }
}
