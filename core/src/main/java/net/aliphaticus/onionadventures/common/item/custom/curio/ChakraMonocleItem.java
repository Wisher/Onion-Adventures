/*
 * Copyright (c) 2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.common.item.custom.curio;

import com.mojang.blaze3d.vertex.PoseStack;
import net.aliphaticus.onionadventures.api.client.chakra.ClientOnionChakraData;
import net.aliphaticus.onionadventures.common.item.OAItemsCurios;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiComponent;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.type.capability.ICurioItem;

import java.util.concurrent.atomic.AtomicBoolean;

public class ChakraMonocleItem extends Item implements ICurioItem {
    public ChakraMonocleItem(Properties properties) {
        super(properties);
    }

    public static class Hud {

        public static void render(PoseStack poseStack) {
            Minecraft mc = Minecraft.getInstance();

            int x = mc.getWindow().getGuiScaledWidth() - mc.getWindow().getGuiScaledWidth() + 4;
            int y = mc.getWindow().getGuiScaledHeight() - 10;

            GuiComponent.drawString(poseStack, mc.font, "Chakra: " + ClientOnionChakraData.getPlayerOnionChakra(),
                    x, y, 0x00ff00);
        }

    }

    public static boolean hasMonocle(LivingEntity entity) {

        AtomicBoolean isEquipped = new AtomicBoolean(false);
        CuriosApi.getCuriosHelper().getEquippedCurios(entity).ifPresent(consumer -> {

                    for (int i = 0; i < consumer.getSlots(); i++) {
                        if (ItemStack.matches(consumer.getStackInSlot(i), new ItemStack(OAItemsCurios.CHAKRA_MONOCLE.get()))) {
                            isEquipped.set(true);
                        }
                    }

                }
        );

        return isEquipped.get();
    }

}
