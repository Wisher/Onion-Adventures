/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 *
 * Permission & a License is hereby granted to use this code under the conditions that:
 * 1) The project using this code is open-source and accessible publicly via a service such as GitHub, GitLab, etc.
 * 2) The Project using this code is copy-left under a license such as MIT, GNU General Public License v3.0, etc.
 */

package net.aliphaticus.onionadventures.api.item;

/**
 * Parent class for empowered items
 */
public abstract class AbstractChakraEmpoweredItem extends AbstractChakraItem implements IChakraEmpoweredItem {

    /**
     * Calls the {@link AbstractChakraItem} constructor, as properties are shared
     * @param activationCost Chakra cost to activate
     * @param useCost Chakra cost to use
     * @param hoverTranslatable Shift hover text
     * @param properties Item {@link net.minecraft.world.item.Item.Properties Properties}
     */
    public AbstractChakraEmpoweredItem(int activationCost, int useCost, String[] hoverTranslatable, Properties properties) {
        super(activationCost, useCost, hoverTranslatable, properties);
    }

    @Override
    public boolean getEmpowered() {
        return true;
    }

}
