/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.common.event;

import net.aliphaticus.onionadventures.OnionAdventures;
import net.aliphaticus.onionadventures.api.capability.chakra.PlayerOnionChakraProvider;
import net.aliphaticus.onionadventures.api.item.AbstractChakraItem;
import net.aliphaticus.onionadventures.api.item.IChakraEmpoweredArmourItem;
import net.aliphaticus.onionadventures.api.item.IChakraEmpoweredItem;
import net.aliphaticus.onionadventures.api.item.IChakraItem;
import net.aliphaticus.onionadventures.api.item.food.AbstractChakraFoodItem;
import net.aliphaticus.onionadventures.api.networking.OAAPIPackets;
import net.aliphaticus.onionadventures.api.networking.packet.ChakraDataSyncS2CPacket;
import net.aliphaticus.onionadventures.api.util.OALoggingUtils;
import net.aliphaticus.onionadventures.api.util.PlayerChakraUtils;
import net.aliphaticus.onionadventures.common.item.custom.projectile.RedOnionEmpoweredItem;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.common.capabilities.RegisterCapabilitiesEvent;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.EntityJoinLevelEvent;
import net.minecraftforge.event.entity.item.ItemTossEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.player.EntityItemPickupEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.LogicalSide;
import net.minecraftforge.fml.common.Mod;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

@Mod.EventBusSubscriber(modid = OnionAdventures.MODID)
public class OAEvents {

    // add nbt to player (OnionChakra currently)
    @SubscribeEvent
    public static void attachCapabilitiesToPlayer(AttachCapabilitiesEvent<Entity> event) {
        if (event.getObject() instanceof Player) {
            if (!event.getObject().getCapability(PlayerOnionChakraProvider.PLAYER_ONION_CHAKRA).isPresent()) {
                OALoggingUtils.logDebugInfo("Attaching new PlayerOnionChakra to player");

                event.addCapability(new ResourceLocation(OnionAdventures.MODID, "player_chakra"),
                        new PlayerOnionChakraProvider());
            }
        }
    }

    // copy nbt to cloned player on death or dimension change
    // if death subtract chakra
    @SubscribeEvent
    public static void onPlayerCloned(PlayerEvent.Clone event) {
        copyPlayerCapabilities(event);

        if (event.isWasDeath()) {
            ServerPlayer player = (ServerPlayer) event.getEntity();
            PlayerChakraUtils.subChakra(player, Math.max(5, (int) Math.round((float) PlayerChakraUtils.getChakra(player) / 40)));
        }
    }

    // register the chakra provider
    @SubscribeEvent
    public static void registerCapabilities(RegisterCapabilitiesEvent event) {
        event.register(PlayerOnionChakraProvider.class);
    }

    // sync players chakra to them when they join the world
    @SubscribeEvent
    public static void onPlayerJoinedWorld(EntityJoinLevelEvent event) {
        // if on server
        if (!event.getLevel().isClientSide()) {
            if (event.getEntity() instanceof ServerPlayer player) {

                player.getCapability(PlayerOnionChakraProvider.PLAYER_ONION_CHAKRA).ifPresent(playerOnionChakra -> {
                    OALoggingUtils.logDebugInfo("Sending Chakra Sync packet onJoinedWorld to " + player.getName().getString());

                    OAAPIPackets.sendToPlayer(new ChakraDataSyncS2CPacket(playerOnionChakra.getChakra()), player);
                });

            }
        }
    }

    // randomly grant chakra to players, ~2.5x per 10 minutes
    @SubscribeEvent
    public static void onPlayerTick(TickEvent.PlayerTickEvent event) {
        // if on server
        if (event.side == LogicalSide.SERVER) {

            // if player has the chakra capability
            event.player.getCapability(PlayerOnionChakraProvider.PLAYER_ONION_CHAKRA).ifPresent(playerOnionChakra -> {
                if (event.player.getRandom().nextFloat() < 0.0001666f) { // about 1 every five minutes
                    OALoggingUtils.logDebugInfo("Randomly Adding chakra to " + event.player.getName().getString());

                    playerOnionChakra.addChakra(1);

                    OAAPIPackets.sendToPlayer(new ChakraDataSyncS2CPacket(playerOnionChakra.getChakra()),
                            event.player.getServer().getPlayerList().getPlayer(event.player.getUUID()));
                }
            });

        }
    }

    // dis-empower empowered item entities (item frames ignore onItemToss so this must be used to encompass all except death events)
    @SubscribeEvent
    public static void onItemToss(ItemTossEvent event) {
        if (event.getEntity().getItem().getItem() instanceof IChakraEmpoweredItem chakraEmpoweredItem
                && !(event.getEntity().getItem().getItem() instanceof RedOnionEmpoweredItem)
        ) {
            OALoggingUtils.logDebugInfo("onItemTossEvent found an instance of Empowered item, dis-empowering " +
                    chakraEmpoweredItem);

            event.getEntity().setItem(
                    chakraEmpoweredItem.createUpdatedItem(event.getEntity().getItem().getDamageValue())
                            .copyWithCount(event.getEntity().getItem().getCount())
            );
        }
    }

    // dis-empower empowered items dropped from frames or sources other than toss and death
    // add uuid to picked up chakra items, so they are unique when dis/empowering
    @SubscribeEvent
    public static void onPickupItem(EntityItemPickupEvent event) {
        // dis-empower empowered dropped items, except empowered red onions
        if (event.getItem().getItem().getItem() instanceof IChakraEmpoweredItem chakraEmpoweredItem
                && !(event.getItem().getItem().getItem() instanceof RedOnionEmpoweredItem)
        ) {

            OALoggingUtils.logDebugInfo("onItemPickupEvent found instance of empowered item, dis-empowering");

            // store the original ItemStack damage
            int damage = event.getItem().getItem().getDamageValue();

            // update item entity
            event.getItem().setItem(
                    chakraEmpoweredItem.createUpdatedItem(event.getItem().getItem().getDamageValue())
            );

            // assign ItemStack to updated entity
            ItemStack itemStackFinal = new ItemStack(event.getItem().getItem().getItem());

            // assign damage, if not a stackable item
            if (!(event.getItem().getItem().getItem() instanceof AbstractChakraItem)) {
                itemStackFinal.setDamageValue(damage);
            }


            // add updated ItemStack to inventory
            event.getEntity().getInventory().add(itemStackFinal);

            // remove original ItemEntity
            event.getItem().makeFakeItem();
        }

        // apply uuid if missing, and not a food item
        if (event.getItem().getItem().getItem() instanceof IChakraItem &&
                !(event.getItem().getItem().getItem() instanceof AbstractChakraFoodItem)
        ) {
            if (!event.getItem().getItem().getTag().contains("uuid")) {
                CompoundTag nbt = event.getItem().getItem().getTag().copy();
                nbt.putUUID("uuid", UUID.randomUUID());
                event.getItem().getItem().setTag(nbt);
            }
        }
    }

    // dis-empower empowered items
    @SubscribeEvent
    public static void onDeath(LivingDeathEvent event) {

        if (event.getEntity() instanceof Player player) {
            OALoggingUtils.logDebugInfo("Dis-Empowering inventory items and armour due to death of "
                    + player.getName().getString());

            // drop from inventory slots, and dis-empower
            AtomicInteger itemSlot = new AtomicInteger(0);
            player.getInventory().items.forEach(itemStack -> {
                if (itemStack.getItem() instanceof IChakraEmpoweredItem chakraEmpoweredItem
                        && !(itemStack.getItem() instanceof RedOnionEmpoweredItem)
                ) {
                    chakraEmpoweredItem.updateItemForDeath(player, itemStack, itemSlot.get());
                }
                itemSlot.getAndIncrement();
            });

            // drop from armour slots, and dis-empower
            AtomicInteger armourSlot = new AtomicInteger(0);
            player.getInventory().armor.forEach(itemStack -> {
                if (itemStack.getItem() instanceof IChakraEmpoweredArmourItem armourItem) {
                    armourItem.updateEquippedItemForDeath(player, itemStack, armourSlot.get());
                }
                armourSlot.getAndIncrement();
            });

        }
    }

    /**
     * Copy the player NBT data from the original player entity to the new one
     *
     * @param event Player Clone Event
     */
    private static void copyPlayerCapabilities(PlayerEvent.Clone event) {
        OALoggingUtils.logDebugInfo("Copying Capabilities from dead player to spawned player");

        // restore original player capabilities (nbt)
        event.getOriginal().reviveCaps();

        // apply old nbt to nbt
        event.getOriginal().getCapability(PlayerOnionChakraProvider.PLAYER_ONION_CHAKRA).ifPresent(oldStore -> {
            event.getEntity().getCapability(PlayerOnionChakraProvider.PLAYER_ONION_CHAKRA).ifPresent(newStore -> {
                newStore.copyFrom(oldStore);
            });
        });

        // delete original player capabilities, as no longer needed
        event.getOriginal().invalidateCaps();
    }

}
