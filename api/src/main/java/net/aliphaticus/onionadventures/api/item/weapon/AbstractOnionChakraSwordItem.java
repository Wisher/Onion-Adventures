/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 *
 * Permission & a License is hereby granted to use this code under the conditions that:
 * 1) The project using this code is open-source and accessible publicly via a service such as GitHub, GitLab, etc.
 * 2) The Project using this code is copy-left under a license such as MIT, GNU General Public License v3.0, etc.
 */

package net.aliphaticus.onionadventures.api.item.weapon;

import net.aliphaticus.onionadventures.api.capability.chakra.PlayerOnionChakra;
import net.aliphaticus.onionadventures.api.item.IChakraItem;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.SwordItem;
import net.minecraft.world.item.Tier;
import net.minecraft.world.level.Level;
import org.jetbrains.annotations.NotNull;

/**
 * Parent class for Chakra-using Sword items
 */
public abstract class AbstractOnionChakraSwordItem extends SwordItem implements IChakraItem {

    /**
     * Base attack damage, modified by {@link Tier#getAttackDamageBonus()}
     */
    private final float attackDamage;
    /**
     * Whether the item is empowered
     */
    private final boolean isEmpowered;
    /**
     * {@link PlayerOnionChakra PlayerOnionChakra} Cost to activate
     */
    private final int activationCost;
    /**
     * {@link PlayerOnionChakra PlayerOnionChakra} Cost to use
     */
    private final int useCost;
    /**
     * Shift hover text
     */
    private final String[] hoverTranslatable;

    /**
     * Calls the {@link SwordItem} constructor and sets the additional {@link IChakraItem} Values
     * @param tier {@link Tier} of the item, can be a custom Tier
     * @param attackDamage base attack damage modified by {@link Tier#getAttackDamageBonus()}
     * @param attackSpeed attack speed
     * @param isEmpowered whether the item is empowered
     * @param activationCost {@link PlayerOnionChakra PlayerOnionChakra} Cost to activate
     * @param useCost {@link PlayerOnionChakra PlayerOnionChakra} Cost to use
     * @param properties Item {@link net.minecraft.world.item.Item.Properties Properties}
     * @param hoverTranslatable String[] for shift hover text
     */
    public AbstractOnionChakraSwordItem(Tier tier, int attackDamage, float attackSpeed, boolean isEmpowered,
                                        int activationCost, int useCost, Properties properties, String[] hoverTranslatable) {
        super(tier, attackDamage, attackSpeed, properties);

        this.isEmpowered = isEmpowered;
        this.attackDamage = attackDamage;
        this.activationCost = activationCost;
        this.useCost = useCost;

        this.hoverTranslatable = hoverTranslatable;
    }

    public int getActivationCost() {
        return this.activationCost;
    }

    public int getUseCost() {
        return this.useCost;
    }

    public boolean getEmpowered() {
        return this.isEmpowered;
    }

    public String[] getHoverTranslatable() {
        return this.hoverTranslatable;
    }

    @Override
    public @NotNull InteractionResultHolder<ItemStack> use(@NotNull Level level, @NotNull Player player,
                                                           @NotNull InteractionHand hand) {

        if (hand == InteractionHand.MAIN_HAND && !level.isClientSide()) {
            updateItem(player, player.getItemInHand(hand));
        }

        return super.use(level, player, hand);
    }

    @Override
    public boolean isFoil(@NotNull ItemStack itemStack) {
        return getEmpowered();
    }

    @Override
    public void onCraftedBy(@NotNull ItemStack itemStack, @NotNull Level level, @NotNull Player player) {
        copyOrSetUUID(itemStack, itemStack);

        super.onCraftedBy(itemStack, level, player);
    }

}
