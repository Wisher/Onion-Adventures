/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.datagen;

import net.aliphaticus.onionadventures.OnionAdventures;
import net.aliphaticus.onionadventures.common.block.OABlocks;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.CropBlock;
import net.minecraftforge.client.model.generators.BlockModelProvider;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.registries.RegistryObject;

public class OABlockModelProvider extends BlockModelProvider {

    public OABlockModelProvider(PackOutput output, ExistingFileHelper existingFileHelper) {
        super(output, OnionAdventures.MODID, existingFileHelper);
    }

    @Override
    protected void registerModels() {
        registerCropModel(OABlocks.ONION_CROP);
        registerCropModel(OABlocks.WILD_ONIONS);
    }

    private void registerCropModel(RegistryObject<Block> block) {
        ((CropBlock) block.get()).getAgeProperty().getPossibleValues().forEach(
                age -> {
                    crop(block.getId().getPath() + "_stage" + age,
                            new ResourceLocation(
                                    OnionAdventures.MODID,
                                    "block/" + block.getId().getPath() + "_stage" + age
                            )
                    ).renderType("cutout");
                }
        );
    }

    private void registerBushCropModel(RegistryObject<Block> block) {
        crop(block.getId().getPath(), new ResourceLocation(OnionAdventures.MODID, "block/" + block.getId().getPath()))
                .renderType("cutout");
    }
}
