# Onion Adventures

WIP mod relating to the lore of the Onion Sage and the Brown Onion Empire

## Pipelines

| Main | [![pipeline status](https://gitlab.com/Wisher/Onion-Adventures/badges/main/pipeline.svg?job&key_text=main)](https://gitlab.com/Wisher/Onion-Adventures/-/commits/main) |
| ---- | ------------------------------------------------------------ |
| Dev  | [![pipeline status](https://gitlab.com/Wisher/Onion-Adventures/badges/dev/pipeline.svg?job&key_text=dev)](https://gitlab.com/Wisher/Onion-Adventures/-/commits/main) |


## Tags
Should be formatted as {MC_VERSION}-{MOD_VERSION}, Mod version as is in the gradle.properties (NO SNAPSHOTS)

e.g. 1.19.3-0.0.1.0

## Developers

You can find information on using the Onion Adventures API [on the Wiki](https://gitlab.com/Wisher/Onion-Adventures/-/wikis/home#developers), you may also be interested in the additional jars in the [Releases page](https://gitlab.com/Wisher/Onion-Adventures/-/releases)

The javadoc for the latest release can always be found [Here](https://wisher.gitlab.io/Onion-Adventures/)