/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.client.entity.mob;

import net.aliphaticus.onionadventures.OnionAdventures;
import net.aliphaticus.onionadventures.common.entity.custom.mobs.BrownOnionEntity;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import software.bernie.geckolib.constant.DataTickets;
import software.bernie.geckolib.core.animatable.model.CoreGeoBone;
import software.bernie.geckolib.core.animation.AnimationState;
import software.bernie.geckolib.model.GeoModel;
import software.bernie.geckolib.model.data.EntityModelData;

public class BrownOnionModel extends GeoModel<BrownOnionEntity> {

    @Override
    public ResourceLocation getModelResource(BrownOnionEntity animatable) {
        return new ResourceLocation(OnionAdventures.MODID, "geo/brown_onion.geo.json");
    }

    @Override
    public ResourceLocation getTextureResource(BrownOnionEntity animatable) {
        return new ResourceLocation(OnionAdventures.MODID, "textures/entity/brown_onion.png");
    }

    @Override
    public ResourceLocation getAnimationResource(BrownOnionEntity animatable) {
        return new ResourceLocation(OnionAdventures.MODID, "animations/brown_onion.animation.json");
    }

    @Override
    public void setCustomAnimations(BrownOnionEntity animatable, long instanceId, AnimationState<BrownOnionEntity> animationState) {
        CoreGeoBone head = getAnimationProcessor().getBone("main_body");

        if (head != null) {
            EntityModelData entityData = animationState.getData(DataTickets.ENTITY_MODEL_DATA);

            head.setRotY(entityData.headPitch() * Mth.DEG_TO_RAD);
        }
    }
}
