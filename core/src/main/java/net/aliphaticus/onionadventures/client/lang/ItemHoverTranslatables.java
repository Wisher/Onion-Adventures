/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.client.lang;

public final class ItemHoverTranslatables {

    /**
     * Base text for hovering over any item with more detail
     */
    public static final String ITEM_HOVER_BASE_EXPANDABLE = "hover.onionadventures.expand";


    /**
     * Cooked Onion hover text
     */
    public static final String ITEM_COOKED_ONION = "hover.onionadventures.cooked_onion";

    public static final String EMPOWERED_ITEM_DROPPED = "hover.onionadventures.empowered_item_drop";

    public static final String EMPOWERED_ITEM_DROPPED_EMPOWERED = "hover.onionadventures.empowered_item_drop_empowered";

    /**
     * Onion Empowerable Item hover text
     */
    public static final String ITEM_ONION_EMPOWERABLE_ITEM = "hover.onionadventures.onion_empowerable_item";
    public static final String ITEM_ONION_EMPOWERABLE_ITEM_1 = "hover.onionadventures.onion_empowerable_item_1";
    public static final String ITEM_ONION_EMPOWERABLE_ITEM_2 = "hover.onionadventures.onion_empowerable_item_2";
    public static final String ITEM_ONION_EMPOWERABLE_ITEM_3 = "hover.onionadventures.onion_empowerable_item_3";
    public static final String ITEM_ONION_EMPOWERABLE_ITEM_3_STACKABLE = "hover.onionadventures.onion_empowerable_item_3_stackable";
    public static final String ITEM_ONION_EMPOWERABLE_FOOD = "hover.onionadventures.onion_empowerable_food";
    public static final String ITEM_ONION_EMPOWERABLE_FOOD_1 = "hover.onionadventures.onion_empowerable_food_1";
    public static final String ITEM_ONION_EMPOWERABLE_FOOD_RED_ONION = "hover.onionadventures.onion_empowerable_food_red_onion";

    /**
     * Onion Empowered Item hover text
     */
    public static final String ITEM_ONION_EMPOWERED_ITEM = "hover.onionadventures.onion_empowered_item";

    public static final String ITEM_ONION_EMPOWERED_ITEM_1 = "hover.onionadventures.onion_empowered_item_1";

    public static final String ITEM_ONION_EMPOWERED_FOOD = "hover.onionadventures.onion_empowered_food";

    public static final String ITEM_ONION_EMPOWERED_FOOD_RED_ONION = "hover.onionadventures.onion_empowered_food_red_onion";

    /**
     * Onion Empowered Tool/Weapon hover text
     */
    public static final String ITEM_ONION_EMPOWERED_TOOL = "hover.onionadventures.onion_empowered_tool";

    public static final String ITEM_ONION_EMPOWERED_ARMOUR = "hover.onionadventures.onion_empowered_armour";

    public static final String ITEM_ONION_EMPOWERED_FOOD_1 = "hover.onionadventures.onion_empowered_food_1";
    public static final String ITEM_ONION_EMPOWERED_FOOD_1_RED_ONION = "hover.onionadventures.onion_empowered_food_1_red_onion";


    /**
     * Chakra Obelisk Texts
     */
    public static final String BLOCKITEM_CHAKRA_OBELISK_INFO = "hover.onionadventures.charka_obelisk_info";


}
