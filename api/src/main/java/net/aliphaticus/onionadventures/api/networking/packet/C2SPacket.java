/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 *
 * Permission & a License is hereby granted to use this code under the conditions that:
 * 1) The project using this code is open-source and accessible publicly via a service such as GitHub, GitLab, etc.
 * 2) The Project using this code is copy-left under a license such as MIT, GNU General Public License v3.0, etc.
 */

package net.aliphaticus.onionadventures.api.networking.packet;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkEvent;

import java.util.function.Supplier;

/**
 * Client to Server Packet parent class
 */
public abstract class C2SPacket {

    public C2SPacket() { }

    public C2SPacket(FriendlyByteBuf buf) { }

    /**
     * Converts objects into network data
     * @param buf {@link FriendlyByteBuf}, Network Buffer of Bytes
     */
    public abstract void toBytes(FriendlyByteBuf buf);

    /**
     * Handles the received packet on the server
     * @param supplier of {@link NetworkEvent.Context}
     */
    public abstract void handle(Supplier<NetworkEvent.Context> supplier);


}
