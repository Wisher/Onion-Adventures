/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.common.entity.goal;

import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.ai.goal.Goal;

public class HealMobGoal extends Goal {

    private final Mob mob;
    private final float amountToHeal;
    private final float chanceToHeal;

    private boolean canHeal;

    public HealMobGoal(Mob mob, float amountToHeal, float chanceToHeal) {
        this.mob = mob;
        this.amountToHeal = amountToHeal;
        this.chanceToHeal = chanceToHeal;
    }

    @Override
    public boolean canUse() {
        if (this.mob.getRandom().nextFloat() < chanceToHeal) {
            return true;
        }

        return false;
    }

    @Override
    public boolean canContinueToUse() {
        return false;
    }

    @Override
    public void start() {
        this.canHeal = true;
    }

    @Override
    public void stop() {
        this.canHeal = false;
    }

    @Override
    public void tick() {
        if (canHeal) {
            mob.heal(amountToHeal);
        }
    }
}
