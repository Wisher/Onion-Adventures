/*
 * Copyright (c) 2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.datagen;

import net.aliphaticus.onionadventures.OnionAdventures;
import net.aliphaticus.onionadventures.common.item.OAItems;
import net.aliphaticus.onionadventures.common.item.OAItemsCurios;
import net.minecraft.core.HolderLookup;
import net.minecraft.data.PackOutput;
import net.minecraft.data.tags.ItemTagsProvider;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.common.data.ExistingFileHelper;
import org.jetbrains.annotations.Nullable;

import java.util.concurrent.CompletableFuture;

public class OAItemTagProvider extends ItemTagsProvider {

    public OAItemTagProvider(PackOutput packOutput, CompletableFuture<HolderLookup.Provider> lookupProvider,
                             CompletableFuture<TagLookup<Block>> blockTagProvider,
                             @Nullable ExistingFileHelper existingFileHelper) {
        super(packOutput, lookupProvider, blockTagProvider, OnionAdventures.MODID, existingFileHelper);
    }

    @Override
    protected void addTags(HolderLookup.Provider provider) {


        // curios
        tag(curio("charm")).add(
                OAItemsCurios.CHAKRA_MONOCLE.get()
        );
    }

    private static TagKey<Item> curio(String name) {
        return ItemTags.create(new ResourceLocation("curios", name));
    }
}
