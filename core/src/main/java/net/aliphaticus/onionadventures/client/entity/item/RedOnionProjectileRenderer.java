/*
 * Copyright (c) 2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.client.entity.item;

import com.mojang.blaze3d.vertex.PoseStack;
import net.aliphaticus.onionadventures.OnionAdventures;
import net.aliphaticus.onionadventures.common.entity.custom.projectile.RedOnionEmpoweredProjectile;
import net.aliphaticus.onionadventures.common.item.OAItems;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemDisplayContext;
import net.minecraft.world.item.ItemStack;

public class RedOnionProjectileRenderer extends EntityRenderer<RedOnionEmpoweredProjectile> {

    public RedOnionProjectileRenderer(EntityRendererProvider.Context context) {
        super(context);
    }

    @Override
    public void render(RedOnionEmpoweredProjectile entityIn, float entityYaw, float partialTicks, PoseStack poseStack,
                       MultiBufferSource bufferIn, int light) {
        super.render(entityIn, entityYaw, partialTicks, poseStack, bufferIn, light);
        poseStack.pushPose();
        poseStack.scale(0.25F, 0.25F, 0.25F);
        poseStack.mulPose(this.entityRenderDispatcher.cameraOrientation());
        Minecraft.getInstance().getItemRenderer().renderStatic(
                new ItemStack(OAItems.RED_ONION_EMPOWERED.get()),
                ItemDisplayContext.FIXED,
                light,
                OverlayTexture.NO_OVERLAY,
                poseStack,
                bufferIn,
                null,
                (int) entityIn.blockPosition().asLong()
                );
        poseStack.popPose();
    }

    @Override
    public ResourceLocation getTextureLocation(RedOnionEmpoweredProjectile entity) {
        return new ResourceLocation(OnionAdventures.MODID, "textures/item/red_onion.png");
    }
}
