/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.common.item;

import net.aliphaticus.onionadventures.OnionAdventures;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.event.CreativeModeTabEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = OnionAdventures.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class OACreativeModeTabs {

    public static CreativeModeTab ONION_ADVENTURES_TAB;
    public static CreativeModeTab ONION_ADVENTURES_ARMOUR_TAB;
    public static CreativeModeTab ONION_ADVENTURES_TOOL_TAB;
    public static CreativeModeTab ONION_ADVENTURES_WEAPON_TAB;

    public static final String TRANSLATABLE_ONION_ADVENTURES_TAB = "tab.onionadventures.items";
    public static final String TRANSLATABLE_ONION_ADVENTURES_ARMOUR_TAB = "tab.onionadventures.armour";
    public static final String TRANSLATABLE_ONION_ADVENTURES_TOOL_TAB = "tab.onionadventures.tools";
    public static final String TRANSLATABLE_ONION_ADVENTURES_WEAPON_TAB = "tab.onionadventures.weapons";

    @SubscribeEvent
    public static void registerCreativeModeTabs(CreativeModeTabEvent.Register event) {
        ONION_ADVENTURES_TAB = event.registerCreativeModeTab(new ResourceLocation(OnionAdventures.MODID,
                "onion_adventures_tab"), builder -> builder.icon(() -> new ItemStack(OAItems.ONION.get()))
                .title(Component.translatable(TRANSLATABLE_ONION_ADVENTURES_TAB)).build());

        ONION_ADVENTURES_TOOL_TAB = event.registerCreativeModeTab(new ResourceLocation(OnionAdventures.MODID,
                "onion_adventures_tools_tab"), builder -> builder.icon(() -> new ItemStack(OAItems.ONION_PICKAXE.get()))
                .title(Component.translatable(TRANSLATABLE_ONION_ADVENTURES_TOOL_TAB)).build());

        ONION_ADVENTURES_WEAPON_TAB = event.registerCreativeModeTab(new ResourceLocation(OnionAdventures.MODID,
                "onion_adventures_weapons_tab"), builder -> builder.icon(() -> new ItemStack(OAItems.ONION_SWORD.get()))
                .title(Component.translatable(TRANSLATABLE_ONION_ADVENTURES_WEAPON_TAB)).build());

        ONION_ADVENTURES_ARMOUR_TAB = event.registerCreativeModeTab(new ResourceLocation(OnionAdventures.MODID,
                "onion_adventures_armour_tab"), builder -> builder.icon(() -> new ItemStack(OAItems.ONION_HELMET.get()))
                .title(Component.translatable(TRANSLATABLE_ONION_ADVENTURES_ARMOUR_TAB)).build());

    }

}
