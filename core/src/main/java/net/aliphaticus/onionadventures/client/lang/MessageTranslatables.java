/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.onionadventures.client.lang;

public class MessageTranslatables {

    public static final String CHAKRA_OBELISK_NOT_ENOUGH_CHAKRA = "msg.onionadventures.chakra_obelisk_not_enough_chakra";

}
