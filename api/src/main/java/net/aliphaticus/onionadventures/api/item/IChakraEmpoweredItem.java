/*
 * Copyright (c) 2023-2023. Phillip MacNaughton. All Rights Reserved unless otherwise explicitly stated
 *
 * Permission & a License is hereby granted to use this code under the conditions that:
 * 1) The project using this code is open-source and accessible publicly via a service such as GitHub, GitLab, etc.
 * 2) The Project using this code is copy-left under a license such as MIT, GNU General Public License v3.0, etc.
 */

package net.aliphaticus.onionadventures.api.item;

import net.aliphaticus.onionadventures.api.util.OALoggingUtils;
import net.aliphaticus.onionadventures.api.util.PlayerChakraUtils;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.common.extensions.IForgeItem;

import java.util.function.Consumer;

/**
 * Interface for Empowered Chakra items with some default logic
 */
public interface IChakraEmpoweredItem extends IForgeItem, IChakraItem {

    @Override
    default <T extends LivingEntity> int damageItem(ItemStack stack, int amount, T entity, Consumer<T> onBroken) {
        // implement chakra system
        OALoggingUtils.logDebugInfo("Attempting to damage " + ((ServerPlayer) entity).getName().getString() + "'s "
                + stack.getDisplayName().getString());

        // if not enough chakra remaining, convert item then damage
        if (!hasEnoughChakra(getUseCost(), (ServerPlayer) entity)) {
            // remove any remaining chakra
            PlayerChakraUtils.subChakra((ServerPlayer) entity, getUseCost());

            OALoggingUtils.logDebugInfo("Damaging then dis-empowering " + ((ServerPlayer) entity).getName().getString() + "'s " +
                    stack.getDisplayName().getString() + " by " + amount);

            stack.setDamageValue(stack.getDamageValue() + amount);
            updateItem((ServerPlayer) entity, stack);
            return 0; // as damage is already applied

        // else-if has enough chakra, don't damage and check remaining chakra
        } else {
            PlayerChakraUtils.subChakra((ServerPlayer) entity, getUseCost());

            // if not enough chakra remaining after stopping damage, convert item without damaging
            if (!hasEnoughChakra(getUseCost(), (ServerPlayer) entity)) {
                OALoggingUtils.logDebugInfo("Dis-Empowering & not damaging " + ((ServerPlayer) entity).getName().getString() + "'s " +
                        stack.getDisplayName().getString());

                updateItem((ServerPlayer) entity, stack);
                return 0; // as damage is already applied

            } else {
                // don't damage as enough chakra existed to absorb damage
                OALoggingUtils.logDebugInfo("Not damaging " + ((ServerPlayer) entity).getName().getString() +
                        "'s " + stack.getDisplayName().getString());

                return IForgeItem.super.damageItem(stack, 0, entity, onBroken);
            }
        }
    }


    /**
     * Update all inventory items as dis-empowered items
     * <br>This causes it to drop with the natural force/spread
     * @param player Player to drop from
     * @param itemStack item to be dis-empowered and dropped
     * @param itemSlot slot of the item
     */
    default void updateItemForDeath(Player player, ItemStack itemStack, int itemSlot) {
        OALoggingUtils.logDebugInfo("Dis-Empowering slot " + itemSlot + ", " + itemStack.getDisplayName().getString()
                + " due to player death");

        // create the dis-empowered item with appropriate damage
        int damage = itemStack.getDamageValue();
        ItemStack itemStack1 = createUpdatedItem(damage).copyWithCount(itemStack.getCount());
        copyOrSetUUID(itemStack1, itemStack);

        // set the armour slot to updated item
        player.getInventory().items.set(itemSlot, itemStack1);

        // remove the original item
        player.getInventory().removeItem(itemStack);
    }


}
